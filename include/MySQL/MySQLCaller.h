//MySQLCaller.h
//This namespace contains methods for connecting to and querying our MySQL database
//Construct an infostruct then pass it to init
//Call queryThis with the desired SQL query
//Need to include $(MYSQL) and $(MYSQL)\lib\opt
//Requires Boost, 1.56 seems to work fine
//Include the mysql dll in the bin folder

//For basic information on simple queries see: https://www.w3schools.com/sql/sql_intro.asp
//If you're ever uncertain which tables contain what you can perform basic operations to learn more about the tables:
////SELECT table_name FROM information_schema.tables WHERE table_schema="horseracingdb";  {will show all tables available}
////SELECT * FROM [table name];  {observe the keys and responses to get the idea of the table format}
////SELECT [list of variables] FROM [table name] WHERE [variable]=[desired output];  {will retrieve only matches can use AND and OR to stack}
////SELECT [list of variables] FROM [table name] WHERE [variable] in range (1,2,3,...); {will retrieve the desired list of entries}
//For more complex SQL queries read the link above as well as take a simple couple hour class on udacity or coursera

//The following is the connection info
//Username: statsreader
//Password: stats2010
//Host: statsmasterdb.mysql.database.azure.com
//Schema: horseracingdb

//TO DO:
//More secure way than statics

#include "mysql_connection.h"
#include <cppconn\driver.h>
#include <cppconn\exception.h>
#include <cppconn\resultset.h>
#include <cppconn\statement.h>
#include <cppconn\prepared_statement.h>
#include <vector>
#include <map>

namespace MySQLCaller
{
	//Connection information
	struct infoStruct
	{
		std::string username;
		std::string password;
		std::string host;
		std::string schema;
	};

	static std::vector<std::map<std::string, std::string>> result; //Always clear before using again or results will stack
	static sql::Driver *driver;
	static std::unique_ptr<sql::Connection> con;
	static bool isInit = false;
	static bool driverInit = false;

	//Driver only needs initialized once for the whole entire instance of the program do not repeat
	static bool initDriver()
	{
		try{
			driver = get_driver_instance();
		}
		catch (...)
		{
			return false;
		}
		driverInit = true;
		return driverInit;
	}

	//If any information is incorrect or connection fails will return false
	static bool init(const infoStruct &info){
		try{
			if (!driverInit){
				if (!initDriver())
					return false;
			} 
			con = std::unique_ptr<sql::Connection>(driver->connect(info.host, info.username, info.password));
			con->setSchema(info.schema);
			isInit = true;
		}
		catch (...)
		{
			isInit = false;
		}
		return isInit;
	}

	//Always call this after finishing queries
	static bool close(){ con->close(); return true; }

	//When done please call MySQLCaller::close()
	//Send a query, please construct safely
	//Only use the reading account do not use the admin account
	//Driver will throw an an unhandled exception running outside of full release mode
	//Recommend adding a vector<map<string,string>> method to store desired data into desired data structure in desired namespace
	//For example Jockey Cap Dataset -> SQLMapToDataset function
	static bool queryThis(const std::string &query)
	{
		if (isInit && driverInit)//driverInit not necessary but here in case something weird happens
		{
			try
			{
				sql::Statement *stmt;
				sql::ResultSet *res;
				sql::ResultSetMetaData *res_meta;
				std::vector<std::string> colnames;
				con->setSchema("horseracingdb");
				stmt = con->createStatement();
				res = stmt->executeQuery(query);
				res_meta = res->getMetaData();
				int columns = res_meta->getColumnCount();
				for (int i = 1; i <= columns; ++i)
				{
					colnames.emplace_back(res_meta->getColumnName(i));
				}
				while (res->next())
				{
					std::map <std::string, std::string> thisResult;
					for (int i = 1; i <= columns; ++i)
					{
						if (!res->isNull(i))
							thisResult[colnames[i - 1]] = res->getString(i);
						else
							thisResult[colnames[i - 1]] = "NULL";
					}
					result.emplace_back(thisResult);
				}
				delete stmt;
				delete res;
			}
			catch (sql::SQLException &e)
			{
				return false;
			}
		}
		if (close())
		{
			return true;
		}
		return false;
	}
}