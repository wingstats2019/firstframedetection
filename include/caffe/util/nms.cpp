#include "caffe/util/nms.hpp"
#include <cmath>
using std::max;
using std::min;

namespace caffe {

template <typename Dtype> 
Dtype iou(const Dtype A[], const Dtype B[])
{
  if (A[0] > B[2] || A[1] > B[3] || A[2] < B[0] || A[3] < B[1]) 
  {
    return 0;
  }

  // overlapped region (= box)
  const Dtype x1 = std::max(A[0],  B[0]);
  const Dtype y1 = std::max(A[1],  B[1]);
  const Dtype x2 = std::min(A[2],  B[2]);
  const Dtype y2 = std::min(A[3],  B[3]);

  // intersection area
  const Dtype width = std::max((Dtype)0,  x2 - x1 + (Dtype)1);
  const Dtype height = std::max((Dtype)0,  y2 - y1 + (Dtype)1);
  const Dtype area = width * height;

  // area of A, B
  const Dtype A_area = (A[2] - A[0] + (Dtype)1) * (A[3] - A[1] + (Dtype)1);
  const Dtype B_area = (B[2] - B[0] + (Dtype)1) * (B[3] - B[1] + (Dtype)1);

  // IoU
  return area / (A_area + B_area - area);
}

template float iou(const float A[], const float B[]);
template double iou(const double A[], const double B[]);


template <typename Dtype>
void soft_nms_cpu(const int num_boxes,
             const Dtype boxes[],
             int index_out[],
             int* const num_out,
             const int base_index,
             const Dtype nms_thresh, 
             const int max_num_out, 
             const int method)
{
  
  float threshold = 0.001;
  int count = 0;
  std::vector<char> is_dead(num_boxes);
  std::vector<Dtype> scores(num_boxes);
  for (int i = 0; i < num_boxes; ++i) {
    is_dead[i] = 0;
    scores[i] = boxes[i*5+4];
  }

  for (int i = 0; i < num_boxes; ++i) {
    if (is_dead[i]) {
      continue;
    }

    index_out[count++] = base_index + i;
    if (count == max_num_out) {
      break;
    }

    for (int j = i + 1; j < num_boxes; ++j) 
    {
      if (!is_dead[j]) 
      {
        Dtype ov = iou(&boxes[i*5], &boxes[j*5]);
        Dtype weight = 0.0;

        if(ov <= 0.0)
          continue;
          
        if(method == 1)
        {
          // linear
          weight = 1 - ov;
        }

        if(method == 2)
        {
          Dtype sigma = 0.5;
          weight = exp(-(ov*ov)/sigma);
        }

        if(method == 0)
        {
          if(ov > nms_thresh)
            weight = 0.0;
          else 
            weight = 1.0;
        }

        // decay score
        scores[j] = weight*scores[j];

        if(scores[j] < threshold)
          is_dead[j] = 1;
      }
    }
  }

  *num_out = count;
  is_dead.clear();
  scores.clear();
}


template <typename Dtype>
void nms_cpu(const int num_boxes,
             const Dtype boxes[],
             int index_out[],
             int* const num_out,
             const int base_index,
             const Dtype nms_thresh, const int max_num_out)
{
  int count = 0;
  std::vector<char> is_dead(num_boxes);
  for (int i = 0; i < num_boxes; ++i) {
    is_dead[i] = 0;
  }

  for (int i = 0; i < num_boxes; ++i) {
    if (is_dead[i]) {
      continue;
    }

    index_out[count++] = base_index + i;
    if (count == max_num_out) {
      break;
    }

    for (int j = i + 1; j < num_boxes; ++j) {
      if (!is_dead[j] && iou(&boxes[i * 5], &boxes[j * 5]) > nms_thresh) {
        is_dead[j] = 1;
      }
    }
  }

  *num_out = count;
  is_dead.clear();
}

template
void nms_cpu(const int num_boxes,
             const float boxes[],
             int index_out[],
             int* const num_out,
             const int base_index,
             const float nms_thresh, const int max_num_out);
template
void nms_cpu(const int num_boxes,
             const double boxes[],
             int index_out[],
             int* const num_out,
             const int base_index,
             const double nms_thresh, const int max_num_out);

template
void soft_nms_cpu(const int num_boxes,
             const float boxes[],
             int index_out[],
             int* const num_out,
             const int base_index,
             const float nms_thresh, 
             const int max_num_out, 
             const int method);
template
void soft_nms_cpu(const int num_boxes,
             const double boxes[],
             int index_out[],
             int* const num_out,
             const int base_index,
             const double nms_thresh, 
             const int max_num_out, 
             const int method);

}  // namespace caffe
