#include <cfloat>
#include <cmath>
#include "caffe/layers/roi_align_layer.hpp"

using std::max;
using std::min;
using std::floor;
using std::ceil;

// check http://kaiminghe.com/iccv17tutorial/maskrcnn_iccv2017_tutorial_kaiminghe.pdf page 24

namespace caffe {

template <typename Dtype>
void ROIAlignLayer<Dtype>::LayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) 
{
  auto ra_param = this->layer_param_.roi_align_param();

  CHECK_GT(ra_param.pooled_h(), 0)
      << "pooled_h must be > 0";
  CHECK_GT(ra_param.pooled_w(), 0)
      << "pooled_w must be > 0";
  pooled_height_ = ra_param.pooled_h();
  pooled_width_ = ra_param.pooled_w();
  spatial_scale_ = ra_param.spatial_scale();
  LOG(INFO) << "Spatial scale: " << spatial_scale_;
}

template <typename Dtype>
void ROIAlignLayer<Dtype>::Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) 
{
  channels_ = bottom[0]->channels();
  height_ = bottom[0]->height();
  width_ = bottom[0]->width();
  top[0]->Reshape(bottom[1]->num(), channels_, pooled_height_,
      pooled_width_);
  max_idx_x_.Reshape(bottom[1]->num(), channels_, pooled_height_,
      pooled_width_);
  max_idx_y_.Reshape(bottom[1]->num(), channels_, pooled_height_,
      pooled_width_);
}

template <typename Dtype>
void ROIAlignLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) 
{
  const Dtype* bottom_data = bottom[0]->cpu_data();
  const Dtype* bottom_rois = bottom[1]->cpu_data();
  // Number of ROIs
  int num_rois = bottom[1]->num();
  int batch_size = bottom[0]->num();
  int top_count = top[0]->count();
  Dtype* top_data = top[0]->mutable_cpu_data();
  caffe_set(top_count, Dtype(-FLT_MAX), top_data);
  Dtype* argmax_x = max_idx_x_.mutable_cpu_data();
  Dtype* argmax_y = max_idx_y_.mutable_cpu_data();

  caffe_set(top_count, Dtype(-1.0), argmax_x);
  caffe_set(top_count, Dtype(-1.0), argmax_y);

  // For each ROI R = [batch_index x1 y1 x2 y2]: max pool over R
  for (int n = 0; n < num_rois; ++n) 
  {
    int roi_batch_ind = bottom_rois[0];
    Dtype roi_start_w = bottom_rois[1] * spatial_scale_;
    Dtype roi_start_h = bottom_rois[2] * spatial_scale_;
    Dtype roi_end_w = bottom_rois[3] * spatial_scale_;
    Dtype roi_end_h = bottom_rois[4] * spatial_scale_;
    CHECK_GE(roi_batch_ind, 0);
    CHECK_LT(roi_batch_ind, batch_size);

    Dtype roi_height = max(roi_end_h - roi_start_h, static_cast<Dtype>(1));
    Dtype roi_width = max(roi_end_w - roi_start_w, static_cast<Dtype>(1));
    const Dtype bin_size_h = static_cast<Dtype>(roi_height)
                             / static_cast<Dtype>(pooled_height_);
    const Dtype bin_size_w = static_cast<Dtype>(roi_width)
                             / static_cast<Dtype>(pooled_width_);

    const Dtype* batch_data = bottom_data + bottom[0]->offset(roi_batch_ind);

    for (int c = 0; c < channels_; ++c) 
    {
      // for one channel, calculate roi align result and update top blob
      for (int ph = 0; ph < pooled_height_; ++ph) 
      {
        for (int pw = 0; pw < pooled_width_; ++pw) 
        {
          Dtype hstart = static_cast<int>(static_cast<Dtype>(ph)
                                              * bin_size_h);
          Dtype wstart = static_cast<int>(static_cast<Dtype>(pw)
                                              * bin_size_w);
          Dtype hend = static_cast<int>(static_cast<Dtype>(ph + 1)
                                           * bin_size_h);
          Dtype wend = static_cast<int>(static_cast<Dtype>(pw + 1)
                                           * bin_size_w);

          hstart = min(max(hstart + roi_start_h, static_cast<Dtype>(0)), static_cast<Dtype>(height_));
          hend = min(max(hend + roi_start_h, static_cast<Dtype>(0)), static_cast<Dtype>(height_));
          wstart = min(max(wstart + roi_start_w, static_cast<Dtype>(0)), static_cast<Dtype>(width_));
          wend = min(max(wend + roi_start_w, static_cast<Dtype>(0)), static_cast<Dtype>(width_));

          bool is_empty = (hend <= hstart) || (wend <= wstart);

          Dtype maxval = is_empty ? Dtype(0) : -FLT_MAX;
          Dtype max_idx_x = Dtype(-1);
          Dtype max_idx_y = Dtype(-1);

          // sample four regular points
          // inside each roi bin
          Dtype h_stride = (hend - hstart)/Dtype(3.0);
          Dtype w_stride = (wend - wstart)/Dtype(3.0);
          for(Dtype h=hstart+h_stride; h<=hend-h_stride+Dtype(0.01); h+=max(h_stride, Dtype(0.01)))
          {
            for(Dtype w=wstart+w_stride; w<=wend-w_stride+Dtype(0.01); w+=max(w_stride, Dtype(0.01)))
            {
              // calculate nearest four points used for bilinear interpolation
              int hlow = min(max(static_cast<int>(floor(h)), 0), height_-1);
              int hhigh = min(max(static_cast<int>(ceil(h)), 0), height_-1);
              int wleft = min(max(static_cast<int>(floor(w)), 0), width_-1);
              int wright = min(max(static_cast<int>(ceil(w)), 0), width_-1);

              // index in batch data
              int top_left = hlow*width_+wleft;
              int top_right = hlow*width_+wright;
              int bottom_left = hhigh*width_+wleft;
              int bottom_right = hhigh*width_+wright;

              Dtype alpha = (hlow == hhigh) ? static_cast<Dtype>(0.5) : (h - hlow) / (hhigh - hlow);
              Dtype beta = (wleft == wright) ? static_cast<Dtype>(0.5) : (w - wleft) / (wright - wleft);
              Dtype value = (1 - alpha) * (1 - beta) * batch_data[top_left] + alpha * (1 - beta) * batch_data[bottom_left]
                                  + (1 - alpha) * beta * batch_data[top_right] + alpha * beta * batch_data[bottom_right];

              // max pooling sampled points
              if (value > maxval) 
              {
                maxval = value;
                max_idx_x = w;
                max_idx_y = h;
              }
            }
          }

          int index = ph * pooled_width_ + pw;
          top_data[index] = maxval;
          argmax_x[index] = (Dtype)max_idx_x;
          argmax_y[index] = (Dtype)max_idx_y;
        }
      }

      // Increment all data pointers by one channel
      batch_data += bottom[0]->offset(0, 1);
      top_data += top[0]->offset(0, 1);
      argmax_x += max_idx_x_.offset(0, 1);
      argmax_y += max_idx_y_.offset(0, 1);
    }
    // Increment ROI data pointer
    bottom_rois += bottom[1]->offset(1);
  }
}

template <typename Dtype>
void ROIAlignLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
  NOT_IMPLEMENTED;
}


#ifdef CPU_ONLY
STUB_GPU(ROIAlignLayer);
#endif

INSTANTIATE_CLASS(ROIAlignLayer);
REGISTER_LAYER_CLASS(ROIAlign);

}  // namespace caffe