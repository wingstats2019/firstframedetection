#include <cfloat>
#include <cmath>
#include "caffe/layers/roi_align_layer.hpp"

using std::max;
using std::min;

namespace caffe {

template <typename Dtype>
__global__ void ROIAlignForward(const int nthreads, const Dtype* bottom_data,
    const Dtype spatial_scale, const int channels, const int height,
    const int width, const int pooled_height, const int pooled_width,
    const Dtype* bottom_rois, Dtype* top_data, Dtype* argmax_x, Dtype* argmax_y) {
  CUDA_KERNEL_LOOP(index, nthreads) 
  {
    // (n, c, ph, pw) is an element in the pooled output
    int pw = index % pooled_width;
    int ph = (index / pooled_width) % pooled_height;
    int c = (index / pooled_width / pooled_height) % channels;
    int n = index / pooled_width / pooled_height / channels;

    bottom_rois += n * 5;
    int roi_batch_ind = bottom_rois[0];

    if(roi_batch_ind < 0)
    {
      top_data[index] = 0;
      argmax_x[index] = 0;
      argmax_y[index] = 0;
      continue;
    }

    Dtype roi_start_w = bottom_rois[1] * spatial_scale;
    Dtype roi_start_h = bottom_rois[2] * spatial_scale;
    Dtype roi_end_w = bottom_rois[3] * spatial_scale;
    Dtype roi_end_h = bottom_rois[4] * spatial_scale;

    // Force malformed ROIs to be 1x1
    Dtype roi_width = max(roi_end_w - roi_start_w, Dtype(1));
    Dtype roi_height = max(roi_end_h - roi_start_h, Dtype(1));
    Dtype bin_size_h = static_cast<Dtype>(roi_height)
                       / static_cast<Dtype>(pooled_height);
    Dtype bin_size_w = static_cast<Dtype>(roi_width)
                       / static_cast<Dtype>(pooled_width);

    Dtype hstart = static_cast<Dtype>(static_cast<Dtype>(ph)
                                        * bin_size_h);
    Dtype wstart = static_cast<Dtype>(static_cast<Dtype>(pw)
                                        * bin_size_w);
    Dtype hend = static_cast<Dtype>(static_cast<Dtype>(ph + 1)
                                     * bin_size_h);
    Dtype wend = static_cast<Dtype>(static_cast<Dtype>(pw + 1)
                                     * bin_size_w);

    // Add roi offsets and clip to input boundaries
    hstart = min(max(hstart + roi_start_h, static_cast<Dtype>(0)), static_cast<Dtype>(height));
    hend = min(max(hend + roi_start_h, static_cast<Dtype>(0)), static_cast<Dtype>(height));
    wstart = min(max(wstart + roi_start_w, static_cast<Dtype>(0)), static_cast<Dtype>(width));
    wend = min(max(wend + roi_start_w, static_cast<Dtype>(0)), static_cast<Dtype>(width));

    bool is_empty = (hend <= hstart) || (wend <= wstart);

    // Define an empty pooling region to be zero
    Dtype maxval = is_empty ? Dtype(0) : -FLT_MAX;
    Dtype max_idx_x = Dtype(-1);
    Dtype max_idx_y = Dtype(-1);

    bottom_data += (roi_batch_ind * channels + c) * height * width;
    Dtype h_stride = (hend - hstart)/Dtype(3.0);
    Dtype w_stride = (wend - wstart)/Dtype(3.0);
    for(Dtype h=hstart+h_stride; h<=hend-h_stride+Dtype(0.01); h+=max(h_stride, Dtype(0.01)))
    {
      for(Dtype w=wstart+w_stride; w<=wend-w_stride+Dtype(0.01); w+=max(w_stride, Dtype(0.01)))
      {
        int hlow = min(max(static_cast<int>(floor(h)), 0), height-1);
        int hhigh = min(max(static_cast<int>(ceil(h)), 0), height-1);
        int wleft = min(max(static_cast<int>(floor(w)), 0), width-1);
        int wright = min(max(static_cast<int>(ceil(w)), 0), width-1);
        int topleft = hlow * width + wleft;
        int topright = hlow * width + wright;
        int bottomleft = hhigh * width + wleft;
        int bottomright = hhigh * width + wright;

        Dtype alpha = (hlow == hhigh) ? static_cast<Dtype>(0.5) : (h - hlow) / (hhigh - hlow);
        Dtype beta = (wleft == wright) ? static_cast<Dtype>(0.5) : (w - wleft) / (wright - wleft);
        Dtype value = (1 - alpha) * (1 - beta) * bottom_data[topleft] + alpha * (1 - beta) * bottom_data[bottomleft]
                            + (1 - alpha) * beta * bottom_data[topright] + alpha * beta * bottom_data[bottomright];
        
        if (value > maxval) 
        {
          maxval = value;
          max_idx_x = w;
          max_idx_y = h;
        }
      }
    }

    top_data[index] = maxval;
    argmax_x[index] = (Dtype)max_idx_x;
    argmax_y[index] = (Dtype)max_idx_y;
  }
}

template <typename Dtype>
void ROIAlignLayer<Dtype>::Forward_gpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
  const Dtype* bottom_data = bottom[0]->gpu_data();
  const Dtype* bottom_rois = bottom[1]->gpu_data();
  Dtype* top_data = top[0]->mutable_gpu_data();
  Dtype* argmax_x = max_idx_x_.mutable_gpu_data();
  Dtype* argmax_y = max_idx_y_.mutable_gpu_data();
  int count = top[0]->count();
  // NOLINT_NEXT_LINE(whitespace/operators)
  ROIAlignForward<Dtype><<<CAFFE_GET_BLOCKS(count), CAFFE_CUDA_NUM_THREADS>>>(
      count, bottom_data, spatial_scale_, channels_, height_, width_,
      pooled_height_, pooled_width_, bottom_rois, top_data, argmax_x, argmax_y);
  CUDA_POST_KERNEL_CHECK;
}

template <typename Dtype>
__global__ void ROIAlignBackward(const int nthreads, const Dtype* top_diff,
    const Dtype* argmax_x, const Dtype* argmax_y, const int num_rois, const Dtype spatial_scale,
    const int channels, const int height, const int width,
    const int pooled_height, const int pooled_width, Dtype* bottom_diff,
    const Dtype* bottom_rois) {
  CUDA_KERNEL_LOOP(index, nthreads) 
  {
    // (n, c, h, w) coords in bottom data
    int w = index % width;
    int h = (index / width) % height;
    int c = (index / width / height) % channels;
    int n = index / width / height / channels;

    Dtype gradient = 0;
    // Accumulate gradient over all ROIs that pooled this element
    for (int roi_n = 0; roi_n < num_rois; ++roi_n) 
    {
      const Dtype* offset_bottom_rois = bottom_rois + roi_n * 5;
      int roi_batch_ind = offset_bottom_rois[0];
      // Skip if ROI's batch index doesn't match n
      if (n != roi_batch_ind) 
      {
        continue;
      }

      Dtype roi_start_w = offset_bottom_rois[1] * spatial_scale;
      Dtype roi_start_h = offset_bottom_rois[2] * spatial_scale;
      Dtype roi_end_w = offset_bottom_rois[3] * spatial_scale;
      Dtype roi_end_h = offset_bottom_rois[4] * spatial_scale;

      // Skip if ROI doesn't include (h, w)
      const bool in_roi = (w >= roi_start_w - 1.0 && w <= roi_end_w + 1.0 &&
                           h >= roi_start_h - 1.0 && h <= roi_end_h + 1.0);
      if (!in_roi) 
      {
        continue;
      }

      int offset = (roi_n * channels + c) * pooled_height * pooled_width;
      const Dtype* offset_top_diff = top_diff + offset;
      const Dtype* offset_argmax_x = argmax_x + offset; // corresponding max pool index
      const Dtype* offset_argmax_y = argmax_y + offset; // corresponding max pool index

      // Force malformed ROIs to be 1x1
      Dtype roi_width = max(roi_end_w - roi_start_w, static_cast<Dtype>(1));
      Dtype roi_height = max(roi_end_h - roi_start_h, static_cast<Dtype>(1));

      for(int ph=0; ph<pooled_height; ++ph)
      {
        for(int pw=0; pw<pooled_width; ++pw)
        {
          const int pool_index = ph*pooled_width+pw;
          Dtype a_x = offset_argmax_x[pool_index];
          Dtype a_y = offset_argmax_y[pool_index];

          // convert max pool index to four corner points
          int hlow = min(max(static_cast<int>(floor(a_y)), 0), height-1);
          int hhigh = min(max(static_cast<int>(ceil(a_y)), 0), height-1);
          int wleft = min(max(static_cast<int>(floor(a_x)), 0), width-1);
          int wright = min(max(static_cast<int>(ceil(a_x)), 0), width-1);

          // (w, h) is not around (a_x, a_y)
          if(h!=hlow && h!=hhigh && w!=wleft && w!=wright)
            continue;

          // bp
          Dtype alpha = (hlow == hhigh) ? static_cast<Dtype>(0.5) : (a_y - hlow) / (hhigh - hlow);
          Dtype beta = (wleft == wright) ? static_cast<Dtype>(0.5) : (a_x - wleft) / (wright - wleft);
          if (h == hlow && w == wleft) gradient += offset_top_diff[pool_index] * (1 - alpha) * (1 - beta);
          else if (h == hlow && w == wright) gradient += offset_top_diff[pool_index] * (1 - alpha) * beta;
          else if (h == hhigh && w == wleft) gradient += offset_top_diff[pool_index] * alpha * (1 - beta);
          else if (h == hhigh && w == wright) gradient += offset_top_diff[pool_index] * alpha * beta;
        }
      }
    }
    bottom_diff[index] = gradient;
  }
}

template <typename Dtype>
void ROIAlignLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
  if (!propagate_down[0]) {
    return;
  }
  const Dtype* bottom_rois = bottom[1]->gpu_data();
  const Dtype* top_diff = top[0]->gpu_diff();
  Dtype* bottom_diff = bottom[0]->mutable_gpu_diff();
  const int count = bottom[0]->count();
  caffe_gpu_set(count, Dtype(0.), bottom_diff);
  
  const Dtype* argmax_x = max_idx_x_.gpu_data();
  const Dtype* argmax_y = max_idx_y_.gpu_data();

  // NOLINT_NEXT_LINE(whitespace/operators)
  ROIAlignBackward<Dtype><<<CAFFE_GET_BLOCKS(count), CAFFE_CUDA_NUM_THREADS>>>(
      count, top_diff, argmax_x, argmax_y, top[0]->num(), spatial_scale_, channels_,
      height_, width_, pooled_height_, pooled_width_, bottom_diff, bottom_rois);
  CUDA_POST_KERNEL_CHECK;
}

INSTANTIATE_LAYER_GPU_FUNCS(ROIAlignLayer);

}  // namespace caffe