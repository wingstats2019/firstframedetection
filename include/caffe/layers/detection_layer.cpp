
#include "caffe/layers/detection_layer.hpp"

using std::min;
using std::max;
using std::sort;
using cv::Range;
using cv::Mat_;

namespace caffe {

template <typename Dtype> 
Mat_<Dtype> DetectionLayer<Dtype>::soft_nms(const Mat_<Dtype>& cls_dets, const Dtype& soft_thresh) {
  vector<Dtype> x1(cls_dets.rows);
  vector<Dtype> y1(cls_dets.rows);
  vector<Dtype> x2(cls_dets.rows);
  vector<Dtype> y2(cls_dets.rows);
  vector<Dtype> scores(cls_dets.rows);
  vector<Dtype> areas(cls_dets.rows);
  CHECK_EQ(cls_dets.cols, 6);
  for (int i = 0; i < cls_dets.rows; ++i) {
    auto det_i = cls_dets[i];
    x1[i] = det_i[0];
    y1[i] = det_i[1];
    x2[i] = det_i[2];
    y2[i] = det_i[3];
    // classid[i] = det_i[4];
    scores[i] = det_i[5];
    areas[i] = (x2[i] - x1[i] + 1) * (y2[i] - y1[i] + 1);
  }
  // soft nms
  vector<int> idx(cls_dets.rows);
  std::iota(idx.begin(), idx.end(), 0);
  std::sort(idx.begin(), idx.end(), 
    [&scores](int i, int j) { return scores[i] > scores[j]; });
  int n = cls_dets.rows;
  vector<int> sup;
  for(int i=0; i<n; i++)
  {
    int u = idx[i];
    int k=i+1;
    sup.resize(0);
    for(int j=k; j<n; j++)
    {
      int v = idx[j];
      auto xx1 = max(x1[u], x1[v]);
      auto yy1 = max(y1[u], y1[v]);
      auto xx2 = min(x2[u], x2[v]);
      auto yy2 = min(y2[u], y2[v]);
      auto w = max(Dtype(0), xx2 - xx1 + 1);
      auto h = max(Dtype(0), yy2 - yy1 + 1);
      auto inter = w * h;
      auto iou = inter / (areas[u] + areas[v] - inter);
      Dtype sigma = 0.5;
      Dtype weight = exp(-(iou*iou)/sigma);
      // decay score
      scores[j] = weight*scores[v];
      if(scores[j] < soft_thresh) 
      {
        // abandon this box 
        sup.push_back(idx[j]);
      }else{
        idx[k++] = idx[j];
      }
    }
    CHECK_EQ(sup.size() + k, n);
    std::copy(sup.begin(), sup.end(), idx.begin() + k);
    n = k;
  }
  Mat_<Dtype> NMSed_dets(n, cls_dets.cols);
  for (int i = 0; i < n; ++i) {
    cls_dets.row(idx[i]).copyTo(NMSed_dets.row(i));
  }
  return NMSed_dets;
}

template <typename Dtype>
Mat_<Dtype> DetectionLayer<Dtype>::bbox_transform_inv(const Mat_<Dtype>& boxes, const Mat_<Dtype>& box_deltas) {
  Mat_<Dtype> pred_boxes(box_deltas.size());
  CHECK(pred_boxes.data);
  for (int i = 0; i < pred_boxes.rows; ++i) {
    auto box_i = boxes[i];
    auto delta_i = box_deltas[i];
    auto pred_box_i = pred_boxes[i];
    auto w = box_i[2] - box_i[0] + 1;
    auto h = box_i[3] - box_i[1] + 1;
    auto ctr_x = box_i[0] + Dtype(.5) * w;
    auto ctr_y = box_i[1] + Dtype(.5) * h;
    for (int j = 0; j < pred_boxes.cols / 4; ++j) {
      auto dx = delta_i[j*4];
      auto dy = delta_i[j*4+1];
      auto dw = delta_i[j*4+2];
      auto dh = delta_i[j*4+3];
      auto pred_ctr_x = dx * w + ctr_x;
      auto pred_ctr_y = dy * h + ctr_y;
      auto pred_w = std::exp(dw) * w;
      auto pred_h = std::exp(dh) * h;
      pred_box_i[j*4  ] = max(min(pred_ctr_x - Dtype(.5) * pred_w, im_shape_[1] - 1), Dtype(0));
      pred_box_i[j*4+1] = max(min(pred_ctr_y - Dtype(.5) * pred_h, im_shape_[0] - 1), Dtype(0));
      pred_box_i[j*4+2] = max(min(pred_ctr_x + Dtype(.5) * pred_w, im_shape_[1] - 1), Dtype(0));
      pred_box_i[j*4+3] = max(min(pred_ctr_y + Dtype(.5) * pred_h, im_shape_[0] - 1), Dtype(0));
    }
  }
  return pred_boxes;
}

template <typename Dtype> 
Mat_<Dtype> DetectionLayer<Dtype>::nms_vote(const Mat_<Dtype>& cls_dets, const Dtype& thresh, bool vote) {
  vector<Dtype> x1(cls_dets.rows);
  vector<Dtype> y1(cls_dets.rows);
  vector<Dtype> x2(cls_dets.rows);
  vector<Dtype> y2(cls_dets.rows);
  vector<Dtype> scores(cls_dets.rows);
  vector<Dtype> areas(cls_dets.rows);
  CHECK_EQ(cls_dets.cols, 6);
  for (int i = 0; i < cls_dets.rows; ++i) {
    auto det_i = cls_dets[i];
    x1[i] = det_i[0];
    y1[i] = det_i[1];
    x2[i] = det_i[2];
    y2[i] = det_i[3];
    // classid[i] = det_i[4];
    scores[i] = det_i[5];
    areas[i] = (x2[i] - x1[i] + 1) * (y2[i] - y1[i] + 1);
  }
  // nms
  vector<int> idx(cls_dets.rows);
  std::iota(idx.begin(), idx.end(), 0);
  std::sort(idx.begin(), idx.end(), 
    [&scores](int i, int j) { return scores[i] > scores[j]; });
  int n = cls_dets.rows;
  vector<int> sup;
  for (int i = 0; i < n; ++i) {
    int u = idx[i];
    sup.resize(0);
    int k = i + 1;
    for (int j = k; j < n; ++j) {
      int v = idx[j];
      auto xx1 = max(x1[u], x1[v]);
      auto yy1 = max(y1[u], y1[v]);
      auto xx2 = min(x2[u], x2[v]);
      auto yy2 = min(y2[u], y2[v]);
      auto w = max(Dtype(0), xx2 - xx1 + 1);
      auto h = max(Dtype(0), yy2 - yy1 + 1);
      auto inter = w * h;
      auto iou = inter / (areas[u] + areas[v] - inter);
      if (iou >= thresh) {
        sup.push_back(idx[j]);
      } else {
        idx[k++] = idx[j];
      }
    }
    CHECK_EQ(sup.size() + k, n);
    std::copy(sup.begin(), sup.end(), idx.begin() + k);
    n = k;
  }
  // bbox_vote
  if (vote) {
    Dtype thresh(.5);
    Mat_<Dtype> voted = Mat_<Dtype>::zeros(n, 6);
    std::sort(idx.begin() + n, idx.end());
    for (int i = 0; i < n; ++i) {
      int u = idx[i];
      auto det_u = cls_dets[u];
      auto voted_i = voted[i];
      auto acc_score = det_u[5];
      voted_i[0] = det_u[0] * acc_score;
      voted_i[1] = det_u[1] * acc_score;
      voted_i[2] = det_u[2] * acc_score;
      voted_i[3] = det_u[3] * acc_score;
      for (int j = n; j < idx.size(); ++j) {
        int v = idx[j];
        auto det_v = cls_dets[v];
        auto xx1 = max(x1[u], x1[v]);
        auto yy1 = max(y1[u], y1[v]);
        auto xx2 = min(x2[u], x2[v]);
        auto yy2 = min(y2[u], y2[v]);
        auto iw = xx2 - xx1 + 1;
        auto ih = yy2 - yy1 + 1;
        if (iw <= 0 || ih <= 0)
          continue;
        auto ua = areas[u] + areas[v] - iw * ih;
        auto ov = iw * ih / ua;
        if (ov < thresh)
          continue;
        auto score = det_v[5];
        voted_i[0] += det_v[0] * score;
        voted_i[1] += det_v[1] * score;
        voted_i[2] += det_v[2] * score;
        voted_i[3] += det_v[3] * score;
        acc_score += score;
      }
      voted_i[0] /= acc_score;
      voted_i[1] /= acc_score;
      voted_i[2] /= acc_score;
      voted_i[3] /= acc_score;
      voted_i[4] = det_u[4];
      voted_i[5] = det_u[5];
    }
    return voted;
  } 
  
  Mat_<Dtype> NMSed_dets(n, cls_dets.cols);
  for (int i = 0; i < n; ++i) {
    cls_dets.row(idx[i]).copyTo(NMSed_dets.row(i));
  }
  return NMSed_dets;
}

template <typename Dtype>
void DetectionLayer<Dtype>::LayerSetUp(const vector<Blob<Dtype>*>& bottom, 
    const vector<Blob<Dtype>*>& top) {
  auto& param = this->layer_param_.detection_param();    
  num_classes_ = param.num_classes();
  CHECK_GE(param.thresh_size(), 1);
  if (param.thresh_size() == 1) {
    for (int i = 1; i < num_classes_; ++i)
      threshs_.push_back(param.thresh(0));
  } else {
    CHECK_EQ(param.thresh_size(), num_classes_);
    for (int i = 1; i < num_classes_; ++i) 
      threshs_.push_back(param.thresh(i));
  }
  CHECK_GE(param.nms_size(), 1);
  if (param.nms_size() == 1) {
    for (int i = 1; i < num_classes_; ++i) 
      nmss_.push_back(param.nms(0));
  } else {
    CHECK_EQ(param.nms_size(), num_classes_);
    for (int i = 1; i < num_classes_; ++i) 
      nmss_.push_back(param.nms(i));
  }
  CHECK_EQ(param.im_shape_size(), 2);
  im_shape_.resize(2);
  im_shape_[0] = param.im_shape(0);
  im_shape_[1] = param.im_shape(1);
}

template <typename Dtype>
void DetectionLayer<Dtype>::Reshape(const vector<Blob<Dtype>*>& bottom, 
    const vector<Blob<Dtype>*>& top) {}

template <typename Dtype>
void DetectionLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
    const vector<Blob<Dtype>*>& top) {
  
  const Mat_<Dtype> rois(bottom[0]->shape(0), bottom[0]->shape(1), 
    const_cast<Dtype*>(bottom[0]->cpu_data()));
  const Mat_<Dtype> box_deltas(bottom[1]->shape(0), bottom[1]->shape(1), 
    const_cast<Dtype*>(bottom[1]->cpu_data()));
  const Mat_<Dtype> scores(bottom[2]->shape(0), bottom[2]->shape(1), 
    const_cast<Dtype*>(bottom[2]->cpu_data()));
  const Mat_<Dtype> im_info(bottom[3]->shape(0), 1,
    const_cast<Dtype*>(bottom[3]->cpu_data()));

  // Mat_<Dtype> boxes = rois(Range::all(), Range(1, 5)) / im_info(2);
  Mat_<Dtype> boxes = rois(Range::all(), Range(1, 5)).clone();
  for (int i = 0; i < boxes.rows; ++i) {
    auto boxi = boxes[i];
    boxi[0] /= im_info(2);
    boxi[1] /= im_info(3);
    boxi[2] /= im_info(2);
    boxi[3] /= im_info(3);
  }
  auto pred_boxes = bbox_transform_inv(boxes, box_deltas);
  
  Mat_<Dtype> all_boxes;
  for (int i = 1; i < num_classes_; ++i) {
    vector<int> inds;
    for (int j = 0; j < scores.rows; ++j) {
      if (scores(j, i) > threshs_[i - 1]) 
        inds.push_back(j);
    }
    Mat_<Dtype> cls_dets(inds.size(), 6);
    for (int j = 0; j < inds.size(); ++j) {
      const auto box = pred_boxes[inds[j]];
      auto cls_det_j = cls_dets[j];
      cls_det_j[0] = box[i*4];
      cls_det_j[1] = box[i*4+1];
      cls_det_j[2] = box[i*4+2];
      cls_det_j[3] = box[i*4+3];
      cls_det_j[4] = i;
      cls_det_j[5] = scores(inds[j], i);
    }

    auto vote = this->layer_param_.detection_param().vote();

    Mat_<Dtype> NMSed_detes;
    if(vote)
      NMSed_detes = nms_vote(cls_dets, nmss_[i - 1], vote);
    else
      NMSed_detes = soft_nms(cls_dets, nmss_[i - 1]);

    if (NMSed_detes.rows) {
      all_boxes.push_back(NMSed_detes);
    }
  }  
  top[0]->Reshape({all_boxes.rows, all_boxes.cols});
  all_boxes = all_boxes.clone();
  if (top[0]->count())
    caffe_copy(top[0]->count(), all_boxes[0], top[0]->mutable_cpu_data());
}

#ifdef CPU_ONLY
STUB_GPU(DetectionLayer);
#endif

INSTANTIATE_CLASS(DetectionLayer);
REGISTER_LAYER_CLASS(Detection);


}

