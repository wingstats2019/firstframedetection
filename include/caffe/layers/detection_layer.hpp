#ifndef CAFFE_DETECTION_LAYER_HPP
#define CAFFE_DETECTION_LAYER_HPP

#include "caffe/blob.hpp"
#include "caffe/layer.hpp"
#include "caffe/proto/caffe.pb.h"
#include <vector>
#include <opencv2/core.hpp>
#include <numeric>

namespace caffe {

template <typename Dtype>
class DetectionLayer : public Layer<Dtype> {
 public:
  explicit DetectionLayer(const LayerParameter& param)
    : Layer<Dtype>(param) {}
  virtual void LayerSetUp(const vector<Blob<Dtype>*>& bottom, 
      const vector<Blob<Dtype>*>& top);
  virtual void Reshape(const vector<Blob<Dtype>*>& bottom, 
      const vector<Blob<Dtype>*>& top);
  virtual inline const char* type() const { return "Detection"; }
  virtual inline int ExactNumBottomBlobs() const { return 4; }
  virtual inline int ExactNumTopBlobs() const { return 1; }
 protected:
  virtual void Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  // virtual void Forward_gpu(const vector<Blob<Dtype>*>& bottom,
  //     const vector<Blob<Dtype>*>& top) {
  //   Forward_cpu(bottom, top);    
  // }
  virtual void Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {}
  // virtual void Backward_gpu(const vector<Blob<Dtype>*>& top,
  //     const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {}
  
  virtual cv::Mat_<Dtype> bbox_transform_inv(const cv::Mat_<Dtype>& boxes, 
      const cv::Mat_<Dtype>& box_deltas);
  virtual cv::Mat_<Dtype> nms_vote(const cv::Mat_<Dtype>& cls_dets, 
      const Dtype& thresh, bool vote);
  virtual cv::Mat_<Dtype> soft_nms(const cv::Mat_<Dtype>& cls_dets, 
    const Dtype& thresh);

  int num_classes_;
  vector<Dtype> threshs_;
  vector<Dtype> nmss_;
  vector<Dtype> im_shape_;
};

}




#endif