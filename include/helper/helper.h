#pragma once

#include <set>
#include <map>
#include <direct.h>
#include <numeric>
#include "boost\filesystem.hpp"
#include "opencv2\opencv.hpp"

#define DO_DESCRIPTION(e)  #e,
#define DO_ENUM(e)  e,

#define MAKE_ENUM_NAME(e) e ## Name
#define MAKE_ENUM(e) e ## Enum
#define ENUM_SIZE(e) (*(&MAKE_ENUM_NAME(e) + 1) - MAKE_ENUM_NAME(e))
#define TO_ENUM(str, e) \
[&]()->MAKE_ENUM(e) { \
	int i = 0; \
	for (; i < ENUM_SIZE(e); i++) { \
		if (strcmp(MAKE_ENUM_NAME(e)[i], str) == 0) \
			break; \
	} \
	return (MAKE_ENUM(e))i; \
}()

//////////
// 
static std::string to_imgName(const int frmNum)
{
	char s[20];
	sprintf_s(s, "img%06d.jpg", frmNum);
	return s;
}

static std::string to_imgLabel(const std::string & raceLabel, const int frmNum)
{
	return raceLabel + "/" + to_imgName(frmNum);
}

//////////
// Helper Functions for String Operation

// Split a single string to a string array by the char c
static std::vector<std::string> split_string(const std::string & str, const char c)
{
	std::vector<std::string> strings;
	std::string str_temp = str;
	while (true) {
		const int idx = (int)str_temp.find(c);

		if (idx == -1) {
			strings.push_back(str_temp);
			break;
		}
		else if (idx == 0)
			strings.push_back("");
		else
			strings.push_back(str_temp.substr(0, idx));

		if (idx == str_temp.length() - 1)
			break;

		str_temp = str_temp.substr(idx + 1);
	}
	if (strings.back().empty())
		strings.pop_back();
	return strings;
}

// Compine a string array to a single string by the char c
static std::string combine_strings(const std::vector<std::string> & strs, const char c)
{
	std::string str = "";
	for (int i = 0; i < strs.size(); i++)
		str += strs[i] + c;
	return str.substr(0, str.size() - 1);
}

static char* to_charPtr(const std::string & str)
{
	return strcpy(new char[1000], str.c_str());
}

// compare string with intelligence for numbers inside
static int string_cmp(const std::string & str0, const std::string & str1)
{
	int i0 = 0, i1 = 0;
	while (i0 < str0.size() && i1 < str1.size())
	{
		// check if both number
		if (isdigit(str0[i0]) && isdigit(str1[i1])) {
			// assemble the number in both string
			int n0 = 0;
			while (isdigit(str0[i0])) {
				n0 = n0 * 10 + str0[i0++] - '0';
			}

			int n1 = 0;
			while (isdigit(str1[i1])) {
				n1 = n1 * 10 + str1[i1++] - '0';
			}

			// compare converted numbers
			if (n0 < n1)
				return -1;
			if (n0 > n1)
				return 1;
		}
		else {
			// compare as char
			if (str0[i0] < str1[i1])
				return -1;
			if (str0[i0] > str1[i1])
				return 1;

			i0++, i1++;
		}
	}

	// check if both finished
	if (i0 < str0.size())
		return 1;
	if (i1 < str1.size())
		return -1;

	// all the same return 0
	return 0;
}

struct string_less_cmp
{
	string_less_cmp(const std::vector<std::string> & varr) : arr(varr) {}
	inline bool operator()(const size_t a, const size_t b) const
	{
		return string_cmp(arr[a], arr[b]) <= 0;
	}
	const std::vector<std::string> arr;
};

struct string_larger_cmp
{
	string_larger_cmp(const std::vector<std::string> & varr) : arr(varr) {}
	inline bool operator()(const size_t a, const size_t b) const
	{
		return string_cmp(arr[a], arr[b]) >= 0;
	}
	const std::vector<std::string> arr;
};

static void sort_strings_index(const std::vector<std::string> & vals, std::vector<int> & idx, const bool bAscending = true)
{
	if (vals.empty())
		return;

	if (idx.empty() || idx.size() != vals.size())
	{
		idx.clear();
		for (int i = 0; i < (int)vals.size(); idx.push_back(i++));
	}

	if (bAscending)
		sort(idx.begin(), idx.end(), string_less_cmp(vals));
	else
		sort(idx.begin(), idx.end(), string_larger_cmp(vals));
}

template <typename T>
static std::vector<int> get_sorting_indices(const std::vector<T> & vec)
{
	std::vector<int> indices;
	for (int i = 0; i < vec.size(); i++)
		indices.push_back(i);
	sort(indices.begin(), indices.end(), [&](int i, int j) { return vec[i] < vec[j]; });
	return indices;
}

template <typename T, typename CallbackFunction>
static std::vector<int> get_sorting_indices(const std::vector<T> & vec, const CallbackFunction & f)
{
	std::vector<int> indices;
	for (int i = 0; i < vec.size(); i++)
		indices.push_back(i);
	sort(indices.begin(), indices.end(), [&](int i, int j) { return f(vec[i]) < f(vec[j]); });
	return indices;
}

//////////
// Helper Functions for Reading CSV

#define READ_CSV_LINE_LENGTH 100000

// Read a .csv file
static std::ifstream& operator>>(std::ifstream & ifs, std::vector<std::vector<std::string>> & csvContent)
{
	// Get csv content
	csvContent.clear();
	while (!ifs.eof()) {
		// Get line
		char* l = new char[READ_CSV_LINE_LENGTH];
		ifs.getline(l, READ_CSV_LINE_LENGTH);

		// Break if line != empty
		if (l[0] == '\0')
			break;

		// Update Strings
		const std::vector<std::string> strings = split_string(l, ',');
		csvContent.push_back(strings);

		delete l;
	}

	return ifs;
}

static std::vector<std::vector<std::string>> parse_csv(const std::string & csv_path)
{
	std::vector<std::vector<std::string>> content;
	std::ifstream ifs(csv_path);
	if (!ifs.is_open())
		throw cv::format("Unable to parse csv: %s", csv_path.c_str()).c_str();
	ifs >> content;
	ifs.close();
	return content;
}

static void parse_csv(const std::string & csv_path, std::vector<std::string> & headers, std::vector<std::vector<std::string>> & content)
{
	std::vector<std::vector<std::string>> csv_cells;
	std::ifstream ifs(csv_path);
	CV_Assert(ifs.is_open());
	ifs >> csv_cells;
	ifs.close();
	CV_Assert(!csv_cells.empty());
	headers = csv_cells[0];
	content = std::vector<std::vector<std::string>>(csv_cells.begin() + 1, csv_cells.end());
}

////////
// Helper Functions for Vector Operation

template <typename T>
static bool is_unique(const std::vector<T> & vec)
{
	std::set<T> V;
	for (T x : vec)
		V.insert(x);
	return V.size() == vec.size();
}

template <typename T, typename ITER, typename CallbackFunction>
static std::vector<T> to_vector(const ITER & it_begin,
	const ITER & it_end,
	const CallbackFunction & f
	)
{
	std::vector<T> vec;
	for (ITER it = it_begin; it != it_end; it++) {
		const T x = f(*it);
		vec.push_back(x);
	}
	return vec;
}

// ITER             - std::vector<T>::iterator
// CallbackFunction - std::function<U(std::vector<T>::iterator)>
template <typename ITER, typename CallbackFunction>
static bool is_unique(const ITER & it_begin,
	const ITER & it_end,
	const CallbackFunction & f
	)
{
	typedef decltype(f(*it_begin)) T;
	const std::vector<T> vec = to_vector<T>(it_begin, it_end, f);
	return is_unique(vec);
}

static enum SortType { ASCENDING, DESCENDING, STRICTLY_ASCENDING, STRICITLY_DESCENDING };

// ITER             - std::vector<T>::iterator
// CallbackFunction - std::function<U(std::vector<T>::iterator)>
template <typename ITER, typename CallbackFunction>
static bool is_sorted(const ITER & it_begin,
	const ITER & it_end,
	const CallbackFunction & f,
	const SortType sortType
	)
{
	typedef decltype(f(*it_begin)) T;

	for (ITER it = it_begin; it != it_end - 1; it++) {
		const T x = f(*it);
		const T y = f(*(it + 1));
		switch (sortType) {
		case ASCENDING:
			if (!(x <= y)) return false;
			break;
		case DESCENDING:
			if (!(x >= y)) return false;
			break;
		case STRICTLY_ASCENDING:
			if (!(x < y)) return false;
			break;
		case STRICITLY_DESCENDING:
			if (!(x > y)) return false;
			break;
		default:
			throw "Unhandled sorting operator";
		}
	}
	return true;
}

template <typename ITER, typename CallbackFunction, typename T>
static ITER find(const ITER & it_begin,
	const ITER & it_end,
	const CallbackFunction & f,
	const T & x
	)
{
	for (ITER it = it_begin; it != it_end; it++) {
		if (f(*it) == x)
			return it;
	}
	return it_end;
}

template<typename FuncDomain>
static bool validate_domain(const std::vector<FuncDomain> & D)
{
	std::set<FuncDomain> D_set;
	for (int i = 0; i < D.size(); i++)
		D_set.insert(D[i]);
	return D.size() == D_set.size();
}

template<typename X>
static X interpolate(const X & x1,
	const X & x2,
	const float alpha
	)
{
	return x1 * (1 - alpha) + x2 * alpha;
}

static cv::Point interpolate(const cv::Point & x1,
	const cv::Point & x2,
	const float alpha
	)
{
	return cv::Point((int)round(x1.x * (1 - alpha) + x2.x * alpha),
		(int)round(x1.y * (1 - alpha) + x2.y * alpha)
		);
}

static cv::Size interpolate(const cv::Size & x1,
	const cv::Size & x2,
	const float alpha
	)
{
	return cv::Size((int)round(x1.width * (1 - alpha) + x2.width * alpha),
		(int)round(x1.height * (1 - alpha) + x2.height * alpha)
		);
}

template<typename FuncDomain, typename FuncRange>
static std::vector<FuncRange> passRangeWithInterpolation(const std::vector<FuncDomain> & D1,
	const std::vector<FuncRange> & R1,
	const std::vector<FuncDomain> & D2, 
	const int max_dist = INT_MAX
	)
{
	if (D1.size() != R1.size())
		throw "Mismatch sizes of D1 and R1";
	if (!validate_domain(D1))
		throw "D1 is not valid";
	if (!validate_domain(D2))
		throw "D2 is not valid";

	std::vector<FuncRange> R2;
	std::set<int> indices;
	passRange(D1, R1, D2, R2, indices);

	for (std::set<int>::iterator it = indices.begin(); it != prev(indices.end()); it++) {
		const int idx1 = *it;
		const int idx2 = *(next(it));
		CV_Assert(idx2 > idx1 && idx2 < D2.size());
		if (idx2 - idx1 > max_dist)
			continue;
		for (int i = idx1 + 1; i < idx2; i++) {
			const int dt = i - idx1;
			const float alpha = (float)dt / (idx2 - idx1);
			R2[i] = interpolate(R2[idx1], R2[idx2], alpha);
		}
	}

	return R2;
}

template<typename T>
static void interpolation(std::vector<T> & R, const int max_dist = INT_MAX)
{
	if (R.size() <= 1)
		return;
	std::vector<int> indices;
	for (int i = 0; i < R.size(); i++)
		indices.push_back(i);
	R = passRangeWithInterpolation(indices, R, indices, max_dist);
}

template<typename FuncDomain, typename FuncRange>
static std::vector<FuncRange> passRange(const std::vector<FuncDomain> & D1,
										const std::vector<FuncRange> & R1,
										const std::vector<FuncDomain> & D2
										)
{
	if (D1.size() != R1.size())
		throw "Mismatch sizes of D1 and R1";
	if (!validate_domain(D1))
		throw "D1 is not valid";
	if (!validate_domain(D2))
		throw "D2 is not valid";

	std::vector<FuncRange> R2(D2.size());
	for (int i = 0; i < D1.size(); i++) {
		const FuncDomain x = D1[i];
		const FuncRange y = R1[i];
		const auto it = find(D2.begin(), D2.end(), x);
		if (it == D2.end() || y == FuncRange())
			continue;
		const int idx = distance(D2.begin(), it);
		R2[idx] = y;
	}
	return R2;
}

template<typename FuncDomain, typename FuncRange>
static void passRange(const std::vector<FuncDomain> & D1,
	const std::vector<FuncRange> & R1,
	const std::vector<FuncDomain> & D2,
	std::vector<FuncRange> & R2,
	std::set<int> & indices
	)
{
	if (D1.size() != R1.size())
		throw "Mismatch sizes of D1 and R1";
	if (!validate_domain(D1))
		throw "D1 is not valid";
	if (!validate_domain(D2))
		throw "D2 is not valid";

	std::vector<FuncRange> other_R2(D2.size());
	std::set<int> other_indices;
	for (int i = 0; i < D1.size(); i++) {
		const FuncDomain x = D1[i];
		const FuncRange y = R1[i];
		const auto it = find(D2.begin(), D2.end(), x);
		if (it == D2.end() || y == FuncRange())
			continue;
		const int idx = distance(D2.begin(), it);
		other_R2[idx] = y;
		other_indices.insert(idx);
	}

	R2 = other_R2;
	indices = other_indices;
}

template<typename T, typename EleProjFunc>
static float __avg(const std::vector<T> & v, const EleProjFunc & f)
{
	float sum = 0;
	for (T x : v)
		sum += f(x);
	return sum / v.size();
}

template<typename T, typename U, typename EleProjFunc>
static float __avg(const std::map<T, U> & v, const EleProjFunc & f)
{
	float sum = 0;
	for (std::pair<T, U> x : v)
		sum += f(x);
	return sum / v.size();
}

template<typename T, typename EleProjFunc>
static float __stdev(const std::vector<T> & v, const EleProjFunc & f)
{
	const float mean = __avg(v, f);
	float sum = 0;
	for (T x : v)
		sum += (f(x) - mean) * (f(x) - mean);
	return sqrt(sum / v.size());
}

template<typename T, typename U, typename EleProjFunc>
static float __stdev(const std::map<T, U> & v, const EleProjFunc & f)
{
	const float mean = __avg(v, f);
	float sum = 0;
	for (std::pair<T, U> x : v)
		sum += (f(x) - mean) * (f(x) - mean);
	return sqrt(sum / v.size());
}

////////
// Helper Functions for Directory/Path Operation

// Find a file in a directory
// output: paths_found - all the paths found
// return false if the directory not exist
static bool find_file(const boost::filesystem::path & dir,
	const std::string & filename,
	std::vector<boost::filesystem::path> & paths_found
	)
{
	if (!exists(dir))
		return false;
	boost::filesystem::directory_iterator end_itr;
	for (boost::filesystem::directory_iterator itr(dir); itr != end_itr; ++itr) {
		if (is_directory(itr->status()))
			find_file(itr->path(), filename, paths_found);
		else if (itr->path().filename().string() == filename)
			paths_found.push_back(itr->path());
	}
	return true;
}

static void create_dir(const boost::filesystem::path & dir)
{
	boost::filesystem::path parent_path = dir.parent_path();

	if (!parent_path.empty() && !boost::filesystem::is_directory(parent_path))
		create_dir(parent_path);

	const bool bFailed = _mkdir(dir.string().c_str());
	if (bFailed && errno == ENOENT) {
		std::cout << "Unable to create directory: " << dir << std::endl;
		CV_Assert(!(bFailed && errno == ENOENT));
	}
}

// Create directory recursively
static void create_dir(const std::string & dir)
{
	create_dir((boost::filesystem::path)dir);
}

// Create directory recursively
static void create_dir(const char * dir)
{
	create_dir((boost::filesystem::path)dir);
}

////////
// Helper Functions for OpenCV Operation

// Resize a Rect
static enum ResizeFlag { RESIZE_FLOOR, RESIZE_ROUND };

template <typename R>
static void resizeRect(const R & src,
	R & dst,
	const float ratio,
	const ResizeFlag flag
	)
{
	switch (flag)
	{
	case RESIZE_FLOOR:
		dst = R(src.x * ratio, src.y * ratio, src.width * ratio, src.height * ratio);
		break;
	case RESIZE_ROUND:
		dst = R(round(src.x * ratio), round(src.y * ratio), round(src.width * ratio), round(src.height * ratio));
		break;
	default:
		break;
	}
}

////////
// Helper Functions for Video MD5

#include "MD5.h"
static std::string get_videoMD5(const std::string & video_path)
{
	// Open VideoCapture
	cv::VideoCapture cap(video_path);
	// Get video properties
	const int w = (int)round(cap.get(CV_CAP_PROP_FRAME_WIDTH));
	const int h = (int)round(cap.get(CV_CAP_PROP_FRAME_HEIGHT));
	const int fps = (int)round(cap.get(CV_CAP_PROP_FPS));
	const int fourCC = (int)round(cap.get(CV_CAP_PROP_FOURCC));
	const int nFrms = (int)round(cap.get(CV_CAP_PROP_FRAME_COUNT));
	// Get first frame
	cv::Mat img;
	cap >> img;
	// Release VideoCapture
	cap.release();
	// Convert the frame to gray-scaled
	char * buffer = new char[h * w * 3];
	cv::Mat grayImg(h, w, CV_8UC1, buffer);
	cv::cvtColor(img, grayImg, CV_BGR2GRAY);
	// Set '\0' to 1
	grayImg.setTo(1, grayImg == 0);
	// Convert buffer and video properties to a string
	const std::string videoStr = buffer + std::to_string(w) + std::to_string(h) + std::to_string(fps) + std::to_string(fourCC) + std::to_string(nFrms);
	// Return md5
	return get_MD5(videoStr);
}


////////
// Helper Functions for Race Configuration Operation

static std::string to_raceLabel(const int raceID)
{
	char raceLabel[100];
	sprintf_s(raceLabel, "%04d", raceID);
	return raceLabel;
}

// Read images
static std::vector<cv::Mat> readAllImgs(const std::string & dir,
										const int startFrmNum,
										const int endFrmNum,
										const cv::Size & frmImgSz = cv::Size(),
										const bool bAllowResize = false,
										const int flags = 1
										)
{
	std::cout << "Reading all frames" << std::endl;

	std::vector<cv::Mat> frmImg_stack(endFrmNum - startFrmNum + 1);
#pragma omp parallel for
	for (int frmNum = startFrmNum; frmNum <= endFrmNum; frmNum++) {
		const int frmID = frmNum - startFrmNum;
		const std::string imgLabel = to_imgName(frmNum);
		const std::string imgPath = dir + "/" + imgLabel;
		const cv::Mat frmImg = cv::imread(imgPath, flags);
		frmImg_stack[frmID] = frmImg;
	}

	for (cv::Mat frmImg : frmImg_stack) {
		CV_Assert(!frmImg.empty());
		CV_Assert(bAllowResize || frmImgSz == cv::Size() || (!bAllowResize && frmImg.size() == frmImgSz));
	}

	for (int i = 0; i < frmImg_stack.size(); i++)
		if (frmImgSz != cv::Size() && frmImg_stack[i].size() != frmImgSz)
			resize(frmImg_stack[i], frmImg_stack[i], frmImgSz);

	std::cout << "Reading all frames - done" << std::endl;

	return frmImg_stack;
}

// Read images by the a list of requesting frame numbers
static std::vector<cv::Mat> readAllImgs(const std::string & dir,
										const std::vector<int> frmNums, 
										const cv::Size & frmImgSz = cv::Size(),
										const bool bAllowResize = false,
										const int flags = 1
										)
{
	std::cout << "Reading all frames" << std::endl;

	std::vector<cv::Mat> frmImgs(frmNums.size());
#pragma omp parallel for
	for (int i = 0; i < frmNums.size(); i++) {
		const int frmNum = frmNums[i];
		const std::string imgPath = dir + "/" + to_imgName(frmNum);
		const cv::Mat frmImg = cv::imread(imgPath, flags);
		frmImgs[i] = frmImg;
	}

	for (cv::Mat frmImg : frmImgs) {
		CV_Assert(!frmImg.empty());
		CV_Assert(bAllowResize || frmImgSz == cv::Size() || (!bAllowResize && frmImg.size() == frmImgSz));
	}

	for (int i = 0; i < frmImgs.size(); i++)
		if (frmImgSz != cv::Size() && frmImgs[i].size() != frmImgSz)
			resize(frmImgs[i], frmImgs[i], frmImgSz);

	std::cout << "Reading all frames - done" << std::endl;

	return frmImgs;
}

////////
// Helper Functions for 

static cv::Point2f transformPoint(const cv::Mat & H, const double p[3])
{
	double p_temp[3] = { p[0], p[1], p[2] };
	const cv::Mat pt(3, 1, CV_64FC1, p_temp);
	const cv::Mat pt_dst = H * pt;
	return cv::Point2f((float)(pt_dst.at<double>(0, 0) / pt_dst.at<double>(2, 0)), (float)(pt_dst.at<double>(1, 0) / pt_dst.at<double>(2, 0)));
}

static cv::Point2f transformPoint(const cv::Mat & H, const cv::Point2f & pt)
{
    const double p[3] = { pt.x, pt.y, 1 };
    return transformPoint(H, p);
}

// Draw line on a given canvas
static void drawLine(cv::Mat & canvas,
	const cv::Point2f & l,
	const cv::Point & lineShift,
	const cv::Scalar & color = cv::Scalar(0, 255, 0),
	const int lineWidth = 3
	)
{
	CV_Assert(!canvas.empty());
	CV_Assert(canvas.type() == CV_8UC1 || canvas.type() == CV_8UC3);

	const float c = l.x;
	const float m = l.y;
	const cv::Point pt1(0, (int)round(m * 0 + c));
	const cv::Point pt2(canvas.cols - 1, (int)round(m * (canvas.cols - 1) + c));
	line(canvas, pt1 + lineShift, pt2 + lineShift, color, lineWidth);
}
