#pragma once

#include "opencv2/opencv.hpp"
#include "helper/Module.h"
#include "Subrace.h"

#define Track(FOO) \
	FOO(HVT) \
	FOO(STT) \
	FOO(STAWT) \
	FOO(Kranji) \
	FOO(KranjiS) \
	FOO(KranjiL) \
	FOO(KranjiP) \
	FOO(Chukyo) \
	FOO(Fukushima) \
	FOO(Nakayama) \
	FOO(Hakodate) \
	FOO(Hanshin) \
	FOO(Kokura) \
	FOO(Kyoto) \
	FOO(Niigata) \
	FOO(Sapporo) \
	FOO(Tokyo) \
	FOO(UndefinedTrack)

// Define TrackEnum as enum
static enum MAKE_ENUM(Track) { Track(DO_ENUM) };
// Define TrackName as char * array
static char* MAKE_ENUM_NAME(Track)[] = { Track(DO_DESCRIPTION) };
// Implicily define TO_ENUM(Track, char *)

#define Location(FOO) \
	FOO(HongKong) \
	FOO(Singapore) \
	FOO(Japan) \
	FOO(UndefinedLocation)

// Define LocationEnum as enum
static enum MAKE_ENUM(Location) { Location(DO_ENUM) };
// Define LocationName as char * array
static char* MAKE_ENUM_NAME(Location)[] = { Location(DO_DESCRIPTION) };
// Implicily define TO_ENUM(LocationEnum, char *)

struct Race {
	// Label
	std::string raceLabel;

	// General configuration (known)
	TrackEnum track = UndefinedTrack;
	int date;
	std::string course;

	// General configuration (unknown; updated by system)
	int raceNum;
	int distance;
	std::string raceTime;

	// Video configuration
	int fps;
	int startFrmNum;
	int endFrmNum;
	std::map<int, Subrace> subraces;

	// Frame configuration
	cv::Size frmImgSz = cv::Size(1920, 1080);
	cv::Size smallFrmImgSz = cv::Size(640, 360);
	std::vector<int> frmNum_stack;
	std::vector<cv::Mat> frmImg_stack;
	std::vector<cv::Mat> smallFrmImg_stack;

	// Get nFrms
	int get_nFrms() const { return endFrmNum - startFrmNum + 1; }

    // Get nSubraces
    int get_nSubraces() const { return subraces.size(); }

	// Get location
	inline LocationEnum get_location() const;

	// Print
	inline void print() const;
};

inline LocationEnum Race::get_location() const
{
	switch (this->track) {
	case HVT:
	case STT:
	case STAWT:
		return HongKong;
	case Kranji:
	case KranjiS:
	case KranjiL:
	case KranjiP:
		return Singapore;
	case Chukyo:
	case Fukushima:
	case Nakayama:
	case Hakodate:
	case Hanshin:
	case Kokura:
	case Kyoto:
	case Niigata:
	case Sapporo:
	case Tokyo:
		return Japan;
	default:
		return UndefinedLocation;
	}
}

inline void Race::print() const
{
	std::cout << std::endl;
	std::cout << "====== Race ======" << std::endl;
	std::cout << "raceLabel     = " << raceLabel << std::endl;
	std::cout << "date          = " << date << std::endl;
	std::cout << "location      = " << LocationName[this->get_location()] << std::endl;
	std::cout << "track         = " << TrackName[track] << std::endl;
	std::cout << "distance      = " << distance << std::endl;
	std::cout << "course        = " << course << std::endl;
	std::cout << "frmImgSz      = " << frmImgSz << std::endl;
	std::cout << "smallFrmImgSz = " << smallFrmImgSz << std::endl;
	std::cout << "startFrmNum   = " << startFrmNum << std::endl;
	std::cout << "endFrmNum     = " << endFrmNum << std::endl;
	std::cout << "fps           = " << fps << std::endl;
	std::cout << "==================" << std::endl;
	std::cout << std::endl;
}