#pragma once
#include "opencv2/opencv.hpp"

struct Subrace {
	int nFrms;
	int startFrmNum;
	int endFrmNum;
	int startFrmID;
	int endFrmID;

	cv::Mat stitching;
	cv::Mat stitching_model;
    std::vector<cv::Mat> H_stack_to_stitching;					// the ith element is the homography from frame ID #i to stitching
	cv::Mat proj_stitching;
	cv::Mat H_proj_stitching;									// the homography from stitching to model using key objects
	std::map<int, cv::Rect> keyObjects_on_Stitching;
	std::map<int, std::vector<int>> corresponding_Frames_Number;
	std::map<int, double > corresponding_Scores;
	std::map<int, std::vector<cv::Rect>> keyObjects_on_Frames;
};

// Cout a subrace
static std::ostream& operator<<(std::ostream & os, const Subrace a)
{
	return os	<< "nFrms = " << a.nFrms << ", "
				<< "frmNum = (" << a.startFrmNum << ", " << a.endFrmNum << "), "
				<< "frmID = (" << a.startFrmID << ", " << a.endFrmID << ")"
				<< std::endl;
}