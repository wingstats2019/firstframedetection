#pragma once

#include "helper/helper.h"

#define Module(FOO) \
	FOO(GraphicsRecognition) \
	FOO(VideoProcessor) \
	FOO(OpticalFlow) \
	FOO(CameraChange) \
	FOO(JKCP) \
	FOO(JKCP_Detection) \
	FOO(JKCP_Tracking) \
	FOO(RailDetection) \
	FOO(Localisation) \
    FOO(KeyObjectDetectionOld) \
    FOO(KeyObjectDetection) \
    FOO(Stitching) \
    FOO(RaceCourse)

static enum MAKE_ENUM(Module) { Module(DO_ENUM) };
static char* MAKE_ENUM_NAME(Module)[] = { Module(DO_DESCRIPTION) };

namespace graphics_recognition { const ModuleEnum module = GraphicsRecognition; }
namespace video_processor { const ModuleEnum module = VideoProcessor; }
namespace optical_flow { const ModuleEnum module = OpticalFlow; }
namespace camera_change { const ModuleEnum module = CameraChange; }
namespace jkcp { const ModuleEnum module = JKCP; }
namespace jkcp { namespace detection { const ModuleEnum module = JKCP_Detection; } }
namespace jkcp { namespace tracking { const ModuleEnum module = JKCP_Tracking; } }
namespace rail_detection { const ModuleEnum module = RailDetection; }
namespace localisation { const ModuleEnum module = Localisation; }
namespace key_object_detection_old { const ModuleEnum module = KeyObjectDetectionOld; }
namespace key_object_detection { const ModuleEnum module = KeyObjectDetection; }
namespace stitching { const ModuleEnum module = Stitching; }
namespace race_course { const ModuleEnum module = RaceCourse; }
