#pragma once

#include "opencv2/opencv.hpp"

// cv::FileStorage functions for save std::vector<...> (... have ambiguous size)
template <typename T>
static inline cv::FileStorage & operator << (cv::FileStorage & fs, const std::vector<std::vector<T>> & vecs);

// cv::FileStorage functions for read std::vector<...> (... have ambiguous size)
template <typename T>
static inline void operator >> (const cv::FileNode& node, std::vector<std::vector<T>>& vecs);

// cv::FileStorage functions to write cv::RotatedRect
static inline cv::FileStorage & operator << (cv::FileStorage & fs, const cv::RotatedRect & ell);
static inline cv::FileStorage & operator << (cv::FileStorage & fs, const std::vector<cv::RotatedRect> & ellipses);

// cv::FileStorage functions to read cv::RotatedRect
static inline void operator >> (const cv::FileNode& node, cv::RotatedRect & vec);
static inline void operator >> (const cv::FileNode& node, std::vector<cv::RotatedRect> & vecs);

// cv::FileStorage functions to write/read std::vector<cv::Mat>
static inline cv::FileStorage& operator << (cv::FileStorage& fs, const std::vector<cv::Mat> & matrixes);
static inline void operator >> (const cv::FileNode& node, std::vector<cv::Mat> & vecs);

// cv::FileStorage functions to write/read std::vector<std::string>
static inline cv::FileStorage& operator << (cv::FileStorage& fs, const std::vector<std::string> & matrixes);
static inline void operator >> (const cv::FileNode& node, std::vector<std::string> & vecs);

// cv::FileStorage functions to write/read cv::RotatedRect
static inline cv::FileStorage & operator << (cv::FileStorage & fs, const cv::PCA & pca);
static inline void operator >> (const cv::FileNode& node, cv::PCA & pca);

// cv::FileStorage functions for std::vector<std::vector<int>>
template <typename T>
inline cv::FileStorage & operator << (cv::FileStorage & fs, const std::vector<std::vector<T>> & vecs)
{
	fs << "{" << "number" << (int)vecs.size();

	for (size_t i = 0; i < vecs.size(); i++)
	{
		std::ostringstream idx;
		idx << "idx-" << i;
		fs << idx.str() << vecs[i];
	}

	fs << "}";
	return fs;
}
template cv::FileStorage & operator << (cv::FileStorage & fs, const std::vector<std::vector<int>> & vecs);
template cv::FileStorage & operator << (cv::FileStorage & fs, const std::vector<std::vector<float>> & vecs);
template cv::FileStorage & operator << (cv::FileStorage & fs, const std::vector<std::vector<cv::Point>> & vecs);
template cv::FileStorage & operator << (cv::FileStorage & fs, const std::vector<std::vector<cv::Point2f>> & vecs);

// cv::FileStorage functions for read std::vector<...> (... have ambiguous size)
template <typename T>
inline void operator >> (const cv::FileNode& node, std::vector<std::vector<T>>& vecs)
{
	int num = node["number"];
	vecs.resize(num);
	for (size_t i = 0; i < num; i++)
	{
		std::ostringstream idx;
		idx << "idx-" << i;
		node[idx.str()] >> vecs[i];
	}
}
template void operator >> (const cv::FileNode& node, std::vector<std::vector<int>>& vecs);
template void operator >> (const cv::FileNode& node, std::vector<std::vector<float>>& vecs);
template void operator >> (const cv::FileNode& node, std::vector<std::vector<cv::Point>>& vecs);
template void operator >> (const cv::FileNode& node, std::vector<std::vector<cv::Point2f>>& vecs);

// cv::FileStorage function to write cv::RotatedRect
inline cv::FileStorage& operator << (cv::FileStorage& fs, const cv::RotatedRect & ell)
{
	fs << "{" << "x" << ell.center.x << "y" << ell.center.y;
	fs << "angle" << ell.angle << "width" << ell.size.width << "height" << ell.size.height << "}";
	return fs;
}

inline cv::FileStorage& operator << (cv::FileStorage& fs, const std::vector<cv::RotatedRect> & ellipses)
{
	fs << "{" << "number" << (int)ellipses.size();

	for (size_t i = 0; i < ellipses.size(); i++)
	{
		std::ostringstream idx;
		idx << "idx-" << i;
		fs << idx.str() << ellipses[i];
	}

	fs << "}";
	return fs;
}

// cv::FileStorage functions to read cv::RotatedRect
inline void operator >> (const cv::FileNode& node, cv::RotatedRect & item)
{
	node["x"] >> item.center.x;
	node["y"] >> item.center.y;
	node["angle"] >> item.angle;
	node["width"] >> item.size.width;
	node["height"] >> item.size.height;
}

inline void operator >> (const cv::FileNode& node, std::vector<cv::RotatedRect> & vecs)
{
	int num = node["number"];
	vecs.resize(num);
	for (size_t i = 0; i < num; i++)
	{
		std::ostringstream idx;
		idx << "idx-" << i;
		node[idx.str()] >> vecs[i];
	}
}

// cv::FileStorage functions to write std::vector<cv::Mat>
inline cv::FileStorage& operator << (cv::FileStorage& fs, const std::vector<cv::Mat> & matrixes)
{
	fs << "{" << "number" << (int)matrixes.size();

	for (size_t i = 0; i < matrixes.size(); i++)
	{
		std::ostringstream idx;
		idx << "idx-" << i;
		fs << idx.str() << matrixes[i];
	}

	fs << "}";
	return fs;
}

// cv::FileStorage functions to read std::vector<cv::Mat>
inline void operator >> (const cv::FileNode& node, std::vector<cv::Mat> & vecs)
{
	int num = node["number"];
	vecs.resize(num);
	for (size_t i = 0; i < num; i++)
	{
		std::ostringstream idx;
		idx << "idx-" << i;
		node[idx.str()] >> vecs[i];
	}
}

// cv::FileStorage functions to write std::vector<std::string>
inline cv::FileStorage& operator << (cv::FileStorage& fs, const std::vector<std::string> & matrixes)
{
	fs << "{" << "number" << (int)matrixes.size();

	for (size_t i = 0; i < matrixes.size(); i++)
	{
		std::ostringstream idx;
		idx << "idx-" << i;
		fs << idx.str() << matrixes[i];
	}

	fs << "}";
	return fs;
}

// cv::FileStorage functions to read std::vector<std::string>
inline void operator >> (const cv::FileNode& node, std::vector<std::string> & vecs)
{
	int num = node["number"];
	vecs.resize(num);
	for (size_t i = 0; i < num; i++)
	{
		std::ostringstream idx;
		idx << "idx-" << i;
		node[idx.str()] >> vecs[i];
	}
}

// cv::FileStorage functions to write/read cv::RotatedRect
inline cv::FileStorage & operator << (cv::FileStorage & fs, const cv::PCA & pca)
{
	fs << "{";
	fs << "mean" << pca.mean;
	fs << "eigenvalues" << pca.eigenvalues;
	fs << "eigenvectors" << pca.eigenvectors;
	fs << "}";
	return fs;
}

inline void operator >> (const cv::FileNode& node, cv::PCA & pca)
{
	node["mean"] >> pca.mean;
	node["eigenvalues"] >> pca.eigenvalues;
	node["eigenvectors"] >> pca.eigenvectors;
}
