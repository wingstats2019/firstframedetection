#pragma once

#include <functional>

class CmShow
{
public:
	static inline cv::Mat HistBins(const cv::Mat& color3f, const cv::Mat& val, const std::string& title, bool descendShow = false, const cv::Mat &with = cv::Mat());
	static inline void showTinyMat(const std::string &title, const cv::Mat &m);
	static inline void SaveShow(const cv::Mat& img, const std::string& title);
};

typedef std::pair<int, int> CostiIdx;
inline cv::Mat CmShow::HistBins(const cv::Mat& color3f, const cv::Mat& val, const std::string& title, bool descendShow, const cv::Mat &with)
{
	// Prepare data
	int H = 300, spaceH = 6, barH = 10, n = color3f.cols;
	CV_Assert(color3f.size() == val.size() && color3f.rows == 1);
	cv::Mat binVal1i, binColor3b, width1i;
	if (with.size() == val.size())
		with.convertTo(width1i, CV_32S, 400 / sum(with).val[0]); // Default shown width
	else
		width1i = cv::Mat(1, n, CV_32S, cv::Scalar(10)); // Default bin width = 10
	int W = cvRound(sum(width1i).val[0]);
	color3f.convertTo(binColor3b, CV_8UC3, 255);
	double maxVal, minVal;
	minMaxLoc(val, &minVal, &maxVal);
	printf("%g\n", H / std::max(maxVal, -minVal));
	val.convertTo(binVal1i, CV_32S, 20000);
	cv::Size szShow(W, H + spaceH + barH);
	szShow.height += minVal < 0 && !descendShow ? H + spaceH : 0;
	cv::Mat showImg3b(szShow, CV_8UC3, cv::Scalar(255, 255, 255));
	int* binH = (int*)(binVal1i.data);
	cv::Vec3b* binColor = (cv::Vec3b*)(binColor3b.data);
	int* binW = (int*)(width1i.data);
	std::vector<CostiIdx> costIdx(n);
	if (descendShow){
		for (int i = 0; i < n; i++)
			costIdx[i] = { binH[i], i };
		sort(costIdx.begin(), costIdx.end(), std::greater<CostiIdx>());
	}

	// Show image
	for (int i = 0, x = 0; i < n; i++){
		int idx = descendShow ? costIdx[i].second : i;
		int h = descendShow ? abs(binH[idx]) : binH[idx];
		cv::Scalar color(binColor[idx]);
		cv::Rect reg(x, H + spaceH, binW[idx], barH);
		showImg3b(reg) = color; // Draw bar
		rectangle(showImg3b, reg, cv::Scalar(0));

		reg.height = abs(h);
		reg.y = h >= 0 ? H - h : H + 2 * spaceH + barH;
		showImg3b(reg) = color;
		rectangle(showImg3b, reg, cv::Scalar(0));

		x += binW[idx];
	}
	imshow(title, showImg3b);
	return showImg3b;
}

inline void CmShow::showTinyMat(const std::string &title, const cv::Mat &m)
{
	int scale = 50, sz = m.rows * m.cols;
	while (sz > 200){
		scale /= 2;
		sz /= 4;
	}

	cv::Mat img;
	resize(m, img, cv::Size(), scale, scale, CV_INTER_NN);
	if (img.channels() == 3)
		cvtColor(img, img, CV_RGB2BGR);
	SaveShow(img, title);
}

inline void CmShow::SaveShow(const cv::Mat& img, const std::string& title)
{
	if (title.size() == 0)
		return;

	int mDepth = CV_MAT_DEPTH(img.type());
	double scale = (mDepth == CV_32F || mDepth == CV_64F ? 255 : 1);
	if (title.size() > 4 && title[title.size() - 4] == '.')
		imwrite(title, img*scale);
	else if (title.size())
		imshow(title, img);
}