#pragma once

#include "opencv2/opencv.hpp"

cv::Scalar randomColor(cv::RNG & rng = cv::RNG(0xFFFFFFFF));
void getColors(std::vector<cv::Scalar> & colors);

// Draw points
template <typename T>
void drawPoint(const cv::Point_<T> & pnt, cv::Mat & image, cv::Scalar color = cv::Scalar::all(-1), int radius = 3, int thick = 1, int iType = -1);

template <typename T>
void drawPoints(const std::vector<T> & pts, cv::Mat & image, cv::Scalar color = cv::Scalar::all(-1), int radius = 3, int thick = 1, std::vector<int> & iTypes = std::vector<int>());

template <typename T>
void drawPoints(const std::vector<T> & pts, const cv::Point2f & shift, cv::Mat & image, cv::Scalar color = cv::Scalar::all(-1), int radius = 3, int thick = 1, std::vector<int> & iTypes = std::vector<int>());

template <typename T>
void drawNPoints(const cv::Point_<T> * pts, const cv::Point2f & shift, cv::Mat & image, int radius = 3, int thick = 1, int N = 4);

template <typename T>
void drawNPoints(std::vector<cv::Point_<T>> & pts, const cv::Point2f & shift, cv::Mat & image, int radius = 3, int thick = 1);

template <typename T>
void drawPointsPos(const std::vector<T> & pts, const cv::Point2f & shift, cv::Mat & image, cv::Scalar color = cv::Scalar::all(-1));

template <typename T>
void drawPointPairs(const std::vector<cv::Point_<T>> & pts0, cv::Mat & image0, const std::vector<cv::Point_<T>> & pts1, cv::Mat & image1, int radius = 3, int thick = 1);

template <typename T0, typename T1>
void drawPointIndexes(const std::vector<T0> & idx, const std::vector<cv::Point_<T1>> & pts, cv::Mat & image, cv::Scalar colorTxt = cv::Scalar::all(255), cv::Scalar colorBck = cv::Scalar::all(0), int thickTxt = 1, int thickBck = 3);
template <typename T0, typename T1>
void drawPointIndexes(const std::vector<T0> & idx, const std::vector<cv::Point_<T1>> & pts, const cv::Point2f & ptShift, cv::Mat & image, cv::Scalar colorTxt = cv::Scalar::all(255), cv::Scalar colorBck = cv::Scalar::all(0), int thickTxt = 1, int thickBck = 3);
template <typename T>
void drawPointIndexes(int N, const std::vector<cv::Point_<T>> & pts, cv::Mat & image, cv::Scalar colorTxt = cv::Scalar::all(255), cv::Scalar colorBck = cv::Scalar::all(0), int thickTxt = 1, int thickBck = 3);
template <typename T>
void drawPointIndexes(int N, const std::vector<cv::Point_<T>> & pts, const cv::Point2f & ptShift, cv::Mat & image, cv::Scalar colorTxt = cv::Scalar::all(255), cv::Scalar colorBck = cv::Scalar::all(0), int thickTxt = 1, int thickBck = 3);

// draw lines
void drawLine(const cv::Vec2f & line, cv::Mat & image, cv::Scalar color = CV_RGB(255, 0, 0), int width = 1);
void drawLine(const cv::Vec4i & line, cv::Mat & image, cv::Scalar color = CV_RGB(255, 0, 0), int width = 1);
void drawLine(const cv::Point2f & p1, const cv::Point2f & p2, cv::Mat & image, cv::Scalar color = CV_RGB(255, 0, 0), int width = 1);
void drawLine(const cv::Vec4i & line, const cv::Point2f & shift, cv::Mat & image, cv::Scalar color, int width);
void drawLine(const std::vector<cv::Point> & line, cv::Mat & image, cv::Scalar color = CV_RGB(255, 0, 0), int width = 1);
void drawLines(const std::vector<cv::Vec2f> & lines, cv::Mat & image, cv::Scalar color = CV_RGB(255, 0, 0), int width = 1, bool drawLabel = false);
void drawLines(const std::vector<cv::Vec4i> & lines, cv::Mat & image, cv::Scalar color = cv::Scalar::all(-1), int width = 1, bool drawLabel = false, const std::vector<int> linesId = std::vector<int>());

//draw arrows
void drawArrow(const cv::Vec4i & line, cv::Mat & image, cv::Scalar color = CV_RGB(255, 0, 0), int width = 1);
void drawArrow(const std::vector<cv::Point2f> & prevPts, const std::vector<cv::Point2f> & nextPts, const std::vector<uchar> status, cv::Mat & image, cv::Scalar color = CV_RGB(255, 0, 0), int width = 1);

void drawIndexedLines(const std::vector<cv::Vec4i> & lines, const std::vector<int> lineIdx, const cv::Point2f shift, cv::Mat & image, cv::Scalar color = cv::Scalar::all(-1), int width = 1, bool drawLabel = false);

void draw4Lines(const std::vector<cv::Vec4i> & vLines, const std::vector<cv::Vec4i> & hLines, const int index[], cv::Mat & image, int width = 1);

void drawLineGroups(const std::vector<std::vector<cv::Vec4i>> & lines, cv::Mat & image, int width = 1);

void drawIntersections(const std::vector<cv::Point2f> & pts, cv::Mat & image, bool label = true, cv::Scalar color = CV_RGB(0, 255, 0), int diameter = 3);
void drawIntersections(const cv::Point2f * pts, int sz, cv::Mat & image, bool label = true, cv::Scalar color = CV_RGB(0, 255, 0), int diameter = 3);

// draw arcs
template <typename T>
void drawArc(const std::vector<cv::Point_<T>> & contour, cv::Mat & image, cv::Scalar color = cvScalar(0, 0, 255), int thick = 1, bool bDrawDot = true);
void drawArc(const cv::Vec4i & contour, cv::Mat & image, cv::Scalar color = cv::Scalar::all(-1), int thick = 1, bool bDrawDot = true);

template <typename T>
void drawArcs(const std::vector<std::vector<cv::Point_<T>> > & contours, cv::Mat & image, cv::Scalar color = cv::Scalar::all(-1), int thick = 1, bool bDrawInfo = false, bool bDrawDot = true);

// draw ellipses
void drawEllipse(const cv::RotatedRect & ellispe, cv::Mat & image, cv::Scalar color = cv::Scalar::all(-1), int thick = 1);
void drawEllipse(const cv::RotatedRect & ellispe, const cv::Point2f & shift, cv::Mat & image, cv::Scalar color = cv::Scalar::all(-1), int thick = 1);
void drawEllipses(const std::vector<cv::RotatedRect> & ellispes, cv::Mat & image, cv::Scalar color = cv::Scalar::all(-1), int thick = 1);
void drawEllipseInfo(const cv::RotatedRect & ellipse, cv::Mat & image, cv::Scalar color1 = cv::Scalar::all(-1), cv::Scalar color2 = cv::Scalar::all(-1), int thick = 1);
void drawEllipsesInfo(const std::vector<cv::RotatedRect> & ellipses, cv::Mat & image, cv::Scalar color1 = cv::Scalar::all(-1), cv::Scalar color2 = cv::Scalar::all(-1), int thick = 1);

// Draw a box
template <typename T>
void drawRect(const cv::Rect_<T> & rect, cv::Mat & image, cv::Scalar color = cv::Scalar::all(-1), int thick = 1);
template <typename T>
void drawRect(const cv::Vec<T, 4> & rect, cv::Mat & image, cv::Scalar color = cv::Scalar::all(-1), int thick = 1);

// Draw a rect surround the pixels on the image
void drawSurroundingRect(const cv::Mat pxlImg, cv::Mat canvas, cv::Scalar color = cv::Scalar(-1), int thick = 1);

// draw text
template <typename T0, typename T1>
void drawText(T0 txt, cv::Mat & canvas, const cv::Point_<T1> & p, cv::Scalar colorTxt = cv::Scalar::all(255), cv::Scalar colorBck = cv::Scalar::all(0), int thickTxt = 1, int thickBck = 3)
{
	std::ostringstream ss;
	ss << txt;
	putText(canvas, ss.str(), p, 1, 0.8, colorBck, thickBck);
	putText(canvas, ss.str(), p, 1, 0.8, colorTxt, thickTxt);
}

// draw time
void drawTime(std::string name, float t, std::string unit, cv::Mat & canvas, const cv::Point2f & p, cv::Scalar colorTxt = cv::Scalar::all(255), cv::Scalar colorBck = cv::Scalar::all(0), int thickTxt = 1, int thickBck = 3);

// Display camera info
void drawCameraInfo(cv::Mat & cvsModel, double alpha, double dCamCnt[], double theta, double beta, double gamma);
void drawCameraInfo(cv::Mat & cvsModel, double alpha, cv::Point3f dCamCnt3f, double theta, double beta, double gamma);

// Display homography matrix
void drawHomography(cv::Mat & cvsModel, const cv::Mat & H, std::string txt = "Homography:", const cv::Point2f & ptStart = cv::Point2f(10, 520));

cv::Scalar randomColor(cv::RNG & rng)
{
	return CV_RGB(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
}

void getColors(std::vector<cv::Scalar> & colors)
{
	colors.clear();
	colors.push_back(CV_RGB(0, 0, 255));
	colors.push_back(CV_RGB(0, 255, 255));
	colors.push_back(CV_RGB(255, 0, 0));
	colors.push_back(CV_RGB(255, 0, 255));
	colors.push_back(CV_RGB(0, 255, 0));
}

//////////////////////////////////////////////////////////////////////////
// Draw Points
//////////////////////////////////////////////////////////////////////////

template <typename T>
void drawPoint(const cv::Point_<T> & pnt, cv::Mat & image, cv::Scalar color, int radius, int thick, int iType)
{
	// for certain type of points
	if (iType == 0)
	{
		color = CV_RGB(0, 0, 255);
		radius = 2;
		thick = 2;
	}
	else if (iType == 1) {
		color = CV_RGB(100, 255, 255);
		radius = 4;
		thick = 2;
	}
	else if (iType == 2) {
		color = CV_RGB(255, 0, 0);
		radius = 6;
		thick = 2;
	}
	else if (iType == 3) {
		color = CV_RGB(255, 0, 255);
		radius = 8;
		thick = 2;
	}

	// for random color
	if (color(0) == -1)
		color = randomColor();

	circle(image, pnt, radius, color, thick);
}
template void drawPoint(const cv::Point_<int> & pnt, cv::Mat & image, cv::Scalar color, int radius, int thick, int iType);
template void drawPoint(const cv::Point_<float> & pnt, cv::Mat & image, cv::Scalar color, int radius, int thick, int iType);

template <typename T>
void drawPoints(const std::vector<T> & pts, cv::Mat & image, cv::Scalar color, int radius, int thick, std::vector<int> & iTypes)
{
	cv::Scalar c = color;
	cv::RNG rng = cv::RNG(0x003FFFFF);
	for (size_t i = 0; i < pts.size(); i++)
	{
		if (color[0] == -1)
			c = randomColor(rng);

		if (iTypes.empty())
			drawPoint(cv::Point2f(pts[i]), image, c, radius, thick);
		else
			drawPoint(cv::Point2f(pts[i]), image, c, radius, thick, iTypes[i]);
	}
}
template void drawPoints(const std::vector<cv::Point> & pts, cv::Mat & image, cv::Scalar color, int radius, int thick, std::vector<int> & iTypes);
template void drawPoints(const std::vector<cv::Point2f> & pts, cv::Mat & image, cv::Scalar color, int radius, int thick, std::vector<int> & iTypes);

template <typename T>
void drawPoints(const std::vector<T> & pts, const cv::Point2f & shift, cv::Mat & image, cv::Scalar color, int radius, int thick, std::vector<int> & iTypes)
{
	cv::Scalar c = color;
	cv::RNG rng = cv::RNG(0x003FFFFF);
	for (size_t i = 0; i < pts.size(); i++)
	{
		if (color[0] == -1)
			c = randomColor(rng);

		if (iTypes.empty())
			drawPoint(cv::Point2f(pts[i]) + shift, image, c, radius, thick);
		else
			drawPoint(cv::Point2f(pts[i]) + shift, image, c, radius, thick, iTypes[i]);
	}
}
template void drawPoints(const std::vector<cv::Point> & pts, const cv::Point2f & ptshift, cv::Mat & image, cv::Scalar color, int radius, int thick, std::vector<int> & iTypes);
template void drawPoints(const std::vector<cv::Point2f> & pts, const cv::Point2f & ptshift, cv::Mat & image, cv::Scalar color, int radius, int thick, std::vector<int> & iTypes);

template <typename T>
void drawNPoints(const cv::Point_<T> * pts, const cv::Point2f & shift, cv::Mat & image, int radius, int thick, int N)
{
	if (N >= 1)
		drawPoint(cv::Point2f(pts[0]) + shift, image, CV_RGB(0, 0, 255), radius, thick);
	if (N >= 2)
		drawPoint(cv::Point2f(pts[1]) + shift, image, CV_RGB(0, 255, 255), radius, thick);
	if (N >= 3)
		drawPoint(cv::Point2f(pts[2]) + shift, image, CV_RGB(255, 0, 0), radius, thick);
	if (N >= 4)
		drawPoint(cv::Point2f(pts[3]) + shift, image, CV_RGB(255, 0, 255), radius, thick);
	if (N >= 5)
		drawPoint(cv::Point2f(pts[4]) + shift, image, CV_RGB(0, 255, 0), radius, thick);
	if (N >= 6)
		drawPoint(cv::Point2f(pts[5]) + shift, image, CV_RGB(255, 255, 0), radius, thick);
	if (N > 6)
	{
		cv::RNG rng = cv::RNG(0x002FFFFF);
		for (size_t k = 6; k < N; k++)
			drawPoint(cv::Point2f(pts[k]) + shift, image, randomColor(rng), radius, thick);
	}
}
template void drawNPoints(const cv::Point * pts, const cv::Point2f & shift, cv::Mat & image, int radius, int thick, int N);
template void drawNPoints(const cv::Point2f * pts, const cv::Point2f & shift, cv::Mat & image, int radius, int thick, int N);

template <typename T>
void drawNPoints(std::vector<cv::Point_<T>> & pts, const cv::Point2f & shift, cv::Mat & image, int radius, int thick)
{
	drawPoint(cv::Point2f(*(pts.end() - 4)) + shift, image, CV_RGB(0, 0, 255), radius, thick);
	drawPoint(cv::Point2f(*(pts.end() - 3)) + shift, image, CV_RGB(0, 255, 255), radius, thick);
	drawPoint(cv::Point2f(*(pts.end() - 2)) + shift, image, CV_RGB(255, 0, 0), radius, thick);
	drawPoint(cv::Point2f(*(pts.end() - 1)) + shift, image, CV_RGB(255, 0, 255), radius, thick);
}
template void drawNPoints(std::vector<cv::Point> & pts, const cv::Point2f & shift, cv::Mat & image, int radius, int thick);
template void drawNPoints(std::vector<cv::Point2f> & pts, const cv::Point2f & shift, cv::Mat & image, int radius, int thick);

template <typename T>
void drawPointsPos(const std::vector<T> & pts, const cv::Point2f & shift, cv::Mat & image, cv::Scalar color)
{
	for (size_t i = 0; i < pts.size(); i++)
	{
		drawText(i, image, cv::Point2f(pts[i]) + shift, color);
		//drawText((int)pts[i].x, image, pts[i] + cv::Point2f(-20, 20) + shift, color);
		//drawText((int)pts[i].y, image, pts[i] + cv::Point2f(+10, 20) + shift, color);
	}
}
template void drawPointsPos(const std::vector<cv::Point> & pts, const cv::Point2f & shift, cv::Mat & image, cv::Scalar color);
template void drawPointsPos(const std::vector<cv::Point2f> & pts, const cv::Point2f & shift, cv::Mat & image, cv::Scalar color);

// draw two pairs of points on 2 images with matched color
template <typename T>
void drawPointPairs(const std::vector<cv::Point_<T>> & pts0, cv::Mat & image0, const std::vector<cv::Point_<T>> & pts1, cv::Mat & image1, int radius, int thick)
{
	for (size_t i = 0; i < pts0.size(); i++)
	{
		cv::Scalar color = randomColor();
		drawPoint(pts0[i], image0, color, radius, thick);
		drawPoint(pts1[i], image1, color, radius, thick);
	}
}
template void drawPointPairs(const std::vector<cv::Point> & pts0, cv::Mat & image0, const std::vector<cv::Point> & pts1, cv::Mat & image1, int radius, int thick);
template void drawPointPairs(const std::vector<cv::Point2f> & pts0, cv::Mat & image0, const std::vector<cv::Point2f> & pts1, cv::Mat & image1, int radius, int thick);

// draw indexes on image with given corresponding positions
template <typename T0, typename T1>
void drawPointIndexes(const std::vector<T0> & idx, const std::vector<cv::Point_<T1>> & pts, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck)
{
	for (size_t i = 0; i < idx.size(); i++)
	{
		drawText(idx[i], image, pts[i], colorTxt, colorBck, thickTxt, thickBck);
	}
}
template void drawPointIndexes(const std::vector<int> & idx, const std::vector<cv::Point> & pts, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(const std::vector<int> & idx, const std::vector<cv::Point2f> & pts, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(const std::vector<float> & idx, const std::vector<cv::Point> & pts, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(const std::vector<float> & idx, const std::vector<cv::Point2f> & pts, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck);

template <typename T0, typename T1>
void drawPointIndexes(const std::vector<T0> & idx, const std::vector<cv::Point_<T1>> & pts, const cv::Point2f & ptShift, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck)
{
	for (size_t i = 0; i < idx.size(); i++)
	{
		drawText(idx[i], image, cv::Point2f(pts[i]) + ptShift, colorTxt, colorBck, thickTxt, thickBck);
	}
}
template void drawPointIndexes(const std::vector<int> & idx, const std::vector<cv::Point> & pts, const cv::Point2f & ptShift, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(const std::vector<int> & idx, const std::vector<cv::Point2f> & pts, const cv::Point2f & ptShift, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(const std::vector<float> & idx, const std::vector<cv::Point> & pts, const cv::Point2f & ptShift, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(const std::vector<float> & idx, const std::vector<cv::Point2f> & pts, const cv::Point2f & ptShift, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck);

template <typename T>
void drawPointIndexes(int N, const std::vector<cv::Point_<T>> & pts, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck)
{
	for (size_t i = 0; i < N; i++)
	{
		drawText(i, image, pts[i], colorTxt, colorBck, thickTxt, thickBck);
	}
}
template void drawPointIndexes(int N, const std::vector<cv::Point> & pts, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(int N, const std::vector<cv::Point2f> & pts, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck);

template <typename T>
void drawPointIndexes(int N, const std::vector<cv::Point_<T>> & pts, const cv::Point2f & ptShift, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck)
{
	for (size_t i = 0; i < N; i++)
	{
		drawText(i, image, cv::Point2f(pts[i]) + ptShift, colorTxt, colorBck, thickTxt, thickBck);
	}
}
template void drawPointIndexes(int N, const std::vector<cv::Point> & pts, const cv::Point2f & ptShift, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(int N, const std::vector<cv::Point2f> & pts, const cv::Point2f & ptShift, cv::Mat & image, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck);

//////////////////////////////////////////////////////////////////////////
// Draw Lines
//////////////////////////////////////////////////////////////////////////

void drawLine(const cv::Vec2f & line, cv::Mat & image, cv::Scalar color, int width)
{
	float rho = line[0], theta = line[1];
	cv::Point pt1, pt2;
	double a = cos(theta), b = sin(theta);
	double x0 = a*rho, y0 = b*rho;
	pt1.x = cvRound(x0 + 1000 * (-b));
	pt1.y = cvRound(y0 + 1000 * (a));
	pt2.x = cvRound(x0 - 1000 * (-b));
	pt2.y = cvRound(y0 - 1000 * (a));
	cv::line(image, pt1, pt2, color, width, CV_AA);
}

void drawLine(const cv::Vec4i & line, cv::Mat & image, cv::Scalar color, int width)
{
	cv::line(image, cv::Point(line[0], line[1]), cv::Point(line[2], line[3]), color, width, CV_AA);
}

void drawLine(const cv::Point2f & p1, const cv::Point2f & p2, cv::Mat & image, cv::Scalar color, int width)
{
	cv::line(image, p1, p2, color, width, CV_AA);
}

void drawArrow(const cv::Vec4i & line, cv::Mat & image, cv::Scalar color, int width)
{
	//cv::line(image, cv::Point(line[0], line[1]), cv::Point(line[2], line[3]), color, width, CV_AA);//line src.x src.y dst.x dst.y
	cv::Point2f p, q;
	p.x = (int)line[0];
	p.y = (int)line[1];
	q.x = (int)line[2];
	q.y = (int)line[3];

	double angle = atan2((double)p.y - q.y, (double)p.x - q.x);
	double hypotenuse = sqrt((p.y - q.y)*(p.y - q.y) + (p.x - q.x)*(p.x - q.x));
	q.x = (int)(p.x - 3 * hypotenuse * cos(angle));
	q.y = (int)(p.y - 3 * hypotenuse * sin(angle));
	cv::line(image, p, q, color, width, CV_AA, 0);

	p.x = (int)(q.x + 9 * cos(angle + CV_PI / 4));
	p.y = (int)(q.y + 9 * sin(angle + CV_PI / 4));
	cv::line(image, p, q, color, width, CV_AA, 0);
	p.x = (int)(q.x + 9 * cos(angle - CV_PI / 4));
	p.y = (int)(q.y + 9 * sin(angle - CV_PI / 4));
	cv::line(image, p, q, color, width, CV_AA, 0);
}

void drawArrow(const std::vector<cv::Point2f> & prevPts, const std::vector<cv::Point2f> & nextPts, const std::vector<uchar> status, cv::Mat & image, cv::Scalar color, int width)
{
	assert(prevPts.size() == nextPts.size() && prevPts.size() == status.size());
	for (int i = 0; i < prevPts.size(); i++) {
		if (status[i] == 1) {
			line(image, prevPts[i], nextPts[i], color, width);
			circle(image, nextPts[i], 1, color, -1);
		}
	}
}

void drawLine(const cv::Vec4i & line, const cv::Point2f & shift, cv::Mat & image, cv::Scalar color, int width)
{
	cv::Vec4i lineShift(line);
	lineShift[0] += shift.x;
	lineShift[1] += shift.y;
	lineShift[2] += shift.x;
	lineShift[3] += shift.y;
	drawLine(lineShift, image, color, width);
}

void drawLine(const std::vector<cv::Point> & line, cv::Mat & image, cv::Scalar color, int width)
{
	cv::line(image, line.front(), line.back(), color, width, CV_AA);
}

void drawLines(const std::vector<cv::Vec2f> & lines, cv::Mat & image, cv::Scalar color, int width, bool drawLabel)
{
	if (lines.empty())
		return;

	for (size_t i = 0; i < lines.size(); i++)
	{
		drawLine(lines[i], image, color, width);

		if (drawLabel)
		{
			char num[100] = "";
			sprintf(num, "Line %d", i);
			float rho = lines[i][0], theta = lines[i][1];
			double a = cos(theta), b = sin(theta);
			int x = image.cols / 3, y;
			if (b>FLT_EPSILON)
				y = (rho - x * a) / b;
			x = std::min(std::max(x, image.cols - 50), 10);
			y = std::min(std::max(y, image.rows - 20), 20);
			drawText(num, image, cv::Point(x, y), color);
		}
	}
}

void drawLines(const std::vector<cv::Vec4i> & lines, cv::Mat & image, cv::Scalar color, int width, bool drawLabel, const std::vector<int> linesId)
{
	if (lines.empty())
		return;

	cv::Scalar c = color;
	cv::RNG rng = cv::RNG(0x00FFFAFF);
	for (size_t i = 0; i < lines.size(); i++)
	{
		if (color[0] == -1)
			c = randomColor(rng);
		drawLine(lines[i], image, c, width);

		if (drawLabel)
		{
			char num[100];
			if (linesId.empty())
			{
				sprintf_s(num, "Line %d", i);
			}
			else {
				if (linesId[i] == 255)
					sprintf_s(num, "Other Lines");
				else if (linesId[i] >= 0)
					sprintf_s(num, "H Line %d", linesId[i]);
				else
					sprintf_s(num, "V Line %d", -linesId[i] - 1);
			}
			int x = (lines[i][0] + lines[i][2]) / 2, y = (lines[i][1] + lines[i][3]) / 2;
			x = std::max(std::min(x, image.cols - 80), 10);
			y = std::max(std::min(y, image.rows - 20), 20);
			drawText(num, image, cv::Point(x, y), c);
		}
	}
}

void drawIndexedLines(const std::vector<cv::Vec4i> & lines, const std::vector<int> lineIdx, const cv::Point2f shift, cv::Mat & image, cv::Scalar color, int width, bool drawLabel)
{
	std::vector<cv::Vec4i> mLines;
	for (size_t i = 0; i < lineIdx.size(); i++)
	{
		cv::Vec4i mLine(lines[lineIdx[i]]);
		mLine[0] += shift.x; mLine[2] += shift.x;
		mLine[1] += shift.y; mLine[3] += shift.y;
		mLines.push_back(mLine);
	}

	drawLines(mLines, image, color, width, drawLabel);
}

void draw4Lines(const std::vector<cv::Vec4i> & vLines, const std::vector<cv::Vec4i> & hLines, const int index[], cv::Mat & image, int width)
{
	if (vLines.empty() || hLines.empty())
		return;

	drawLine(vLines[index[0]], image, CV_RGB(0, 0, 255), width);
	drawLine(vLines[index[1]], image, CV_RGB(0, 255, 255), width);
	drawLine(hLines[index[2]], image, CV_RGB(255, 0, 0), width);
	drawLine(hLines[index[3]], image, CV_RGB(255, 0, 255), width);
}

void drawLineGroups(const std::vector<std::vector<cv::Vec4i>> & groupLines, cv::Mat & image, int width)
{
	std::vector<cv::Scalar> colors;
	getColors(colors);
	for (size_t i = 0; i < groupLines.size(); i++)
		drawLines(groupLines[i], image, colors[i % colors.size()], width);
}

void drawIntersections(const cv::Point2f * pts, int sz, cv::Mat & image, bool label, cv::Scalar color, int diameter)
{
	assert(!image.empty());
	for (int i = 0; i < sz; i++)
	{
		cv::Scalar Color = randomColor();
		circle(image, pts[i], diameter, color, -1);
		char num[100] = "";
		sprintf(num, "P%d", i);
		putText(image, num, pts[i], 1, 1, Color);
	}
}

void drawIntersections(const std::vector<cv::Point2f> & pts, cv::Mat & image, bool label, cv::Scalar color, int diameter)
{
	if (!pts.empty())
		drawIntersections(&pts[0], (int)pts.size(), image, label, color, diameter);
}

//////////////////////////////////////////////////////////////////////////
// Draw Arcs
//////////////////////////////////////////////////////////////////////////

template <typename T>
void drawArc(const std::vector<cv::Point_<T>> & contour, cv::Mat & image, cv::Scalar color, int thick, bool bDrawDot)
{
	size_t length = contour.size();

	if (color[0] == -1)
		color = randomColor();

	// draw the starting point
	if (length > 0 && bDrawDot)
		circle(image, contour[0], 3, color);

	if (length > 1)
	{
		// draw the ending point and intermediate points
		if (bDrawDot)
		{
			for (int i = 0; i < length - 1; i++)
				circle(image, contour[i + 1], 1, CV_RGB(255, 255, 255), -1);
			circle(image, contour[length - 1], 3, color);
		}

		for (int i = 0; i < length - 1; i++)
			line(image, contour[i], contour[i + 1], color, thick);
	}
}
template void drawArc(const std::vector<cv::Point> & contour, cv::Mat & image, cv::Scalar color, int thick, bool bDrawDot);
template void drawArc(const std::vector<cv::Point2f> & contour, cv::Mat & image, cv::Scalar color, int thick, bool bDrawDot);

void drawArc(const cv::Vec4i & contour, cv::Mat & image, cv::Scalar color, int thick, bool bDrawDot)
{
	std::vector<cv::Point> line;
	line.push_back(cv::Point(contour[0], contour[1]));
	line.push_back(cv::Point(contour[2], contour[3]));
	drawArc(line, image, color, thick, bDrawDot);
}

template <typename T>
void drawArcs(const std::vector<std::vector<cv::Point_<T>> > & contours, cv::Mat & image, cv::Scalar color, int thick, bool bDrawInfo, bool bDrawDot)
{
	cv::Scalar c = color;
	cv::RNG rng = cv::RNG(0x001FFFFF);
	bool change = true;
	for (size_t i = 0; i < contours.size(); i++)
	{
		if (color[0] == -1)
			c = randomColor(rng);
		drawArc(contours[i], image, c, thick, bDrawDot);
		if (bDrawInfo)
			drawText(i, image, (contours[i].front() + contours[i].back()) * 0.5);
	}
}
template void drawArcs(const std::vector<std::vector<cv::Point>> & contours, cv::Mat & image, cv::Scalar color, int thick, bool bDrawInfo, bool bDrawDot);
template void drawArcs(const std::vector<std::vector<cv::Point2f>> & contours, cv::Mat & image, cv::Scalar color, int thick, bool bDrawInfo, bool bDrawDot);

//////////////////////////////////////////////////////////////////////////
// Draw Ellipses
//////////////////////////////////////////////////////////////////////////

void drawEllipse(const cv::RotatedRect & ellispe, cv::Mat & cdst, cv::Scalar color, int thick)
{
	float h = ellispe.size.height;
	float w = ellispe.size.width;
	if (h >0 && w > 0)
	{
		float ratio = MAX(h, w) / MIN(h, w);
		//if (ratio<5)
		{
			if (color[0] == -1)
				ellipse(cdst, ellispe, randomColor(), thick);
			else
				ellipse(cdst, ellispe, color, thick);
		}
	}
}

void drawEllipse(const cv::RotatedRect & ellipse, const cv::Point2f & shift, cv::Mat & image, cv::Scalar color, int thick)
{
	cv::RotatedRect ell_shift(ellipse);
	ell_shift.center.x += shift.x;
	ell_shift.center.y += shift.y;
	drawEllipse(ell_shift, image, color, thick);
}

void drawEllipses(const std::vector<cv::RotatedRect> & ellipses, cv::Mat & image, cv::Scalar color, int thick)
{
	cv::Scalar c = color;
	cv::RNG rng = cv::RNG(0x000FFFFF);
	for (size_t i = 0; i < ellipses.size(); i++)
	{
		if (color[0] == -1)
			c = randomColor(rng);
		drawEllipse(ellipses[i], image, c, thick);
	}
}

void drawEllipseInfo(const cv::RotatedRect & ellipse, cv::Mat & image, cv::Scalar color1, cv::Scalar color2, int thick)
{
	if (color1[0] == -1)
		color1 = CV_RGB(255, 0, 0);
	if (color2[0] == -1)
		color2 = CV_RGB(0, 255, 0);

	cv::Point2f p0(ellipse.center);
	drawPoint(p0, image, color1, 5, -1);
	float alpha = ellipse.angle / 180 * CV_PI;
	cv::Point2f v0(cos(alpha), sin(alpha));
	cv::Point2f v1(cos(alpha + CV_PI / 2), sin(alpha + CV_PI / 2));
	drawPoint(p0 + v0 * ellipse.size.width * 0.5, image, color1, 5, -1);
	drawPoint(p0 - v0 * ellipse.size.width * 0.5, image, color1, 5, -1);
	drawPoint(p0 + v1 * ellipse.size.height * 0.5, image, color2, 5, -1);
	drawPoint(p0 - v1 * ellipse.size.height * 0.5, image, color2, 5, -1);
}

void drawEllipsesInfo(const std::vector<cv::RotatedRect> & ellipses, cv::Mat & image, cv::Scalar color1, cv::Scalar color2, int thick)
{
	for (size_t i = 0; i < ellipses.size(); i++)
		drawEllipseInfo(ellipses[i], image, color1, color2, thick);
}

//////////////////////////////////////////////////////////////////////////
// Draw a Box
//////////////////////////////////////////////////////////////////////////

template <typename T>
void drawRect(const cv::Rect_<T> & rect, cv::Mat & image, cv::Scalar color, int thick)
{
	if (color[0] == -1)
		color = randomColor();
	line(image, cv::Point_<T>(rect.x, rect.y), cv::Point_<T>(rect.x + rect.width, rect.y), color, thick);
	line(image, cv::Point_<T>(rect.x, rect.y), cv::Point_<T>(rect.x, rect.y + rect.height), color, thick);
	line(image, cv::Point_<T>(rect.x + rect.width, rect.y + rect.height), cv::Point_<T>(rect.x + rect.width, rect.y), color, thick);
	line(image, cv::Point_<T>(rect.x + rect.width, rect.y + rect.height), cv::Point_<T>(rect.x, rect.y + rect.height), color, thick);
}
template void drawRect(const cv::Rect & rect, cv::Mat & image, cv::Scalar color, int thick);
template void drawRect(const cv::Rect_<float> & rect, cv::Mat & image, cv::Scalar color, int thick);

template <typename T>
void drawRect(const cv::Vec<T, 4> & rect, cv::Mat & image, cv::Scalar color, int thick)
{
	cv::Rect rect_(rect[0], rect[1], rect[2] - rect[0], rect[3] - rect[1]);
	drawRect(rect_, image, color, thick);
}
template void drawRect(const cv::Vec4i & rect, cv::Mat & image, cv::Scalar color, int thick);
template void drawRect(const cv::Vec4f & rect, cv::Mat & image, cv::Scalar color, int thick);

// Draw a rect surround the pixels on the image
void drawSurroundingRect(const cv::Mat pxlImg, cv::Mat canvas, cv::Scalar color, int thick)
{
	int x0 = pxlImg.cols - 1, y0 = pxlImg.rows - 1, x1 = 0, y1 = 0;
	for (int x = 0; x < pxlImg.cols; x++) {
		for (int y = 0; y < pxlImg.rows; y++) {
			if (pxlImg.ptr<uchar>(y)[x] != 0) {
				if (x < x0)
					x0 = x;
				if (x > x1)
					x1 = x;
				if (y < y0)
					y0 = y;
				if (y > y1)
					y1 = y;
			}
		}
	}

	if (color[0] == -1)
		color = randomColor();

	if (x0 < x1 && y0 < y1)
		drawRect(cv::Vec4i(x0, y0, x1, y1), canvas, color, thick);
}

//////////////////////////////////////////////////////////////////////////
// draw time

void drawTime(std::string name, float t, std::string unit, cv::Mat & canvas, const cv::Point2f & p, cv::Scalar colorTxt, cv::Scalar colorBck, int thickTxt, int thickBck)
{
	t = floor(t * 1000) / 1000;

	std::ostringstream ss;
	ss << name << ": " << t << " " << unit;
	putText(canvas, ss.str(), p, 1, 0.8, colorBck, thickBck);
	putText(canvas, ss.str(), p, 1, 0.8, colorTxt, thickTxt);
}

//////////////////////////////////////////////////////////////////////////
// Draw Camera Informations
//////////////////////////////////////////////////////////////////////////

void drawCameraInfo(cv::Mat & cvsModel, double alpha, double dCamCnt[], double theta, double beta, double gamma)
{
	drawText("Alpha_U, Alpha_V = ", cvsModel, cv::Point(10, 40));
	drawText(alpha, cvsModel, cv::Point(160, 40));
	drawText("Camera Center X, Y, Z = ", cvsModel, cv::Point(10, 60));
	drawText(dCamCnt[0], cvsModel, cv::Point(40, 80));
	drawText(dCamCnt[1], cvsModel, cv::Point(110, 80));
	drawText(dCamCnt[2], cvsModel, cv::Point(170, 80));
	drawText("Theta, Beta, Gamma = ", cvsModel, cv::Point(10, 100));
	drawText(theta, cvsModel, cv::Point(40, 120));
	drawText(beta, cvsModel, cv::Point(130, 120));
	drawText(gamma, cvsModel, cv::Point(220, 120));
}

void drawCameraInfo(cv::Mat & cvsModel, double alpha, cv::Point3f dCamCnt3f, double theta, double beta, double gamma)
{
	double dCamCnt[3];
	dCamCnt[0] = dCamCnt3f.x;
	dCamCnt[1] = dCamCnt3f.y;
	dCamCnt[2] = dCamCnt3f.z;
	drawCameraInfo(cvsModel, alpha, dCamCnt, theta, beta, gamma);
}

//////////////////////////////////////////////////////////////////////////
// Display homography matrix

void drawHomography(cv::Mat & cvsModel, const cv::Mat & H, std::string txt, const cv::Point2f & ptStart)
{
	// convert the cv::Mat
	double h[9];
	cv::Mat _H(3, 3, CV_64F, h);
	H.convertTo(_H, _H.type());

	if (abs(h[8] - 1) < 1e-5)
		h[8] = 1;

	// leave 2 digits
	for (int i = 0; i < 9; i++)
		h[i] = floor(h[i] * 1000) / 1000;

	// display
	drawText(txt, cvsModel, ptStart - cv::Point2f(0, 20));
	double c = 50, r = 20;
	double shtX[] = { 0, c, c * 2, 0, c, c * 2, 0, c, c * 2 };
	double shtY[] = { 0, 0, 0, r, r, r, r * 2, r * 2, r * 2 };
	for (int i = 0; i < 9; i++)
		drawText(h[i], cvsModel, ptStart + cv::Point2f(shtX[i], shtY[i]));
}
