#include "SparseOpticalFlow.h"

#include "helper/helper.h"

using namespace std;
using namespace cv;

// Constructor
video_processor::SparseOpticalFlow::SparseOpticalFlow(const int area_type)
{
	// Extract area types
	if (area_type & OF_TOP_LEFT)
		this->_area_types.push_back(OF_TOP_LEFT);
	if (area_type & OF_MIDDLE)
		this->_area_types.push_back(OF_MIDDLE);
	if (area_type & OF_TOP)
		this->_area_types.push_back(OF_TOP);
	if (area_type & OF_MIDDLE_SG)
		this->_area_types.push_back(OF_MIDDLE_SG);
	// Initialize output arrays
	for (const AreaType area_type : this->_area_types) {
		this->_each_still_array.insert({ area_type, vector<int>() });
		this->_each_score_array.insert({ area_type, vector<float>() });
	}
}

// Push back image
void video_processor::SparseOpticalFlow::push_back_image(const cv::Mat & img)
{
	CV_Assert(img.type() == CV_8UC1 || img.type() == CV_8UC3);
	// Convert to gray and resize
	Mat img_fixed = img;
	resize(img_fixed, img_fixed, Size(FRAME_WIDTH, FRAME_HEIGHT));
	if (img_fixed.type() == CV_8UC3)
		cvtColor(img_fixed, img_fixed, CV_BGR2GRAY);
	// Push to images
	this->_images.push_back(img_fixed);
	for (auto & each : this->_each_still_array)
		each.second.push_back(-1);
	for (auto & each : this->_each_score_array)
		each.second.push_back(-1);
}

// Dispose image (by frame number)
void video_processor::SparseOpticalFlow::dispose_image(const int frm_num)
{
	this->_images[frm_num] = Mat();
}

// Run optical flow
void video_processor::SparseOpticalFlow::run(const int frm_num)
{
	this->run(frm_num, frm_num + 1);
}

// Run optical flow (not include frm_num_end)
void video_processor::SparseOpticalFlow::run(const int frm_num_begin, const int frm_num_end)
{
	cout<<frm_num_begin <<endl<< frm_num_end<<endl << this->_images.size()<<endl;
	cout <<( 0 <= frm_num_begin) << (frm_num_begin < frm_num_end )<< (frm_num_end <= this->_images.size())<<endl;
	CV_Assert(0 <= frm_num_begin && frm_num_begin < frm_num_end && frm_num_end <= this->_images.size());
	for (int frmID = frm_num_begin; frmID < this->DFRMS; frmID++) {
		for (const AreaType area_type : this->_area_types) {
			this->_each_still_array[area_type][frmID] = -1;
			this->_each_score_array[area_type][frmID] = -1;
		}
	}
	for (int frmID = MAX(this->DFRMS, frm_num_begin); frmID < frm_num_end; frmID++) {
		// Get current image and previous image
		const Mat img_cur = this->_images[frmID];
		const Mat img_prev = this->_images[frmID - DFRMS];
		CV_Assert(img_cur.type() == CV_8UC1);
		CV_Assert(img_prev.type() == CV_8UC1);
		CV_Assert(img_cur.size() == Size(FRAME_WIDTH, FRAME_HEIGHT));
		CV_Assert(img_prev.size() == Size(FRAME_WIDTH, FRAME_HEIGHT));
		// Build optical flow pyramid by previous image
		vector<Mat> pyramid;
		buildOpticalFlowPyramid(img_prev, pyramid, Size(PYRAMID_WIN_WIDTH, PYRAMID_WIN_HEIGHT), PYRAMID_MAX_LEVEL);
		for (const AreaType area_type : this->_area_types) {
			// Avoid redundancy
			CV_Assert(this->_each_still_array[area_type][frmID] == -1 && this->_each_score_array[area_type][frmID] == -1);
			// Set points (grid points) on previous frame
			const vector<Point2f> prevPts = SparseOpticalFlow::__set_prev_pts(Size(FRAME_WIDTH, FRAME_HEIGHT), area_type);
			// Get predicted points, status and error
			vector<Point2f> nextPts;
			vector<uchar> status;
			Mat err;
			calcOpticalFlowPyrLK(pyramid, img_cur, prevPts, nextPts, status, err);
			// Calculate norm histogram
			const float max_movement = (float)FRAME_WIDTH / NORM_HIST_NBINS;
			const vector<int> norm_hist = SparseOpticalFlow::__calc_norm_hist(prevPts, nextPts, status, max_movement, NORM_HIST_NBINS);
			// Check if it is a still scene
			const bool b_still_scene = SparseOpticalFlow::__is_still_scene(norm_hist, area_type);
			const float score = std::accumulate(norm_hist.begin(), norm_hist.begin() + 3, 0) / (float)std::accumulate(norm_hist.begin(), norm_hist.end(), 0);
			// Add to output arrays
			this->_each_still_array[area_type][frmID] = b_still_scene;
			this->_each_score_array[area_type][frmID] = score;
		}
	}
}

// set starting point for optical flow
vector<Point2f> video_processor::SparseOpticalFlow::__set_prev_pts(const Size frame_sz, const AreaType area_type)
{
	vector<Point2f> pts;
	int bnd_x0, bnd_x1, bnd_y0, bnd_y1, step_x, step_y;

	switch (area_type)
	{
	case OF_TOP_LEFT:
		bnd_x0 = frame_sz.width * 0.05;
		bnd_x1 = frame_sz.width * 0.6;
		bnd_y0 = frame_sz.height * 0.05;
		bnd_y1 = frame_sz.height * 0.3;
		step_x = frame_sz.width * 0.05;
		step_y = frame_sz.height * 0.05;
		break;
	case OF_MIDDLE:
	case OF_MIDDLE_SG:
		bnd_x0 = frame_sz.width * 0.1;
		bnd_x1 = frame_sz.width * 0.9;
		bnd_y0 = frame_sz.height * 0.5;
		bnd_y1 = frame_sz.height * 0.7;
		step_x = frame_sz.width * 0.05;
		step_y = frame_sz.height * 0.05;
		break;
	case OF_TOP:
		bnd_x0 = frame_sz.width * 0.03;
		bnd_x1 = frame_sz.width * 0.99;
		bnd_y0 = frame_sz.height * 0.01;
		bnd_y1 = frame_sz.height * 0.26;
		step_x = frame_sz.width * 0.08;
		step_y = frame_sz.height * 0.05;
		break;
	default:
		break;
	}

	for (int x = bnd_x0; x < bnd_x1; x += step_x) {
		for (int y = bnd_y0; y < bnd_y1; y += step_y) {
			pts.push_back(Point2f(x, y));
		}
	}

	return pts;
}

// optical flow to histogram
vector<int> video_processor::SparseOpticalFlow::__calc_norm_hist(const vector<Point2f> & prevPts, const vector<Point2f> & nextPts, const vector<uchar> & status, const float max_movement, const int nBins)
{
	const float step = max_movement / nBins;
	vector<int> norm_hist(nBins, 0);
	for (int i = 0; i < prevPts.size(); i++) {
		// Avoid bad status
		if (!status[i])
			continue;
		// Avoid large movement
		const float movement = norm(nextPts[i] - prevPts[i]);
		if (movement > max_movement)
			continue;
		// Add 1 to a corresponding bin
		const int binID = MIN(nBins - 1, movement / step);
		norm_hist[binID]++;
	}
	for (int i = 0; i < norm_hist.size(); i++)
		norm_hist[i] = (float)norm_hist[i] / prevPts.size() * 100;
	return norm_hist;
}

// Check if it is a still scene
bool video_processor::SparseOpticalFlow::__is_still_scene(const vector<int> & norm_hist, const AreaType area_type)
{
	if (norm_hist.size() < 3)
		return false;

	bool b_valid = false;

	switch (area_type)
	{
	case OF_TOP_LEFT:
		if (norm_hist[0] > 60 ||
			(norm_hist[0] + norm_hist[1] > 90 && abs(norm_hist[0] - norm_hist[1]) < 30) ||
			(norm_hist[0] + norm_hist[1] + norm_hist[2] > 90 && abs(norm_hist[0] - norm_hist[1]) < 20) && abs(norm_hist[0] - norm_hist[2]) < 20)
			b_valid = true;

		for (int i = 3; i < norm_hist.size(); i++) {
			if (norm_hist[i] > 10)
				return false;
		}

		break;

	case OF_MIDDLE:
		if (norm_hist[0] > 80)
			b_valid = true;

		for (int i = 1; i < norm_hist.size(); i++) {
			if (norm_hist[i] > 10)
				return false;
		}

		break;

	case OF_MIDDLE_SG:
		//if (norm_hist[0] + norm_hist[1] > 30)
		b_valid = true;
		//const int total_votes = std::accumulate(norm_hist.begin(), norm_hist.end(), 0);
		//cout << "A:" << std::accumulate(norm_hist.begin() + 6, norm_hist.end(), 0) << endl;
		if (std::accumulate(norm_hist.begin() + 4, norm_hist.end(), 0) / (float)std::accumulate(norm_hist.begin(), norm_hist.end(), 0) > 0.75)
			b_valid = false;
		// Not too high on each individual
		//for (int i = 0; i < norm_hist.size(); i++) {
		//	if ((float)norm_hist[i] / std::accumulate(norm_hist.begin(), norm_hist.end(), 0) > 0.75){
		//		//cout << "B:" << (float)norm_hist[i] / std::accumulate(norm_hist.begin() + 6, norm_hist.end(), 0) << endl;
		//		b_valid = true;
		//	}
		//}
		//if (std::accumulate(norm_hist.begin() + 10, norm_hist.end(), 0) / (float)std::accumulate(norm_hist.begin(), norm_hist.end(), 0) < 0.10){
		//	//cout << "C:" << std::accumulate(norm_hist.begin() + 9, norm_hist.end(), 0) << endl;
		//	b_valid = true;
		//}

		if (std::accumulate(norm_hist.begin(), norm_hist.end(), 0) < 20)
			return true;

	case OF_TOP:
		b_valid = false;
		// if first two blobs contribute >85% of the whole, then its still
		if (std::accumulate(norm_hist.begin(), norm_hist.begin() + 2, 0) / (float)std::accumulate(norm_hist.begin(), norm_hist.end(), 0) > 0.8)
			b_valid = true;

		break;

	default:
		break;
	}

	return b_valid;
}
