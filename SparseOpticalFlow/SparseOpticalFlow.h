#pragma once

#include "opencv2/opencv.hpp"

namespace video_processor {
	class SparseOpticalFlow {
	public:
		static enum AreaType
		{
			OF_TOP_LEFT = 1 << 0,
			OF_MIDDLE = 1 << 1,
			OF_TOP = 1 << 2,
			OF_MIDDLE_SG = 1 << 3,
		};

		int DFRMS = 5;	// Number of frame difference for optical flow
		int FRAME_WIDTH = 480;	// Frame size for optical flow
		int FRAME_HEIGHT = 270;	// Frame size for optical flow
		int PYRAMID_WIN_WIDTH = 25;
		int PYRAMID_WIN_HEIGHT = 25;
		int PYRAMID_MAX_LEVEL = 3;
		int NORM_HIST_NBINS = 20;

	private:
		// Members for input
		std::vector<AreaType> _area_types;
		std::vector<cv::Mat> _images;
		// Members for output
		std::map<AreaType, std::vector<int>> _each_still_array;
		std::map<AreaType, std::vector<float>> _each_score_array;

	public:
		// Constructor
		SparseOpticalFlow(const int area_type);

		// Push back image (also extend output arrays)
		void push_back_image(const cv::Mat& img);

		// Dispose image (by frame number)
		void dispose_image(const int frm_num);

		// Get still arry (by area type)
		std::vector<int> get_still_array(const AreaType area_type) const { return this->_each_still_array.at(area_type); };
		std::vector<float> get_score_array(const AreaType area_type) const { return this->_each_score_array.at(area_type); };

		// Run optical flow
		void run(const int frm_num);

		// Run optical flow (not include frm_num_end)
		void run(const int frm_num_begin, const int frm_num_end);

	private:
		// Run optical flow
		static std::vector<cv::Point2f> __set_prev_pts(const cv::Size frame_sz, const AreaType area_type);
		static std::vector<int> __calc_norm_hist(const std::vector<cv::Point2f>& prevPts, const std::vector<cv::Point2f>& nextPts, const std::vector<uchar>& status, const float max_movement, const int nBins);
		static bool __is_still_scene(const std::vector<int>& norm_hist, const AreaType area_type);
	};
}

#pragma comment(lib, "SparseOpticalFlow.lib")
