When creating a project you'll need to link tesseract, this version of opencv, and leptonica for libs
opencv only for headers
you'll need to use corresponding dll's as well
a sample of how to extract text is here:
#########################################################

#include "opencv2/text.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs/imgcodecs.hpp"
#include "opencv2/core/core.hpp"

#include <iostream>

using namespace std;
using namespace cv;
using namespace cv::text;

int main() {
	Mat img = imread("X:\\ausdetresults\\img000094.jpg");
	Ptr<OCRTesseract> ocr = OCRTesseract::create();  //Initialises tesseract
	vector<Rect> rects;
	vector<string> results;
	vector<float> confs;
	string answer;
	ocr->run(img, answer, &rects, &results, &confs);//, OCR_LEVEL_TEXTLINE);  //Calls tesseract
	for (int i = 0; i < rects.size(); ++i)
	{
		rectangle(img, rects[i], Scalar(100, 100, 100), 2);
		cout << "Result: " << results[i] << endl;
		cout << "Confidence: " << confs[i] << endl;
		putText(img, results[i] + " " + to_string(confs[i]), Point(rects[i].x, rects[i].y), CV_FONT_HERSHEY_PLAIN, 1, Scalar(255,0, 255));
	}
	imshow("result", img);
	waitKey(0);
	return 0;
}
##################################
This isn't the only way though, there's many possibilities for this from finding the text in the images before you send it to OCR
or changing OCR_LEVEL_TEXTLINE.  Please look over the tesseract docs as well as the opencv text module documents for examples and 
references that we can possibly test out.

Once you have the results you'll want to look for key things:
Race distances
Course locations
Finishing times

You can start using this on Australia as currently we only have the english language pack, but other languages do exist.

In order of importance see if you can, on a consistent basis, find the following:
1.  Location
2.  Distance of the race
3.  Race number
4.  Winning race number

Ideally you should run the OCR on all frames of the race for now, then run checks using string literals to find information deemed important.  Since distances
are always 4 digits and "m" it's easy to construct one.

Race number is always a R followed by 1 or 2 numbers.

Troubleshoot issues and use different possible combinations of string literals for example:
1000m may actually be seen by the detector as IOOO or l0O0 or some combination of these.

The race locations are in the title of the videos/foldernames so we can create a string enum of them and check for them or variations of their names
using like string literals.  

Just try different combinations then provide detailed results as to:
Item looking for
Number of races tested
Successful
Failure
Reasons for failure - This should manually checked on a case by case basis, and you come up with some reasonable explanation as to why it might not work

A reason for failure shouldn't be invalid string literal, the only real reasons for failures should be the graphics are obscured, don't exist, or
the detected result is just completely off.  For example if horse just gets detected as "lkjasd" for some reason.