//
//#include <windows.h>
//#include "opencv2/text.hpp"
//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/imgproc/imgproc.hpp"
//#include "opencv2/imgcodecs/imgcodecs.hpp"
//#include "opencv2/core/core.hpp"
//
//#include <iostream>
//
//using namespace std;
//using namespace cv;
//using namespace cv::text;
//
//int main() {
//	Mat img = imread("X:\\ausdetresults\\img000094.jpg");
//	Ptr<OCRTesseract> ocr = OCRTesseract::create();  //Initialises tesseract
//	vector<Rect> rects;
//	vector<string> results;
//	vector<float> confs;
//	string answer;
//	ocr->run(img, answer, &rects, &results, &confs);//, OCR_LEVEL_TEXTLINE);  //Calls tesseract
//	for (int i = 0; i < rects.size(); ++i)
//	{
//		rectangle(img, rects[i], Scalar(100, 100, 100), 2);
//		cout << "Result: " << results[i] << endl;
//		cout << "Confidence: " << confs[i] << endl;
//		putText(img, results[i] + " " + to_string(confs[i]), Point(rects[i].x, rects[i].y), CV_FONT_HERSHEY_PLAIN, 1, Scalar(255, 0, 255));
//	}
//	imshow("result", img);
//	waitKey(0);
//	return 0;
//}
#include <iostream>
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>

int main()
{
	char* outText;

	tesseract::TessBaseAPI* api = new tesseract::TessBaseAPI();
	// Initialize tesseract-ocr with English, without specifying tessdata path
	if (api->Init(NULL, "eng")) {
		fprintf(stderr, "Could not initialize tesseract.\n");
		exit(1);
	}

	// Open input image with leptonica library
	//Pix* image = pixRead("T:/Desktop/OCR/OCRProject/sample.tif");
	Pix* image = pixRead("T:/Frames/Caulfield 2018-06-30 R01 ~ 5803772601001/img000006.jpg");
	std::cout << (image == nullptr) << std::endl;
	api->SetImage(image);
	// Get OCR result
	outText = api->GetUTF8Text();
	printf("OCR output:\n%s", outText);

	// Destroy used object and release memory
	api->End();
	delete[] outText;
	pixDestroy(&image);

	return 0;
}