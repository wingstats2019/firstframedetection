#include<iostream>
#include"SparseOpticalFlow.h"
#include <opencv2/core/core.hpp>

#include "boost/filesystem.hpp"

using namespace boost::filesystem;
using namespace video_processor;
using namespace std;

SparseOpticalFlow optFlow(SparseOpticalFlow::AreaType::OF_MIDDLE);

int main()
{
	string pathStr = "T://Frames_Input//Caulfield 2018-07-14 R06 ~ 5809204144001";
	path p(pathStr);
	int i = 0;
	for (directory_entry& x : directory_iterator(p))
	{
		i++;
		cout << " " << x.path() << endl;
		cv::Mat mat=cv::imread(x.path().string());
		optFlow.push_back_image(mat);
/*
		cv::imshow(x.path().string(),mat);
		cv::waitKey(1);*/
	}
	for (int j = 0; j < i; j++)
	{
		optFlow.run(j);
	}
	int index = 0;
	auto stillArray=optFlow.get_still_array(SparseOpticalFlow::AreaType::OF_MIDDLE);
	for (auto it = stillArray.begin(); it != stillArray.end(); it++)
	{
		cout <<index++<<": "<< *it << endl;
	}
	cout << "Seperarte" << endl;
	 index = 0;

	auto scoreArray = optFlow.get_score_array(SparseOpticalFlow::AreaType::OF_MIDDLE);
	for (auto it = scoreArray.begin(); it != scoreArray.end(); it++)
	{
		cout << index++ << ": " << *it << endl;
	}
	return 1;
}