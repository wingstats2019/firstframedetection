//#include <caffe/caffe.hpp>
#ifdef USE_OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#endif  // USE_OPENCV
#include <algorithm>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <string>
#include <stdlib.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */


#include "helper/helper.h"

#include "boost/filesystem.hpp"

using namespace std;
using namespace cv;
using namespace boost::filesystem;

//using namespace VideoProcessor;
#include "boost/program_options.hpp"
namespace po = boost::program_options;
string _gt_dir;
string _vid_dir;
void main(int argc, char** argv)
{

	VideoCapture vc(_vid_dir);
	if (!vc.isOpened())
	{

		return;
	}
	int frameCount = 0;
	while (true)
	{
		Mat frame;
		vc >> frame;
		if (frame.empty())
			break;
		imwrite(format("%s%s",_gt_dir , frameCount),frame );
		frameCount++;
	}

}



bool parse_inputArg(int argc, char** argv)
{

	po::options_description desc("Options");
	desc.add_options()
		("help", "Print help message")
		//("video", po::value<string>(&_video_root), "Video root")
		("vid", po::value<string>(&_vid_dir), "Vid path")
		("gt", po::value<string>(&_gt_dir), "Gt path")
		("key_object", "Visualise Key Object")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	// If "help"
	if (vm.count("help")) {
		cout << desc << endl;
		return false;
	}

	// Display video path, location, scene model directory and letter model xml path
	cout << "=== Input Roots & Directories ===" << endl;
	cout << "_gt_dir   = " << _gt_dir << endl;
	cout << "=================================" << endl;

	// Return true
	return true;
}