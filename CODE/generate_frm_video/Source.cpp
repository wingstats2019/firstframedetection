//#include <caffe/caffe.hpp>
#ifdef USE_OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#endif  // USE_OPENCV
#include <algorithm>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <string>
#include <stdlib.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */


#include "helper/helper.h"

#include "boost/filesystem.hpp"

using namespace std;
using namespace cv;
using namespace boost::filesystem;

//using namespace VideoProcessor;
#include "boost/program_options.hpp"
namespace po = boost::program_options;
string _gt_dir="frames";
string _vid_dir = "D://RawVideos_SG/Short Race/One camera change";



bool parse_inputArg(int argc, char** argv);

void videoToFrame(string fileName);

void main(int argc, char** argv)
{
	parse_inputArg(argc, argv);

	path p(_vid_dir);
	for (auto i = directory_iterator(p); i != directory_iterator(); i++)
	{
		cout << i->path().stem().string() << endl;
		videoToFrame(i->path().stem().string());
	}

}

void videoToFrame(string fileName)
{
	string _vid_file = format("%s/%s.mp4", _vid_dir.c_str(), fileName);
	cout << _vid_file << endl;

	string _gt_folder= format("%s/%s", _gt_dir.c_str(), fileName);
	cout << _gt_dir << endl;
	cout << _gt_dir.c_str() << endl;
	cout << _gt_folder << endl;
	if (!boost::filesystem::exists(_gt_folder))
	{
		boost::filesystem::create_directories(_gt_folder);
	}
	VideoCapture vc(_vid_file);
	if (!vc.isOpened())
	{
		return;
	}
	int frameCount = 0;
	while (true)
	{
		Mat frame;
		vc >> frame;
		if (frame.empty())
			break;
		/*imshow("ASDF", frame);
		waitKey(1);*/

		if (frameCount % 5 == 0)
		{

			string result = format("%s/%s_%04d.bmp",  _gt_folder,fileName, frameCount);
			cout << result << endl;

			/*	char result[10];

				sprintf(result, "%02d.jpg", frameCount);
				cout << result << endl;*/
				//cout << ss.str()<<endl;
			resize(frame,frame,Size(227,227));
			imwrite(result, frame);

		}
		frameCount++;
		if (frameCount > 500)
			break;
		/*
		std::stringstream ss;
		ss<< "C://Users/statsuser/Desktop/SG_Start_Frm_CNN/"<< frameCount;
		imwrite(ss.str(), frame);
		frameCount++;*/

	}
}



bool parse_inputArg(int argc, char** argv)
{

	po::options_description desc("Options");
	desc.add_options()
		("help", "Print help message")
		//("video", po::value<string>(&_video_root), "Video root")
		("vid", po::value<string>(&_vid_dir), "Vid path")
		("gt", po::value<string>(&_gt_dir), "Gt path")
		("key_object", "Visualise Key Object")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	// If "help"
	if (vm.count("help")) {
		cout << desc << endl;
		return false;
	}

	// Display video path, location, scene model directory and letter model xml path
	cout << "=== Input Roots & Directories ===" << endl;
	cout << "_gt_dir   = " << _gt_dir << endl;
	cout << "=================================" << endl;

	// Return true
	return true;
}