#define _CRT_SECURE_NO_WARNINGS


#include <map>
#include "opencv2/text.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs/imgcodecs.hpp"
#include "opencv2/core/core.hpp"

#include <regex>
#include <iostream>
#include <fstream>
#include <string>

#include "boost/program_options.hpp"
#include "boost/filesystem/path.hpp"
#include "boost/filesystem.hpp"


#include "SparseOpticalFlow.h"

using namespace std;
using namespace cv;
using namespace cv::text;
namespace po = boost::program_options;

namespace vp = video_processor;

string _gt_root, _data_dir;
std::map<string, int> ReadFirstFrameData();
std::map<string, int> ReadEndFrameData();


bool parse_inputArg(int argc, char** argv);
int getRaceLength(string path, int raceRound);
const float framePerDistance = 1.509183;
const float raceDistancePower = 1.085;
const float raceDistanceRatio = 0.8141081435;

map<int, int> errorByRaceLength;

string optflowOutputPath;

void readEndFrameScoreboard(string path);
int opticalFlowTrack(string resultPath, string pathStr, int from, int to);
int ReadMaxErrors();


int main01(int argc, char** argv)
{
	// Parse input argument
	parse_inputArg(argc, argv);

	vector<vector<int>> confusion_mat = vector<vector<int>>(2, vector<int>(2));
	vector<string> unique_raceLabels;
	//

	boost::filesystem::directory_iterator end_itr;
	cout << _data_dir.c_str() << endl;
	boost::filesystem::path rootPath(_data_dir.c_str());
	for (boost::filesystem::directory_iterator it(rootPath); it != end_itr; it++)
	{
		if (boost::filesystem::is_directory(it->path().string()))
		{
			unique_raceLabels.push_back(it->path().string());
			cout << it->path().string() << endl;
		}
	}
	for (int i = 0; i < unique_raceLabels.size(); i++) {
		boost::filesystem::path p(unique_raceLabels[i]);
		string lastFile;

		for (boost::filesystem::directory_iterator it(p); it != end_itr; it++)
		{
			cout << it->path() << endl;
			lastFile = it->path().string();
		}
		cout << lastFile << endl;

		cout << endl;

		readEndFrameScoreboard(lastFile);
	}
	system("pause");
}

int main(int argc, char** argv) {

	ofstream of("steps.csv"); 
	of << "race,raceLength,steptoOcr,endFrame,gtEndframe,Difference" << endl;


	std::cout << argv[0] << endl;

	parse_inputArg(argc, argv);

	ReadMaxErrors();
	boost::filesystem::path p(argv[0]);
	std::cout << p.parent_path().string() << endl;
	_gt_root = p.parent_path().string();
	auto firstFrameData = ReadFirstFrameData();
	auto endFrameData = ReadEndFrameData();

	for (auto it = firstFrameData.begin(); it != firstFrameData.end(); it++)
	{
		string folderName = it->first;
		cout << "Folder name" << folderName << endl;
		regex raceIDRgx("R\\d{2}");
		std::smatch raceIDMatch;
		std::regex_search(folderName, raceIDMatch, raceIDRgx);

		string raceRoundStr = raceIDMatch[0];
		int raceRound = std::stoi(raceRoundStr.substr(1));
		cout << raceRound << endl;

		string folderPath = _data_dir + "/" + folderName;
		std::cout << "File path is " + folderPath << endl;

		of << folderName << ",";
		int raceLength = -1;
		int resultEndFrame;
		for (int i = 0; i < 100; i++)
		{
			char fileNameC[6];
			//sprintf(fileNameC, "%06d", it->second - 5 + i);
			sprintf(fileNameC, "%06d", it->second + i);

			//sprintf(fileNameC, "%06d", 0);
			string fileName = fileNameC;
			fileName = "img" + fileName + ".jpg";
			std::cout << fileName << endl;
			raceLength = getRaceLength(folderPath + "/" + fileName, raceRound);

			if (raceLength != -1)
			{
				std::cout << fileName << ": " << raceLength << endl;
				//int resultEndFrame = (framePerDistance * raceLength + it->second);
				resultEndFrame = (raceDistanceRatio * pow(raceLength, raceDistancePower) + it->second);
				int difference = endFrameData[it->first] - resultEndFrame;
				of << raceLength << "," << i << "," << resultEndFrame << "," << endFrameData[it->first] << "," << difference << endl;
				break;
			}
		}
		if (raceLength == -1)
			of << "-1,-1,-1,-1" << endl;
		else
		{
			int error = 1;
			int loopCount=0;
			for (auto it = errorByRaceLength.begin();it!=errorByRaceLength.end();it++)
			{
				loopCount++;
				if (it->first >= raceLength)
				{
					error = it->second;
					break;
				}
			}
			cout << "Error " << error << endl;
			cout << "raceLength " << raceLength << endl;
			cout << "Loopcount " << loopCount <<endl;

			int from = resultEndFrame - error;
			int to = resultEndFrame + error;
			//TODO
			string optFileName = optflowOutputPath+"\\"+folderName+".csv";
			opticalFlowTrack(optFileName, folderPath, from, to);
		}


	}

	of.close();
	return 0;
}

int getRaceLength(string path, int raceRound)
{
	Mat img = imread(path);
	if (img.empty())
		return -1;
	std::cout << path << endl;
	//imshow("here", img);
	//cv::waitKey(0);
	Ptr<OCRTesseract> ocr = OCRTesseract::create();  //Initialises tesseract
	vector<Rect> rects;
	vector<string> results;
	vector<float> confs;
	string answer;
	ocr->run(img, answer, &rects, &results, &confs);//, OCR_LEVEL_TEXTLINE);  //Calls tesseract

	std::cout << "result Size" << rects.size() << endl;
	string rgxStr = "\\d{3,4}[mM]";
	string rgxPrefix = "R" + std::to_string(raceRound);
	rgxStr = rgxPrefix + rgxStr;
	cout << "regex " << rgxStr << endl;
	regex rgx(rgxStr);
	std::smatch sm;
	int result = -1;
	for (int i = 0; i < rects.size(); ++i)
	{
		std::regex_search(results[i], sm, rgx);
		if (sm.size() > 0 && result == -1)
		{
			std::cout << "Result: " << results[i] << endl;
			string match = sm[0];
			match = match.substr(rgxPrefix.length(), match.length() - 3);
			std::cout << match << endl;

			result = std::stoi(match);
		}
		rectangle(img, rects[i], Scalar(100, 100, 100), 2);
		//std::cout << "Result: " << results[i] << endl;
		//std::cout << "Confidence: " << confs[i] << endl;
		putText(img, results[i] + " " + to_string(confs[i]), Point(rects[i].x, rects[i].y), CV_FONT_HERSHEY_PLAIN, 1, Scalar(255, 0, 255));
	}
	//imshow("result", img);
	//waitKey(0);
	return result;
}



void readEndFrameScoreboard(string path)
{
	Mat img = imread(path);
	if (img.empty())
		return;
	std::cout << path << endl;
	//imshow("here", img);
	//cv::waitKey(0);
	Ptr<OCRTesseract> ocr = OCRTesseract::create();  //Initialises tesseract
	vector<Rect> rects;
	vector<string> results;
	vector<float> confs;
	string answer;
	ocr->run(img, answer, &rects, &results, &confs);//, OCR_LEVEL_TEXTLINE);  //Calls tesseract


	for (int i = 0; i < rects.size(); ++i)
	{
		cout << rects[i] << endl;
		rectangle(img, rects[i], Scalar(100, 100, 100), 2);
		//std::cout << "Result: " << results[i] << endl;
		//std::cout << "Confidence: " << confs[i] << endl;
		putText(img, results[i] + " " + to_string(confs[i]), Point(rects[i].x, rects[i].y), CV_FONT_HERSHEY_PLAIN, 1, Scalar(255, 0, 255));
	}
	imshow("result", img);
	waitKey(0);
	return;
}


map<string, int> ReadFirstFrameData()
{
	map<string, int> result;
	stringstream ss;
	ss << _gt_root << "/FrameDetectResult.csv";
	std::cout << ss.str() << endl;
	std::ifstream groundTruthCsv(ss.str());
	string line;
	bool isFirstLine = true;
	std::cout << groundTruthCsv.eof() << endl;
	while (std::getline(groundTruthCsv, line))
	{
		istringstream templine(line);
		string data;
		string raceName = "";
		int frameID = 0;
		int frameEndID = 0;
		stringstream raceNameStream;
		std::smatch m;
		regex rgx("R[0-9]{2}");

		for (int i = 0; std::getline(templine, data, ','); i++)
		{
			if (i == 0)
			{
				bool found = regex_search(data, m, rgx);
				//raceNameStream << data.substr(0, m.position(0) + 3) << " ~ " << data.substr(m.position(0) + 3);
				//raceName = raceNameStream.str();
				raceNameStream << data;
				raceName = data;
				std::cout << raceNameStream.str() << " ";
				m.empty();
			}
			else if (i == 3)
			{
				frameID = std::atoi(data.c_str());
				std::cout << data << " ";
			}
		}

		if (isFirstLine)
		{
			isFirstLine = false;
			continue;
		}
		std::cout << "=========================================" << endl;
		std::cout << raceName << ": " << frameID << endl;
		result[raceName] = frameID;
		std::cout << endl;
	}
	return result;
}


bool parse_inputArg(int argc, char** argv)
{

	po::options_description desc("Options");
	desc.add_options()
		("help", "Print help message")
		("gt", po::value<string>(&_gt_root), "GT root")
		("data", po::value<string>(&_data_dir), "Data dir")
		("optflow", po::value<string>(&optflowOutputPath), "Optical flow output path")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	// If "help"
	if (vm.count("help")) {
		std::cout << desc << endl;
		return false;
	}

	// Display video path, location, scene model directory and letter model xml path
	std::cout << "=== Input Roots & Directories ===" << endl;
	std::cout << "_data_dir   = " << _data_dir << endl;
	//std::cout << "_gt_root   = " << _gt_root << endl;
	std::cout << "=================================" << endl;

	// Return true
	return true;
}



map<string, int> ReadEndFrameData()
{
	map<string, int> result;
	stringstream ss;
	ss << _gt_root << "/RacingComVideos.csv";
	std::cout << ss.str() << endl;
	std::ifstream groundTruthCsv(ss.str());
	string line;
	bool isFirstLine = true;
	cout << "A" << endl;
	while (std::getline(groundTruthCsv, line))
	{
		cout << "B" << endl;

		istringstream templine(line);
		string data;
		string raceName = "";
		int frameID = 0;
		int frameEndID = 0;
		stringstream raceNameStream;
		std::smatch m;
		regex rgx("R[0-9]{2}");

		for (int i = 0; std::getline(templine, data, ','); i++)
		{
			if (i == 0)
			{
				bool found = regex_search(data, m, rgx);
				raceNameStream << data.substr(0, m.position(0) + 3) << " ~ " << data.substr(m.position(0) + 3);
				raceName = raceNameStream.str();
				std::cout << raceNameStream.str() << " ";
				m.empty();
			}
			else if (i == 4)
			{
				frameID = std::atoi(data.c_str());
				std::cout << data << " ";
			}


		}

		if (isFirstLine)
		{
			isFirstLine = false;
			continue;
		}
		result[raceName] = frameID;
		std::cout << endl;
	}
	return result;
}


int opticalFlowTrack(string resultPath, string pathStr, int from, int to)
{
	if (from > to)
	{
		cout << "Error" << endl;
		return 0;
	}
	ofstream exportPath(resultPath);
	vp::SparseOpticalFlow optFlow(vp::SparseOpticalFlow::AreaType::OF_MIDDLE);
	vp::SparseOpticalFlow optFlowTop(vp::SparseOpticalFlow::AreaType::OF_TOP);
	vp::SparseOpticalFlow optFlowSG(vp::SparseOpticalFlow::AreaType::OF_MIDDLE_SG);
	vp::SparseOpticalFlow optFlowTopLeft(vp::SparseOpticalFlow::AreaType::OF_TOP_LEFT);
	boost::filesystem::path p(pathStr);
	int i = 0;
	for (boost::filesystem::directory_entry& x : boost::filesystem::directory_iterator(p))
	{
		i++;
		if (i >= from && i <= to)
		{
			cout << " " << x.path() << endl;
			cv::Mat mat = cv::imread(x.path().string());
			optFlow.push_back_image(mat);
			optFlowTop.push_back_image(mat);
			optFlowSG.push_back_image(mat);
			optFlowTopLeft.push_back_image(mat);
		}
	}
	for (int j = 0; j < to-from; j++)
	{
		optFlow.run(j);
		optFlowTop.run(j);
		optFlowSG.run(j);
		optFlowTopLeft.run(j);
	}
	int index = 0;

	auto scoreArray = optFlow.get_score_array(vp::SparseOpticalFlow::AreaType::OF_MIDDLE);
	auto scoreArrayTop = optFlowTop.get_score_array(vp::SparseOpticalFlow::AreaType::OF_TOP);
	auto scoreArraySG = optFlowSG.get_score_array(vp::SparseOpticalFlow::AreaType::OF_MIDDLE_SG);
	auto scoreArrayTopLeft = optFlowTopLeft.get_score_array(vp::SparseOpticalFlow::AreaType::OF_TOP_LEFT);
	int j = 0;
	
	auto it= scoreArray.begin();
	auto itTop = scoreArrayTop.begin();
	auto itSG = scoreArraySG.begin();
	auto itTopLeft = scoreArrayTopLeft.begin();

	for (; it != scoreArray.end(); it++,itTop++,itSG++,itTopLeft++)
	{
		exportPath << from + (j++) << ", " << *it <<", " << *itTop <<", " << *itSG <<", " << *itTopLeft << endl;
	}
	exportPath.close();
	return 0;
}


int ReadMaxErrors()
{
	map<string, int> result;
	stringstream ss;
	ss << _data_dir << "\\raceLengthError.csv";
	std::cout << ss.str() << endl;
	std::ifstream errorCSV(ss.str());
	string line;
	bool isFirstLine = true;

	while (std::getline(errorCSV, line))
	{
		istringstream templine(line);
		string data;
		int raceLength=0;
		int maxDifference=0;

		if (isFirstLine)
		{
			isFirstLine = false;
			continue;
		}
		for (int i = 0; std::getline(templine, data, ','); i++)
		{
			if (i == 0)
			{
				raceLength = stoi(data);
			}
			else if (i == 1)
			{
				maxDifference = stoi(data);
			}
		}
		errorByRaceLength[raceLength] = maxDifference;

		std::cout << "=========================================" << endl;
		std::cout << endl;
	}
	return 0;
}