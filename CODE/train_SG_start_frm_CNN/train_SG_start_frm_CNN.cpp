//#include <caffe/caffe.hpp>
#ifdef USE_OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#endif  // USE_OPENCV
#include <algorithm>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <string>
#include <stdlib.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */


#include "helper/helper.h"

#include "boost/filesystem.hpp"
using namespace boost::filesystem;

#include "boost/program_options.hpp"
namespace po = boost::program_options;

//using namespace caffe;
using namespace cv;
using namespace std;


//#define BUILD_LMDB

// image destination paramters

// txt for creating lmdb database
string _train_txt = "train.txt";
string _test_txt = "test.txt";

string _exe_root;

// solver for caffe
string _solver_path = "start_frm_cnn_solver.prototxt";
string _model_path = "bvlc_alexnet.caffemodel";
// Label file
const std::string label_file = "synset_words.txt";


//argv
string _train_dataset_path, _output_dataset_path;
bool _skip_training = false;

bool parse_inputArg(int argc, char** argv);
// Generate the helper text file with filename and label 
void write_image_txt_helper(string train_dataset_path)
{
	std::vector<std::string> labels_;
	std::ifstream labels(label_file.c_str());
	string line;
	while (getline(labels, line))
		labels_.push_back(string(line));

	const float IDEN_THRES = 0.8;
	std::ofstream myfile_train;
	std::ofstream myfile_test;
	myfile_train.open(_train_txt, std::ofstream::out | std::ofstream::app);
	myfile_test.open(_test_txt, std::ofstream::out | std::ofstream::app);

	directory_iterator end_itr;
	cout << train_dataset_path << endl;
	// cycle through the directory
	for (directory_iterator itr(train_dataset_path); itr != end_itr; ++itr)
	{
		if (is_directory(itr->path())) {
			// Label is the name of the directory
			string label = itr->path().stem().string();
			string corr_label = label;
			//for (string eachLabel : labels_){
				//if (eachLabel.substr(1, eachLabel.size()) == label){
				//	corr_label = eachLabel.substr(0, 1);
				//}
			//}
			//cout << train_dataset_path + "/" + label << endl;
			for (directory_iterator imgs_itr(itr->path().string()); imgs_itr != end_itr; ++imgs_itr) {
				if (is_regular_file(imgs_itr->path())) {
					// Use as Train data if the random Num>=0.8, else use it as test data 
					if ((float)(rand() % 10) / 10 >= IDEN_THRES)
						myfile_test << format("%s/%s", label, imgs_itr->path().filename().string()) << " " << corr_label << endl;
					else
						myfile_train << format("%s/%s", label, imgs_itr->path().filename().string()) << " " << corr_label << endl;
				}
			}
		}
	}
	myfile_train.close();
	myfile_test.close();

}


void create_lmdb(string img_dire, string img_txt, string db_output)
{
	stringstream exess;
	const string exe = "convert_imageset.exe";
	const string backend = "-backend lmdb";
	const string shuffle = "-shuffle";
	const string img_dir = img_dire + "\\";
	const string textfile = img_txt;
	const string output = db_output;

	cout << "output " << output << endl;

	exess << exe.c_str() << " " << backend.c_str() << " " << shuffle.c_str() << " " << img_dir.c_str() << " " << textfile.c_str() << " " << _output_dataset_path << output.c_str();
	cout <<"the cmd \n"<< exess.str() << endl;
	system(exess.str().c_str());

	//char cmd[1000];
	//sprintf_s(cmd, "%s %s %s %s %s %s", exe.c_str(), backend.c_str(), shuffle.c_str(), img_dir.c_str(), textfile.c_str(), output.c_str());
	//cout << cmd << endl;
	//cout << endl;
}

void compute_image_mean()
{
	const string dbsource = "train_output";
	const string outputname = "mean.binaryproto";
	stringstream exess;
	exess << _exe_root << "\\compute_image_mean.exe ";
	exess << _output_dataset_path << dbsource.c_str() << " " << _output_dataset_path << outputname.c_str();
	cout << exess.str() << endl;
	system(exess.str().c_str());


	//char cmd[1000];

	//const string exe = "compute_image_mean.exe";

	//string exe = exess.str();
	//cout << exe << endl;
	//sprintf_s(cmd, "%s %s %s", exe.c_str(), dbsource.c_str(), outputname.c_str());
	//system(cmd);
}

void train_CNN_caller(const string & solver)
{
	char cmd[1000];
	const string caffe = "caffe";
	const string train = "train";
	const string solver_structure = "--solver=" + solver;
	const string weight_option = "--weights=" + _output_dataset_path + _model_path;
	sprintf_s(cmd, "%s %s %s %s", caffe.c_str(), train.c_str(), solver_structure.c_str(), weight_option.c_str());
	cout << "CMD\n" << cmd << endl;
	//sprintf_s(cmd, "%s %s %s", caffe.c_str(), train.c_str(), solver_structure.c_str() );
	system(cmd);
}

int main(int argc, char** argv) {
	cout << argv[0] << endl;
	parse_inputArg(argc, argv);
	cout << "output dataset "<< _output_dataset_path << endl;
	try
	{
		//if (argc < 2) {
		//	//cout << "Usage: .exe [Test Data Path] [Train Data Path] [FrameRoot]" << endl;
		//	cout << "Usage: .exe [Train Data Path][Output Path(Optional)]" << endl;
		//	return -1;
		//}

		//_train_dataset_path = argv[1];
		//if (argc > 2)
		//	_output_dataset_path = argv[2];
		//else
		//	_output_dataset_path = ".\\";

		remove("train.txt");
		remove("test.txt");


		boost::filesystem::path p(argv[0]);


		_exe_root = p.parent_path().string();

#ifdef BUILD_LMDB
		// Generate helper txt file
		srand(time(NULL));
		write_image_txt_helper(_train_dataset_path);
		//generate lmdb dataset


		create_lmdb(_train_dataset_path, _test_txt, "test_output");
		create_lmdb(_train_dataset_path, _train_txt, "train_output");

		//compute image mean
		compute_image_mean();
#endif // buildLmdb

		// start caffe training
		if (!_skip_training)
		{
			train_CNN_caller(_solver_path);
			system("pause");
		}

	}
	catch (const char* e) {
		cout << e << endl;
		system("pause");
	}
	catch (...) {
		system("pause");
	}
	//system("pause");
}



bool parse_inputArg(int argc, char** argv)
{
	po::options_description desc("Options");
	desc.add_options()
		("help", "Print help message")
		("data", po::value<string>(&_train_dataset_path), "Set the data input path")
		("output", po::value<string>(&_output_dataset_path), "Set the data output path")
		("setup", "Skip the training process")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	// If "help"
	if (vm.count("help")) {
		cout << desc << endl;
		return false;
	}
	if (!vm.count("output"))
	{
		String exePath = argv[0];
		boost::filesystem::path exeFilePath(exePath.c_str());
		_output_dataset_path = exeFilePath.parent_path().string()+"\\";
	}
	_skip_training = vm.count("setup");
	std::cout << "Done";
	// Return true
	return true;
}