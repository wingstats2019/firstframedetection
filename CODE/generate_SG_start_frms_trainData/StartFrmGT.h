#include "helper/helper.h"

//enum TrackType { PolyTrack = 'P', SC_TurfTrack = 'S', LC_TurfTrack = 'L' };
//enum IconType { P_Icon = 'P', S_Icon = 'S', L_Icon = 'L', N_Icon = 'N' };
enum FrmType { Still = 0, Move = 1 };

// Track icon 
struct StartFrm {
	// Members
	std::string racelabel;
	std::string imgName;
	//TrackType trackType;
	//IconType iconType;
	FrmType frmType;
	cv::Mat patch;			// Initially empty until read_from_dir() is called

	// Constructor
	StartFrm() {}
	StartFrm(const std::string racelabel_, const std::string imgName_, const FrmType FrmType_) :
		racelabel(racelabel_), imgName(imgName_), frmType(FrmType_)
	{}

	// The patch should be located in the patch {root}\{Type}\{Race}\{Race}_{imgName}
	void read_patch(const std::string & root) {
		const std::string path = cv::format("%s/%s/%s_%s", root.c_str(), racelabel.c_str(), racelabel.c_str(), imgName.c_str());
		std::cout << path << std::endl;
		this->patch = cv::imread(path);
		CV_Assert(!this->patch.empty());
	}
};

// Start Frm GT Class
class StartFrmGT : public std::vector<StartFrm> {
private:
	inline void check_headers(const std::vector<std::string> & headers);

public:
	inline StartFrmGT(const std::string & csv);
};

inline void StartFrmGT::check_headers(const std::vector<std::string> & headers)
{
	CV_Assert(headers.size() >= 1);
	CV_Assert(headers[0] == "RaceLabel");
}

inline StartFrmGT::StartFrmGT(const std::string & csv)
{
	// Parse CSV
	std::vector<std::string> headers;
	std::vector<std::vector<std::string>> content;
	parse_csv(csv, headers, content);

	// Parse header and check header
	check_headers(headers);

	// Parse content
	this->clear();
	for (const std::vector<std::string> & each_line : content) {
		CV_Assert(each_line.size() >= 2);
		const std::string racelabel = each_line[0];
		//const std::string type_str = each_line[1];
		//CV_Assert(type_str.size() == 1);

		for (int j = 1; j < each_line.size(); j++) {
			const int label = stoi(each_line[j]);
			if (label == -1)
				continue;
			else if (label == 0) {
				StartFrm StartFrm(racelabel, headers[j], FrmType::Still);
				this->push_back(StartFrm);
			}
			else {
				StartFrm StartFrm(racelabel, headers[j], FrmType::Move);
				this->push_back(StartFrm);
			}
		}
	}
}

inline void visualise_startFrms(const std::vector<StartFrm> & StartFrms, const std::string & dir, cv::Size &out_Size)
{
	_mkdir(dir.c_str());

#pragma omp parallel for
	for (int i = 0; i < StartFrms.size(); i++) {
		const StartFrm icon = StartFrms[i];
		CV_Assert(!icon.patch.empty());
		//const char type_char = icon;
		const int type_int = icon.frmType;
		_mkdir(cv::format("%s/%d", dir.c_str(), type_int).c_str());
		cv::Mat resized_img;
#pragma omp critical
		cv::resize(icon.patch.clone(), resized_img, out_Size);
		imwrite(cv::format("%s/%d/%s_%s", dir.c_str(), type_int, icon.racelabel, icon.imgName), resized_img);
	}
}