//#include "VideoProcessor/VideoProcessor.h"
//#include "RaceGT.h"
#include "StartFrmGT.h"

using namespace std;
using namespace cv;
using namespace boost::filesystem;

//using namespace VideoProcessor;
#include "boost/program_options.hpp"
namespace po = boost::program_options;


string _video_root, _gt_dir;

//string _raceGt_fn = "RaceGT.csv";
//string _trackIconGt_fn = "TrackIconGT.csv";
string _startFrmGT_fn = "StartFrmGT.csv";
//string _trackIconGt_fn = "TrackIconGT.csv";

bool parse_inputArg(int argc, char** argv);

int main(int argc, char** argv)
{
	// Parse input argument
	parse_inputArg(argc, argv);
	// Initialise track icon GT
	cout << format("%s/%s", _gt_dir.c_str(), _startFrmGT_fn.c_str()) << endl;
	//system("pause");
	//return 1;
	StartFrmGT startFrmGT(format("%s/%s", _gt_dir.c_str(), _startFrmGT_fn.c_str()));
	// Read all patch from "All_patches"
#pragma omp parallel for
	for (int i = 0; i < startFrmGT.size(); i++){
		startFrmGT[i].read_patch(_gt_dir);
	}
	// Visualise all patches
	visualise_startFrms(startFrmGT, "./Train_SG_start_frm_CNN", cv::Size(227, 227));
}

bool parse_inputArg(int argc, char** argv)
{

	po::options_description desc("Options");
	desc.add_options()
		("help", "Print help message")
		//("video", po::value<string>(&_video_root), "Video root")
		("gt", po::value<string>(&_gt_dir), "Gt path")
		("key_object", "Visualise Key Object")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	// If "help"
	if (vm.count("help")) {
		cout << desc << endl;
		return false;
	}

	// Display video path, location, scene model directory and letter model xml path
	cout << "=== Input Roots & Directories ===" << endl;
	cout << "_gt_dir   = " << _gt_dir << endl;
	cout << "=================================" << endl;

	// Return true
	return true;
}