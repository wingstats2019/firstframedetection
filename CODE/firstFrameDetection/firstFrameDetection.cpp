
#include <regex>
#include "caffe\caffe.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "../generate_SG_start_frms_trainData/StartFrmGT.h"
#include "SparseOpticalFlow.h"

//#include "TrackIconGT.h"
using namespace boost::filesystem;

using namespace std;
using namespace cv;
using namespace caffe;

#include "boost/program_options.hpp"
namespace po = boost::program_options;

namespace vp = video_processor;

string _gt_root, _race_region, _data_dir, _config_root, _output_path;
bool _is_jpg = false;

// For Caffe CNN
std::shared_ptr<caffe::Net<float> > net_;
int num_channels_;
cv::Size input_geometry_;
Mat mean_;
std::vector<std::string> labels_;

const std::string _model_file = "start_frm_cnn_deploy.prototxt";
const std::string _trained_file = "start_frm_cnn_iter_50000.caffemodel";
const std::string _mean_file = "mean.binaryproto";
const std::string _label_file = "synset_words.txt";

string _startFrGt_fn = "StartFrmGT.csv";
string _overallGt = "RacingComVideos.csv";

bool parse_inputArg(int argc, char** argv);
map<string, int> ReadFirstFrameData();

void Preprocess(const vector<Mat>& imgs, vector<Mat>* input_channels)
{
	/* Convert the input image to the input image format of the network. */
	for (int i = 0; i < imgs.size(); i++) {
		Mat img = imgs[i];
		Mat sample;
		if (img.channels() == 3 && num_channels_ == 1)
			cvtColor(img, sample, COLOR_BGR2GRAY);
		else if (img.channels() == 4 && num_channels_ == 1)
			cvtColor(img, sample, COLOR_BGRA2GRAY);
		else if (img.channels() == 4 && num_channels_ == 3)
			cvtColor(img, sample, COLOR_BGRA2BGR);
		else if (img.channels() == 1 && num_channels_ == 3)
			cvtColor(img, sample, COLOR_GRAY2BGR);
		else
			sample = img;

		Mat sample_resized;
		if (sample.size() != input_geometry_)
			resize(sample, sample_resized, input_geometry_);
		else
			sample_resized = sample;

		Mat sample_float;
		if (num_channels_ == 3)
			sample_resized.convertTo(sample_float, CV_32FC3);
		else
			sample_resized.convertTo(sample_float, CV_32FC1);

		Mat sample_normalized;
		subtract(sample_float, mean_, sample_normalized);

		vector<Mat> sample_split;
		split(sample_normalized, sample_split);

		for (int chn = 0; chn < num_channels_; chn++)
			sample_split[chn].copyTo((*input_channels)[i * num_channels_ + chn]);

	}

}

/* Wrap the input layer of the network in separate Mat objects
* (one per channel). This way we save one memcpy operation and we
* don't need to rely on cudaMemcpy2D. The last preprocessing
* operation will write the separate channels directly to the input
* layer. */
void WrapInputLayer(vector<Mat> * input_channels, const int NumOfimgs)
{
	Blob<float>* input_layer = net_->input_blobs()[0];

	int width = input_layer->width();
	int height = input_layer->height();
	float* input_data = input_layer->mutable_cpu_data();

	for (int i = 0; i < NumOfimgs * input_layer->channels(); ++i) {
		Mat channel(height, width, CV_32FC1, input_data);
		input_channels->push_back(channel);
		input_data += width * height;
	}
}

/* Load the mean file in binaryproto format. */
void SetMean(const string & mean_file)
{
	BlobProto blob_proto;
	ReadProtoFromBinaryFileOrDie(mean_file.c_str(), &blob_proto);

	/* Convert from BlobProto to Blob<float> */
	Blob<float> mean_blob;
	mean_blob.FromProto(blob_proto);
	CHECK_EQ(mean_blob.channels(), num_channels_)
		<< "Number of channels of mean file doesn't match input layer.";

	/* The format of the mean file is planar 32-bit float BGR or grayscale. */
	vector<Mat> channels;
	float* data = mean_blob.mutable_cpu_data();
	for (int i = 0; i < num_channels_; ++i) {
		/* Extract an individual channel. */
		Mat channel(mean_blob.height(), mean_blob.width(), CV_32FC1, data);
		channels.push_back(channel);
		data += mean_blob.height() * mean_blob.width();
	}

	/* Merge the separate channels into a single image. */
	Mat mean;
	merge(channels, mean);

	/* Compute the global mean pixel value and create a mean image
	* filled with this value. */
	Scalar channel_mean = cv::mean(mean);
	mean_ = Mat(input_geometry_, mean.type(), channel_mean);
}

vector<vector<float>> Predict(const vector<Mat> & imgs)
{
	Blob<float>* input_layer = net_->input_blobs()[0];
	input_layer->Reshape(imgs.size(), num_channels_,
		input_geometry_.height, input_geometry_.width);
	/* Forward dimension change to all layers. */
	net_->Reshape();

	vector<Mat> input_channels;
	WrapInputLayer(&input_channels, imgs.size());

	Preprocess(imgs, &input_channels);

	net_->Forward();

	/* Copy the output layer to a vector */
	Blob<float>* output_layer = net_->output_blobs()[0];

	vector<vector<float>> output_stack;
	const float* current = output_layer->cpu_data();
	for (int i = 0; i < imgs.size(); i++) {
		vector<float> output(current + output_layer->channels() * i, current + output_layer->channels() * (i + 1));
		output_stack.push_back(output);
	}

	return output_stack;
}

int opticalFlowTrack(string pathStr, int startRange, int endRange, int preferedIndex)
{
	vp::SparseOpticalFlow optFlow(vp::SparseOpticalFlow::AreaType::OF_MIDDLE);
	path p(pathStr);
	int i = 0;
	for (directory_entry& x : directory_iterator(p))
	{
		i++;
		if (startRange > i)
		{
			continue;
		}
		else if (endRange < i)
		{
			break;
		}
		cout << " " << x.path() << endl;
		cv::Mat mat = cv::imread(x.path().string());
		optFlow.push_back_image(mat);
	}
	for (int j = 0; j < i - startRange - 1; j++)
	{
		optFlow.run(j);
	}
	int index = 0;

	auto scoreArray = optFlow.get_score_array(vp::SparseOpticalFlow::AreaType::OF_MIDDLE);

	vector<int> markedIndex;
	int stillCounter = 0;
	for (auto it = scoreArray.begin(); it != scoreArray.end(); it++)
	{
		cout << startRange + index << ": " << *it << " still: " << stillCounter << endl;
		if (*it < 0.9f && *it >= 0)
		{
			if (stillCounter > 2)
				markedIndex.push_back(index);
			stillCounter = 0;
		}
		else if (*it >= 0.9)
			stillCounter++;
		index++;
	}
	int result = -1;
	float minDifference = -1;
	cout << "the results are ";
	for (auto it = markedIndex.begin(); it != markedIndex.end(); it++)
	{
		cout << "," << (*it + startRange);
		float currentDiffenerece = abs(*it - preferedIndex - startRange);
		if (minDifference < 0 || minDifference >= currentDiffenerece)
		{
			minDifference = currentDiffenerece;
			result = *it;
		}
	}
	cout << endl;
	if (result > 0)
		return result + startRange;
	else
		return preferedIndex;
}

int opticalFlowTrack(string pathStr)
{
	vp::SparseOpticalFlow optFlow(vp::SparseOpticalFlow::AreaType::OF_MIDDLE);
	path p(pathStr);
	int i = 0;
	for (directory_entry& x : directory_iterator(p))
	{
		i++;
		cout << " " << x.path() << endl;
		cv::Mat mat = cv::imread(x.path().string());
		optFlow.push_back_image(mat);
	}
	for (int j = 0; j < i - 1; j++)
	{
		optFlow.run(j);
	}
	int index = 0;

	auto scoreArray = optFlow.get_score_array(vp::SparseOpticalFlow::AreaType::OF_MIDDLE);
	int j = 0;

	for (auto it = scoreArray.begin(); it != scoreArray.end(); it++)
	{

		cout << j++ << ": " << *it << endl;
	}
	return 0;
}






int main(int argc, char** argv)
{

	auto gtMap = ReadFirstFrameData();
	for (auto it = gtMap.begin(); it != gtMap.end(); it++)
	{
		cout << it->first << " with " << it->second << endl;
	}

	// Parse input argument
	parse_inputArg(argc, argv);

	// Get the path
	//const std::string model_file = _config_root + "/VideoProcessor/start_frm/" + _model_file;
	//const std::string trained_file = _config_root + "/VideoProcessor/start_frm/" + _trained_file;
	//const std::string mean_file = _config_root + "/VideoProcessor/start_frm/" + _mean_file;
	//const std::string label_file = _config_root + "/VideoProcessor/start_frm/" + _label_file;


	const std::string model_file = _config_root + "/" + _model_file;
	const std::string trained_file = _config_root + "/" + _trained_file;
	const std::string mean_file = _config_root + "/" + _mean_file;
	const std::string label_file = _config_root + "/" + _label_file;

	cout << model_file << endl;
	cout << trained_file << endl;
	cout << mean_file << endl;
	cout << label_file << endl;

	// Load RaceGT
	StartFrmGT gt(format("%s/%s", _gt_root.c_str(), _startFrGt_fn.c_str()));
	/* Load the network. */
	net_.reset(new Net<float>(model_file, TEST));
	net_->CopyTrainedLayersFrom(trained_file);

	CHECK_EQ(net_->num_inputs(), 1) << "Network should have exactly one input.";
	CHECK_EQ(net_->num_outputs(), 1) << "Network should have exactly one output.";

	Blob<float>* input_layer = net_->input_blobs()[0];
	num_channels_ = input_layer->channels();
	CHECK(num_channels_ == 3 || num_channels_ == 1)
		<< "Input layer should have 1 or 3 channels.";
	input_geometry_ = Size(input_layer->width(), input_layer->height());

	/* Load the binaryproto mean file. */
	SetMean(mean_file);

	/* Load labels. */
	//std::ifstream labels(label_file.c_str());
	//CHECK(labels) << "Unable to open labels file " << label_file;
	//string line;
	//while (getline(labels, line))
	//	labels_.push_back(string(line));
	vector<string> img_suffixs;

	string filetype;
	//for (int i = 5; i <= 500; i += 5) {
	//	img_suffixs.push_back(format("%04d.bmp", i));
	//}
	if (_is_jpg)
		filetype = "jpg";
	else
		filetype = "bmp";
	if (_race_region.compare("SG") == 0)
		for (int i = 5; i <= 500; i += 5) {
			char filenameChar[10];
			sprintf(filenameChar, "%04d.%s", i, filetype);
			string fileName = filenameChar;
			img_suffixs.push_back(fileName);
			//img_suffixs.push_back(format("%04d.bmp", i));
		}
	else //if(_race_region.compare("AUS")==0)
		for (int i = 0; i < 300; i++) {
			char filenameChar[20];
			sprintf(filenameChar, "img%06d.%s", i, filetype);
			string fileName = filenameChar;
			img_suffixs.push_back(fileName);
			//img_suffixs.push_back(format("img%06d.bmp", i));
		}
	// Write the TrackIconGT
	std::ofstream ofs("FrameDetectResult.csv");
	ofs << "RaceLabel,FirstFrame,Differences,OpticalFirstFrame,Difference" << endl;
	vector<string> unique_raceLabels;
	for (StartFrm each_frm : gt) {
		//cout << each_frm.racelabel<< endl;
		//cout << each_frm.racelabel.length() << endl;
		//cout << stoi(each_frm.racelabel) <<endl;
		unique_raceLabels.push_back(each_frm.racelabel);
	}
	std::sort(unique_raceLabels.begin(), unique_raceLabels.end());
	auto last = std::unique(unique_raceLabels.begin(), unique_raceLabels.end());
	unique_raceLabels.erase(last, unique_raceLabels.end());

	vector<vector<int>> confusion_mat = vector<vector<int>>(2, vector<int>(2));

	for (int i = 0; i < unique_raceLabels.size(); i++) {
		string tar_raceLabel = unique_raceLabels[i];
		//string tar_track = gt.tracks[i];


		stringstream ss;
		ss << _data_dir << "/" << tar_raceLabel;
		//string tar_dir = format("%s/%s", _data_dir, tar_raceLabel);
		string tar_dir = ss.str();

		cout << "Processing" << tar_raceLabel << endl;

		// Set up for parallel imread
		int num_of_valid_img = 0;
		vector<string> valid_filenames;
		//for (int j = 0; j < img_suffixs.size(); j++) {
		//	string tar_filename = format("%s/%s_%s", tar_dir, tar_raceLabel, img_suffixs[j]);

		//	cout << tar_filename << endl;
		//	if (!exists(tar_filename)) {
		//		//each_race_data.push_back(Mat());
		//		continue;
		//	}
		//	num_of_valid_img++;
		//	valid_filenames.push_back(tar_filename);
		//}

		for (int j = 0; j < img_suffixs.size(); j++) {
			stringstream tar_filenamess;
			if (_race_region.compare("SG") == 0)
				tar_filenamess << tar_dir << "/" << tar_raceLabel << "_" << img_suffixs[j];
			else //if (_race_region.compare("AUS") == 0)
				tar_filenamess << tar_dir << "/" << img_suffixs[j];
			//string tar_filename = format("%s/%s_%s", tar_dir, tar_raceLabel, img_suffixs[j]);
			string tar_filename = tar_filenamess.str();

			cout << "racelabel " << tar_raceLabel << endl;
			cout << "suffix" << img_suffixs[j] << endl;
			cout << "tar dir" << tar_dir << endl;
			cout << "tar name" << tar_filename << endl;
			if (!exists(tar_filename)) {
				//each_race_data.push_back(Mat());
				continue;
			}
			num_of_valid_img++;
			valid_filenames.push_back(tar_filename);
		}


		// Gather imgs for each race
		vector<Mat> each_race_data(num_of_valid_img);
		cout << valid_filenames.size() << endl;
		for (int j = 0; j < valid_filenames.size(); j++) {
			cout << valid_filenames[j] << endl;
		}

		//video_processor::SparseOpticalFlow optFlow(video_processor::SparseOpticalFlow::OF_MIDDLE);

#pragma omp parallel for
		for (int j = 0; j < valid_filenames.size(); j++) {
			Mat tar_Mat = imread(valid_filenames[j]);
			cout << valid_filenames[j] << endl;
			//resize(tar_Mat, tar_Mat, Size(80, 40));
			each_race_data[j] = tar_Mat;
			//optFlow.push_back_image(tar_Mat);
		}
		//optFlow.run(0,10);
		//auto optArray=optFlow.get_score_array(video_processor::SparseOpticalFlow::AreaType::OF_MIDDLE);
		//cout <<"begin "<<optArray.size()<< endl;
		//for (size_t k = 0; k < optArray.size(); ++k)
		//{
		//	cout << optArray[k]<<" " << endl;
		//}
		// Get the CNN predict result
		vector<int> pred_id;
	/*	imshow("g", each_race_data.front());
		waitKey(1);*/
		vector<vector<float>> result = Predict(each_race_data);
		int startFrame = -1;
		for (int j = 0; j < result.size(); j++) {
			int max_id = distance(result[j].begin(), max_element(result[j].begin(), result[j].end()));
			pred_id.push_back(max_id);
			if (startFrame != -1)
				continue;
			cout << result[j][0] << " " << result[j][1] << " " << max_id << endl;
			float sum = 0;
			int count = 10;
			if (j + count >= result.size())
			{
				count = result.size() - j - 1;
			}
			cout << "count " << count << " start frame " << startFrame << endl;

			for (int k = 0; k < count; k++)
			{
				sum += result[j + k][1];
				cout << "Add value " << result[j + k][1] << endl;
			}
			if (sum / count > 0.95f)
			{
				startFrame = j;
			}


			cout << j << " Sum " << sum << endl;
		}

		cout << i << " Predicted start frame is " << startFrame << endl;

		// Write to each row of TrackIconGT
		//int startRange = startFrame - 50 >= 0 ? startFrame - 50 : 0;
		//int endRange = startFrame + 50;
		//if (startFrame == 0)
		//{
		//	startRange = 0;
		//	endRange = 299;
		//}

		int startRange = 0;
		int endRange = 299;
		//int optResult = opticalFlowTrack(tar_dir, startRange, endRange, startFrame);
		int optResult = opticalFlowTrack(tar_dir);
		cout << optResult << endl;
		ofs << tar_raceLabel << "," << startFrame << "," << abs(gtMap[tar_raceLabel] - startFrame) << "," << optResult << "," << abs(gtMap[tar_raceLabel] - optResult) << endl;



		// Update Confusion Matrix
		for (int j = 0; j < pred_id.size(); j++) {
			string tar_imgName = img_suffixs[j];
			int result_gt = -1;
			int result_pred = pred_id[j];
			for (StartFrm each_frm : gt) {
				if (each_frm.racelabel == tar_raceLabel && each_frm.imgName == tar_imgName) {
					result_gt = each_frm.frmType;
					break;
				}
			}
			//// For Img Draw
			string tar_dir = format("Predict_StartFrms/%d", result_pred);
			string tar_imgName_1 = format("Predict_StartFrms/%d/%s_%s", result_pred, tar_raceLabel, img_suffixs[j]);
			string tar_imgName_2 = "";
			_mkdir(tar_dir.c_str());
			if (result_gt != result_pred) {
				tar_imgName_2 = format("Predict_StartFrms/Incorrect/%s_%s", tar_raceLabel, img_suffixs[j]);
			}
			else {
				tar_imgName_2 = format("Predict_StartFrms/Correct/%s_%s", tar_raceLabel, img_suffixs[j]);
			}
			imwrite(tar_imgName_1, each_race_data[j]);
			if (result_gt != -1)
				imwrite(tar_imgName_2, each_race_data[j]);
			if (result_gt == -1)
				continue;
			// For confusion Mat
			confusion_mat[result_pred][result_gt]++;
		}
	}

	ofs.close();
	system("pause");


	//Blob<float>* output_layer = net_->output_blobs()[0];
	//CHECK_EQ(labels_.size(), output_layer->channels())
	//<< "Number of labels is different from the output layer dimension.";
}

bool parse_inputArg(int argc, char** argv)
{

	po::options_description desc("Options");
	desc.add_options()
		("help", "Print help message")
		("gt", po::value<string>(&_gt_root), "GT root")
		("region", po::value<string>(&_race_region), "Set region of race(SG/AUS)")
		("jpg", "Set input format to jpg")
		("data", po::value<string>(&_data_dir), "Data dir")
		("config", po::value<string>(&_config_root), "config dir")
		("output", po::value<string>(&_output_path), "output path")
		("key_object", "Visualise Key Object")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	// If "help"
	if (vm.count("help")) {
		cout << desc << endl;
		return false;
	}

	// Display video path, location, scene model directory and letter model xml path
	cout << "=== Input Roots & Directories ===" << endl;
	cout << "_data_dir   = " << _data_dir << endl;
	cout << "_gt_root   = " << _gt_root << endl;
	cout << "_config_root   = " << _config_root << endl;
	cout << "=================================" << endl;

	// Return true
	return true;
}

map<string, int> ReadFirstFrameData()
{
	map<string, int> result;
	stringstream ss;
	ss << _gt_root << "/RacingComVideos.csv";
	std::cout << ss.str() << endl;
	std::ifstream groundTruthCsv(ss.str());
	string line;
	bool isFirstLine = true;
	while (std::getline(groundTruthCsv, line))
	{
		istringstream templine(line);
		string data;
		string raceName = "";
		int frameID = 0;
		int frameEndID = 0;
		stringstream raceNameStream;
		std::smatch m;
		regex rgx("R[0-9]{2}");


		for (int i = 0; std::getline(templine, data, ','); i++)
		{
			if (i == 0)
			{
				bool found = regex_search(data, m, rgx);
				raceNameStream << data.substr(0, m.position(0) + 3) << " ~ " << data.substr(m.position(0) + 3);
				raceName = raceNameStream.str();
				std::cout << raceNameStream.str() << " ";
				m.empty();
			}
			else if (i == 2)
			{
				frameID = std::atoi(data.c_str());
				std::cout << data << " ";
			}


		}

		if (isFirstLine)
		{
			isFirstLine = false;
			continue;
		}
		result[raceName] = frameID;
		std::cout << endl;
	}
	return result;
}