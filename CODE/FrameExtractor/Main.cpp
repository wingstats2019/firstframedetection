#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>
#include <iostream>
#include <regex>
#include <algorithm>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <string>
#include <stdlib.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <map>

#include <fstream>
#include "helper/helper.h"

#include "boost/filesystem.hpp"
#include "boost/program_options.hpp"


using namespace boost::filesystem;
using namespace std;
namespace po = boost::program_options;



//char* inputPath = "T:\\Frames";
//char* resizeOutPath = "T:\\Frames_Input_JPG";
//char* outputPath = "T:\\FramesOutput_JPG";
string inputPath = "T:\\Frames";
string resizeOutPath = "T:\\Frames_Input_JPG";
string outputPath = "T:\\FramesOutput_JPG";
bool isJPG = false;


void generateMarkingCsv();
void generateFirstFrameCsv();

const int firstFrameLimit = 300;

class videoFrameData
{
public:
	int raceID;
	int startFrame;
	int totalFrame;
};

int main(int argc, char* argv[])
{
	generateFirstFrameCsv();
	return 1;
	/*generatecsv();
	return 1;*/
	int a = 0;
	char* pathValue = "";
	path p(inputPath);
	std::vector<directory_entry> v; // To save the file names in a vector.

	string frameStoragePath = cv::format("%s//0", outputPath);
	boost::filesystem::create_directories(frameStoragePath);
	frameStoragePath = cv::format("%s//1", outputPath);
	boost::filesystem::create_directories(frameStoragePath);

	if (is_directory(p))
	{
		copy(directory_iterator(p), directory_iterator(), back_inserter(v));
		std::cout << p << " is a directory containing:\n";
		std::ofstream of;

		for (std::vector<directory_entry>::const_iterator it = v.begin(); it != v.end(); ++it)
		{
			string folderPath = (*it).path().string();


			std::cout << (*it).path().string() << endl;

			path raceFolderPath(folderPath);
			if (is_directory(raceFolderPath))
			{
				std::vector<directory_entry> raceFolderPointer;
				copy(directory_iterator(raceFolderPath), directory_iterator(), back_inserter(raceFolderPointer));

				regex folderRgx("[0-9]*$");
				smatch match;
				if (regex_search(folderPath, match, folderRgx))
				{
					for (size_t i = 0; i < match.size(); ++i)
						std::cout << i << ": " << match[i] << '\n';
				}
				string raceID = match[0];
				for (vector<directory_entry>::const_iterator frames_it = raceFolderPointer.begin(); frames_it != raceFolderPointer.end(); ++frames_it)
				{
					string framePathString = frames_it->path().string();
					regex frameRgx("img[0-9]*[.]jpg$");
					if (regex_search(framePathString, match, frameRgx))
					{
						//for (size_t i = 0; i < match.size(); ++i)
						//	std::cout << i << ": " << match[i] << '\n';
						string copyToPath = cv::format("%s/%s_%s", outputPath, raceID, match[0].str());
						std::cout << "Copy from " << framePathString << " to " << copyToPath << endl;
						//boost::filesystem::copy_file(framePathString, copyToPath);
					}
				}
			}

		}
	}



	return 0;

}

void generateMarkingCsv()
{
	stringstream ss;
	ss << outputPath << "/tmplabel.csv";
	std::ofstream labelCsv(ss.str());
	labelCsv << "";


	path p(inputPath);
	std::vector<directory_entry> v; // To save the file names in a vector.

	string frameStoragePath = cv::format("%s//0", outputPath);
	boost::filesystem::create_directories(frameStoragePath);
	frameStoragePath = cv::format("%s//1", outputPath);
	boost::filesystem::create_directories(frameStoragePath);

	if (is_directory(p))
	{
		copy(directory_iterator(p), directory_iterator(), back_inserter(v));
		std::cout << p << " is a directory containing:\n";
		std::ofstream of;

		for (std::vector<directory_entry>::const_iterator it = v.begin(); it != v.end(); ++it)
		{
			string folderPath = (*it).path().string();
			regex folderRgx("[0-9]*$");
			smatch match;
			if (regex_search(folderPath, match, folderRgx))
			{
				for (size_t i = 0; i < match.size(); ++i)
					std::cout << i << ": " << match[i] << '\n';
			}
			string raceID = match[0];
			labelCsv << raceID << ",0\n";
		}
	}

	labelCsv.close();
}


void generateFirstFrameCsv()
{
	map<string, int> startFrameMapping;
	//map<string, int> startFrameEndMapping;
	stringstream ss;
	ss << inputPath << "/RacingComVideos.csv";
	std::cout << ss.str() << endl;
	std::ifstream groundTruthCsv(ss.str());
	string line;
	bool isFirstLine = true;
	while (std::getline(groundTruthCsv, line))
	{

		istringstream templine(line);
		string data;
		string raceName = "";
		int frameID = 0;
		int frameEndID = 0;
		stringstream raceNameStream;
		std::smatch m;
		regex rgx("R[0-9]{2}");


		for (int i = 0; std::getline(templine, data, ','); i++)
		{
			if (i == 0)
			{
				bool found = regex_search(data, m, rgx);
				raceNameStream << data.substr(0, m.position(0) + 3) << " ~ " << data.substr(m.position(0) + 3);
				raceName = raceNameStream.str();
				std::cout << raceNameStream.str() << " ";
				m.empty();
			}
			else if (i == 2)
			{
				frameID = std::atoi(data.c_str());
				std::cout << data << " ";
			}


		}

		if (isFirstLine)
		{
			isFirstLine = false;
			continue;
		}

		startFrameMapping[raceName] = frameID;
		std::cout << endl;
	}

	groundTruthCsv.close();

	ss = std::stringstream();
	ss << outputPath << "/StartFrmGT.csv";
	std::ofstream startFrameFileStream(ss.str());

	startFrameFileStream << "RaceLabel";
	for (int i = 0; i < firstFrameLimit; i += 1)
	{
		startFrameFileStream << ",img" << cv::format("%06d", i) << ".jpg";
	}
	startFrameFileStream << endl;
	int count = 0;
	for (auto it = startFrameMapping.begin(); it != startFrameMapping.end(); ++it, count++)
	{
		stringstream raceFolderPathStream;
		raceFolderPathStream << inputPath << "/" << it->first;
		stringstream resizeRaceFolderPathStream;
		resizeRaceFolderPathStream << resizeOutPath << "/" << it->first;
		stringstream lineInput;
		if (!boost::filesystem::is_directory(resizeRaceFolderPathStream.str()))
		{
			boost::filesystem::create_directories(resizeRaceFolderPathStream.str());
		}
		if (boost::filesystem::is_directory(raceFolderPathStream.str()))
		{
			lineInput << it->first;
			std::cout << raceFolderPathStream.str() << endl;
			string stringArray[firstFrameLimit];
#pragma omp parallel for
			for (int i = 0; i < firstFrameLimit; i++)
			{
				stringstream lineInputSeg;
				stringstream frameInputPathStream;
				frameInputPathStream << raceFolderPathStream.str() << "/" << "img" << cv::format("%06d", i) << ".jpg";
				stringstream frameResizePathStream;
				if (isJPG)
					frameResizePathStream << resizeRaceFolderPathStream.str() << "/" << "img" << cv::format("%06d", i) << ".jpg";
				else
					frameResizePathStream << resizeRaceFolderPathStream.str() << "/" << "img" << cv::format("%06d", i) << ".bmp";

				if (boost::filesystem::is_regular_file(frameInputPathStream.str()))
				{
					auto readImage = cv::imread(frameInputPathStream.str());

					std::cout << "Resizing " << frameInputPathStream.str() << endl;
					cv::resize(readImage, readImage, cv::Size(227, 227));
					stringstream frameToPathStream;

					//std::regex dateExtract("^([A-Z]).*(\d{2})-(\d{2})-(\d{2}).*&");
					std::regex dateExtract("([A-Za-z]{3}).*([0-9]{2})-([0-9]{2})-([0-9]{2}).R([0-9]{2}).*");
					stringstream fileName;
					stringstream filePrefixss;
					filePrefixss << count << regex_replace(it->first, dateExtract, "$1");
					//string filePrefix = regex_replace(it->first, dateExtract, "$1");
					string filePrefix = filePrefixss.str();
					std::cout << "is match";
					std::cout << regex_match(it->first, dateExtract) << endl;
					std::cout << "File prefix " << filePrefix << endl;
					if (i > 0)//&& i % 5 == 0)
					{
						if (i >= it->second)
						{
							if (isJPG)
								frameToPathStream << outputPath << "/1/" << filePrefix << "" << cv::format("%03d", i) << ".jpg";
							else
								frameToPathStream << outputPath << "/1/" << filePrefix << "" << cv::format("%03d", i) << ".bmp";

							std::cout << "One " << frameInputPathStream.str() << endl;
							lineInputSeg << ",1";
						}
						else
						{
							if (isJPG)
								frameToPathStream << outputPath << "/0/" << filePrefix << "" << cv::format("%03d", i) << ".jpg";
							else
								frameToPathStream << outputPath << "/0/" << filePrefix << "" << cv::format("%03d", i) << ".bmp";

							std::cout << "Zero " << frameInputPathStream.str() << endl;
							lineInputSeg << ",0";
						}
						//cout << frameToPathStream.str().size() << endl;
						if (!boost::filesystem::is_regular_file(frameToPathStream.str()))
						{
							cv::imwrite(frameToPathStream.str(), readImage);
							std::cout << "[Result]writing to " << frameToPathStream.str() << endl;
						}
						if (!boost::filesystem::is_regular_file(frameResizePathStream.str()))
						{
							cv::imwrite(frameResizePathStream.str(), readImage);
							std::cout << "[resize]writing to " << frameResizePathStream.str() << endl;
						}
					}
				}
				stringArray[i] = lineInputSeg.str();
			}

			for (int i = 0; i < firstFrameLimit; i++)
			{
				lineInput << stringArray[i];
			}
			lineInput << endl;
			startFrameFileStream << lineInput.str();
		}
		/*
		for (int i = 0; i < firstFrameLimit; i++)
		{
			if (boost::filesystem::is_regular_file())
			{

			}
		}
*/
	}


	startFrameFileStream.close();

}


bool parse_inputArg(int argc, char** argv)
{

	po::options_description desc("Options");
	desc.add_options()
		("help", "Print help message")
		("jpg", po::value<bool>(&isJPG), "output jpg format")
		("output", po::value<string>(&outputPath), "Data output path")
		("input", po::value<string>(&inputPath), "Data input path")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	// If "help"
	if (vm.count("help")) {
		std::cout << desc << endl;
		return false;
	}

	// Display video path, location, scene model directory and letter model xml path
	std::cout << "=== Input Roots & Directories ===" << endl;
	//std::cout << "_gt_root   = " << _gt_root << endl;
	std::cout << "=================================" << endl;

	// Return true
	return true;
}