#include "CNN_Start_Frm_Detector.h"

using namespace std;
using namespace cv;
using namespace caffe;
using namespace CNN;
using namespace CNN::CNN_Start_Frm_SG;

Detector::Detector(const string & dir)
{
#ifdef CPU_ONLY
	Caffe::set_mode(Caffe::CPU);
#else
	Caffe::set_mode(Caffe::GPU);
#endif

	_dir = dir;

	string model_file = _dir + "\\" + _model_file;
	string trained_file = _dir + "\\" + _trained_file;
	string mean_file = _dir + "\\" + _mean_file;
	string label_file = _dir + "\\" + _label_file;

	/* Load the network. */
	net_.reset(new Net<float>(model_file, TEST));
	net_->CopyTrainedLayersFrom(trained_file);

	CHECK_EQ(net_->num_inputs(), 1) << "Network should have exactly one input.";
	CHECK_EQ(net_->num_outputs(), 1) << "Network should have exactly one output.";

	Blob<float>* input_layer = net_->input_blobs()[0];
	num_channels_ = input_layer->channels();
	CHECK(num_channels_ == 3 || num_channels_ == 1)
		<< "Input layer should have 1 or 3 channels.";
	input_geometry_ = Size(input_layer->width(), input_layer->height());

	/* Load the binaryproto mean file. */
	SetMean(mean_file);

	/* Load labels. */
	ifstream labels(label_file.c_str());
	CHECK(labels) << "Unable to open labels file " << label_file;
	string line;
	while (getline(labels, line))
		labels_.push_back(string(line));

	Blob<float>* output_layer = net_->output_blobs()[0];
	//CHECK_EQ(labels_.size(), output_layer->channels())
		//<< "Number of labels is different from the output layer dimension.";
}

static bool PairCompare(const pair<float, int> & lhs, const pair<float, int> & rhs)
{
	return lhs.first > rhs.first;
}

/* Return the indices of the top N values of vector v. */
static vector<int> Argmax(const vector<float> & v, int N)
{
	vector<pair<float, int> > pairs;
	for (size_t i = 0; i < v.size(); ++i)
		pairs.push_back(make_pair(v[i], static_cast<int>(i)));
	partial_sort(pairs.begin(), pairs.begin() + N, pairs.end(), PairCompare);

	vector<int> result;
	for (int i = 0; i < N; ++i)
		result.push_back(pairs[i].second);
	return result;
}

///* Return the top N predictions. */
//vector<vector<Prediction>> Detector::Classify(const vector<Mat> & imgs, int N)
//{
//	vector<vector<float>> output_stack = Predict(imgs);
//	vector<vector<Prediction>> prediction_stack;
//	for (int j = 0; j < output_stack.size(); j++){
//		vector<float> output = output_stack[j];
//		N = min<int>(labels_.size(), N);
//		vector<int> maxN = Argmax(output, N);
//		vector<Prediction> predictions;
//		for (int i = 0; i < N; ++i) {
//			int idx = maxN[i];
//			predictions.push_back(make_pair(labels_[idx], output[idx]));
//		}
//		prediction_stack.push_back(predictions);
//	}
//	return prediction_stack;
//}

/* Load the mean file in binaryproto format. */
void Detector::SetMean(const string & mean_file)
{
	BlobProto blob_proto;
	ReadProtoFromBinaryFileOrDie(mean_file.c_str(), &blob_proto);

	/* Convert from BlobProto to Blob<float> */
	Blob<float> mean_blob;
	mean_blob.FromProto(blob_proto);
	CHECK_EQ(mean_blob.channels(), num_channels_)
		<< "Number of channels of mean file doesn't match input layer.";

	/* The format of the mean file is planar 32-bit float BGR or grayscale. */
	vector<Mat> channels;
	float* data = mean_blob.mutable_cpu_data();
	for (int i = 0; i < num_channels_; ++i) {
		/* Extract an individual channel. */
		Mat channel(mean_blob.height(), mean_blob.width(), CV_32FC1, data);
		channels.push_back(channel);
		data += mean_blob.height() * mean_blob.width();
	}

	/* Merge the separate channels into a single image. */
	Mat mean;
	merge(channels, mean);

	/* Compute the global mean pixel value and create a mean image
	* filled with this value. */
	Scalar channel_mean = cv::mean(mean);
	mean_ = Mat(input_geometry_, mean.type(), channel_mean);
}

vector<vector<float>> Detector::Predict(const vector<Mat> & imgs)
{
	Blob<float>* input_layer = net_->input_blobs()[0];
	input_layer->Reshape(imgs.size(), num_channels_,
		input_geometry_.height, input_geometry_.width);
	/* Forward dimension change to all layers. */
	net_->Reshape();

	vector<Mat> input_channels;
	WrapInputLayer(&input_channels, imgs.size());

	Preprocess(imgs, &input_channels);

	net_->Forward();

	/* Copy the output layer to a vector */
	Blob<float>* output_layer = net_->output_blobs()[0];

	vector<vector<float>> output_stack;
	const float* current = output_layer->cpu_data();
	for (int i = 0; i < imgs.size(); i++){
		vector<float> output(current + output_layer->channels() * i, current + output_layer->channels() * (i + 1));
		output_stack.push_back(output);

	}

	return output_stack;
}

/* Wrap the input layer of the network in separate Mat objects
* (one per channel). This way we save one memcpy operation and we
* don't need to rely on cudaMemcpy2D. The last preprocessing
* operation will write the separate channels directly to the input
* layer. */
void Detector::WrapInputLayer(vector<Mat> * input_channels, const int NumOfimgs)
{
	Blob<float>* input_layer = net_->input_blobs()[0];

	int width = input_layer->width();
	int height = input_layer->height();
	float* input_data = input_layer->mutable_cpu_data();

	for (int i = 0; i < NumOfimgs*input_layer->channels(); ++i) {
		Mat channel(height, width, CV_32FC1, input_data);
		input_channels->push_back(channel);
		input_data += width * height;
	}
}

void Detector::Preprocess(const vector<Mat> & imgs, vector<Mat> * input_channels)
{
	/* Convert the input image to the input image format of the network. */
	for (int i = 0; i < imgs.size(); i++){
		Mat img = imgs[i];
		Mat sample;
		if (img.channels() == 3 && num_channels_ == 1)
			cvtColor(img, sample, COLOR_BGR2GRAY);
		else if (img.channels() == 4 && num_channels_ == 1)
			cvtColor(img, sample, COLOR_BGRA2GRAY);
		else if (img.channels() == 4 && num_channels_ == 3)
			cvtColor(img, sample, COLOR_BGRA2BGR);
		else if (img.channels() == 1 && num_channels_ == 3)
			cvtColor(img, sample, COLOR_GRAY2BGR);
		else
			sample = img;

		Mat sample_resized;
		if (sample.size() != input_geometry_)
			resize(sample, sample_resized, input_geometry_);
		else
			sample_resized = sample;

		Mat sample_float;
		if (num_channels_ == 3)
			sample_resized.convertTo(sample_float, CV_32FC3);
		else
			sample_resized.convertTo(sample_float, CV_32FC1);

		Mat sample_normalized;
		subtract(sample_float, mean_, sample_normalized);

		vector<Mat> sample_split;
		split(sample_normalized, sample_split);

		for (int chn = 0; chn < num_channels_; chn++)
			sample_split[chn].copyTo((*input_channels)[i * num_channels_ + chn]);

	}

}

//void Detector::Forward(const vector<Mat> & patch_stack, vector<float> & posScores, vector<float> & negScores)
//{
//	vector<float> negScores_tmp, posScores_tmp;
//
//	//calculate score for the patch_stack
//	const vector<vector<Prediction>> predictions_stack = this->Classify(patch_stack);
//
//	/* Get the score result. */
//	for (vector<Prediction> predictions : predictions_stack) {
//		for (Prediction p : predictions) {
//			if (p.first == "0Still")
//				negScores_tmp.push_back(p.second);
//			else
//				posScores_tmp.push_back(p.second);
//		}
//	}
//
//	negScores = negScores_tmp;
//	posScores = posScores_tmp;
//}


vector<int> Detector::run(const vector<Mat> & imgs)
{
	vector<vector<float>> output_stack = Predict(imgs);
	const int N = 1;//
	vector<int> results;
	for (int j = 0; j < output_stack.size(); j++){
		vector<float> output = output_stack[j];
		vector<int> maxN = Argmax(output, N);
		if (maxN.size()>0)
			results.push_back(maxN[0]);
	}
	return results;
}

//vector<vector<Box>> Detector::run_diff_thres(const vector<Mat> & imgs, const vector<vector<JKCP::Box>> & boxes_stack, const float thres)
//{
//
//	//calculate the positve and negative scores 
//	vector<vector<float>> posScores_stack, negScores_stack;
//	_run(imgs, boxes_stack, posScores_stack, negScores_stack);
//
//	//get the boxes that meet the positve threshold
//	vector<vector<Box>> pos_boxes_stack;
//	for (int i = 0; i < boxes_stack.size(); i++){
//		vector<Box> boxes = boxes_stack[i];
//		vector<Box> pos_boxes;
//		for (int j = 0; j < boxes.size(); j++){
//			if (posScores_stack[i][j] > negScores_stack[i][j] + thres){
//				pos_boxes.push_back(boxes[j]);
//			}
//		}
//		pos_boxes_stack.push_back(pos_boxes);
//	}
//	return pos_boxes_stack;
//}
//
//vector<vector<Box>> Detector::nms(vector<vector<Box>> & pos_boxes_stack, vector<vector<float>> & pos_scores_stacks){
//	// Overlapping thres (for grouping)
//	const int OVERLAP_THRES = 0.4;
//
//	vector<vector<Box>> output_boxes;
//	for (int i = 0; i < pos_boxes_stack.size(); ++i){
//		// Vector for group
//		vector<vector<Box>> box_gps;
//		vector<float> high_scores;
//		vector<int> high_scores_id;
//		vector<Box> best_boxes;
//		for (int j = 0; j < pos_boxes_stack[i].size(); ++j){
//			Box eachBox = pos_boxes_stack[i][j];
//			float pos_score = pos_scores_stacks[i][j];
//			bool grouped = false;
//			for (int k = 0; k < box_gps.size(); ++k){
//				for (Box each_gp_ref_box : box_gps[k]){
//					const float overlapScore = Box::overlapScore(eachBox, each_gp_ref_box);
//					if (overlapScore > OVERLAP_THRES){
//						// Update Highscore if needed
//						if (pos_score > high_scores[k]){
//							high_scores[k] = pos_score;
//							high_scores_id[k] = box_gps[k].size();
//						}
//						// Add to the group
//						box_gps[k].push_back(eachBox);
//						grouped = true;
//						break;
//					}
//				}
//				if (grouped)
//					break;
//			}
//			// if no group is matached, form a new Group itself
//			if (!grouped){
//				box_gps.push_back(vector<Box>({ eachBox }));
//				high_scores.push_back(pos_score);
//				high_scores_id.push_back(0);
//			}
//		}
//		// for each group, output the best box
//		for (int j = 0; j < box_gps.size(); ++j){
//			int highest_id = high_scores_id[j];
//			best_boxes.push_back(box_gps[j][highest_id]);
//		}
//		output_boxes.push_back(best_boxes);
//	}
//	return output_boxes;
//
//}