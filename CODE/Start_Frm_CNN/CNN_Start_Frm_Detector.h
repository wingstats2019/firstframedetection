#pragma once

#include <caffe/caffe.hpp>

#include "helper/helper.h"
//#include "JKCP_Dataset/Box.h"
//#include "JKCP_Dataset/RaceDataset.h"

namespace CNN {
	namespace CNN_Start_Frm_SG {
		typedef std::pair<std::string, float> Prediction;

		class Detector {
		private:
			std::shared_ptr<caffe::Net<float> > net_;
			cv::Size input_geometry_;
			int num_channels_;
			cv::Mat mean_;
			std::vector<std::string> labels_;
			//files for CNN_Classify
			const std::string _model_file = "start_frm_cnn_deploy.prototxt";
			const std::string _trained_file = "start_frm_cnn_iter_50000.caffemodel";
			const std::string _mean_file = "mean.binaryproto";
			const std::string _label_file = "synset_words.txt";
			std::string _dir;

		public:
			// CNN_Classify constructor input: CNN_Classify directory
			Detector(const std::string & dir);

			//Return the predicted result between 0 - 1 of images stack
			//std::vector<std::vector<Prediction>> Classify(const std::vector<cv::Mat> & imgs, int N = 5);

			//Return positive and negative score
			//void Forward(const std::vector<cv::Mat> & patch_stack, std::vector<float> & negScore, std::vector<float> & posScore);

			//Run the CNN_Classify process
			std::vector<int> run(const std::vector<cv::Mat> & imgs);

			//Run different threshold
			//std::vector<std::vector<JKCP::Box>> run_diff_thres(const std::vector<cv::Mat> & imgs, const std::vector<std::vector<JKCP::Box>> & boxes_stack, const float thres);

		private:
			void SetMean(const std::string & mean_file);

			std::vector<std::vector<float>> Predict(const std::vector<cv::Mat> & imgs);

			void WrapInputLayer(std::vector<cv::Mat> * input_channels, const int NumOfimgs);

			void Preprocess(const std::vector<cv::Mat> & imgs, std::vector<cv::Mat> * input_channels);
		};
	}
}

#ifdef _DEBUG
#pragma comment(lib, "caffe-d.lib")
#pragma comment(lib, "proto-d.lib")
#pragma comment(lib, "caffehdf5_D.lib")
#pragma comment(lib, "caffehdf5_hl_D.lib")
#pragma comment(lib, "lmdbd.lib")
#pragma comment(lib, "libopenblas.dll.a")
#pragma comment(lib, "ntdll.lib")
#pragma comment(lib, "glogd.lib")
#pragma comment(lib, "gflagsd.lib")
#pragma comment(lib, "libprotobufd.lib")
#else
#pragma comment(lib, "caffe.lib")
#pragma comment(lib, "proto.lib")
#pragma comment(lib, "caffehdf5.lib")
#pragma comment(lib, "caffehdf5_hl.lib")
#pragma comment(lib, "lmdb.lib")
#pragma comment(lib, "libopenblas.dll.a")
#pragma comment(lib, "ntdll.lib")
#pragma comment(lib, "glog.lib")
#pragma comment(lib, "gflags.lib")
#pragma comment(lib, "libprotobuf.lib")
#endif

#pragma comment(lib, "Start_Frm_CNN.lib")